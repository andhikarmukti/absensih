<?php

namespace App\Filters;

use Closure;

class CompanyFilter
{
    public function handle($query, Closure $next)
    {
        if(request(('keyword'))){
            $query->where('company_name', 'like', '%' . request('keyword') . '%');
        }
        
        return $next($query);
    }
}

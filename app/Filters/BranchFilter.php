<?php

namespace App\Filters;

use Closure;

class BranchFilter
{
    public function handle($query, Closure $next)
    {
        if(request(('keyword'))){
            $query->where('branch_name', 'like', '%' . request('keyword') . '%');
        }
        if(request(('company_id'))){
            $query->where('company_id', request('company_id'));
        }
        
        return $next($query);
    }
}

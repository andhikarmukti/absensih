<?php

namespace App\Console\Commands;

use DateTime;
use DatePeriod;
use DateInterval;
use App\Models\User;
use App\Models\Holiday;
use App\Models\Overtime;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use App\Models\HistoryAttendance;

class HolidayNational extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'holiday_national_attendance_submit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Cek apakah ada holiday? Jika ada, maka submit absen dengan checkin time 0, checkout time 0, description holiday
        $holiday = Holiday::whereDate('holiday_date', Carbon::today())->first();
        if($holiday->is_holiday_national === 1){
                $user_id_overtime = null;
                $users            = User::all();

            for($i = 1; $i <= $users->count(); $i++){
                $user_overtime = Overtime::where('approval', 'accept')->where('user_id', $i)->whereMonth('start_overtime_date', Carbon::today())->get();
                if($user_overtime){
                    foreach ($user_overtime as $user_overtime_today) {
                        $period = new DatePeriod(
                            new DateTime($user_overtime_today->start_overtime_date),
                            new DateInterval('P1D'),
                            new DateTime(date('Y-m-d', strtotime($user_overtime_today->end_overtime_date . "+ 1 day")))
                        );
                        foreach ($period as $p) {
                            if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {
                                $user_id_overtime = $user_overtime_today->user_id;
                            }
                        }
                    }    
                }
                if($user_id_overtime){
                    if($i != $user_id_overtime){
                        HistoryAttendance::create([
                            'user_id' => $i,
                            'shift_id' => 1,
                            'checkin_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                            'checkout_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                            'total_working_hours' => '00:00:00',
                            'checkin_image' => 'storage/img/default.jpg',
                            'description' => 'holiday national'
                        ]);
                    }
                }else{
                    HistoryAttendance::create([
                        'user_id' => $i,
                        'shift_id' => 1,
                        'checkin_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                        'checkout_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                        'total_working_hours' => '00:00:00',
                        'checkin_image' => 'storage/img/default.jpg',
                        'description' => 'holiday national'
                    ]);
                }
            }
        }
    }
}

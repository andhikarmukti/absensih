<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use App\Models\DynamicShiftChange;

class WeeklyDynamicShiftChange extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:weekly_dynamic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $weekly = DynamicShiftChange::where('rotation_shift', 'Weekly (every Monday)')->get();

        foreach ($weekly as $w) {
            $user = User::find($w->user_id);
            if ($user->shift_schedule_id == $w->from_shift_schedule_id) {
                $dynamic_id = $w->to_shift_schedule_id;
            } else if ($user->shift_schedule_id == $w->to_shift_schedule_id) {
                $dynamic_id = $w->from_shift_schedule_id;
            }
            $user->update([
                'shift_schedule_id' => $dynamic_id
            ]);
        }
    }
}

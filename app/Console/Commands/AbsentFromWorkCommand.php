<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Shift;
use Illuminate\Console\Command;
use App\Models\HistoryAttendance;

class AbsentFromWorkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absentfromwork';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user_length = User::all()->count();

        for ($i = 0; $i < $user_length; $i++) {
            $user = User::where('id', $i)->first();
            $today = Carbon::today()->isoFormat('dddd');
            // print_r($user);
            if ($user) {
                $attendance_check_today = $user->historyattendance()->whereDate('checkin_time', today())->get()->count();
                if ($attendance_check_today > 0) {
                    // print_r('ok');
                } else {
                    if ($user->shiftschedule) {
                        $shift_schedule = $user->shiftschedule;
                        $today          = Carbon::today()->isoFormat('dddd');
                        $user_shift     = Shift::find($shift_schedule->$today);
                        if ($user_shift->is_flexible == 'on') {
                            print_r('ok');
                        } else if (Carbon::now()->format('H:i:s') > $user_shift->end_shift_hours) {
                            HistoryAttendance::create([
                                'user_id' => $i,
                                'shift_id' => 1, // shift off adalah id 1
                                'checkin_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                                'checkout_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                                'total_working_hours' => '00:00:00',
                                'checkin_image' => 'storage/img/default.jpg',
                                'description' => 'absent from work'
                            ]);
                        }
                    }
                }
            }
        }
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Holiday;
use Illuminate\Console\Command;

class HolidayDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'holiday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $curl = curl_init();
        $url = "https://api-harilibur.vercel.app/api";

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        $result = json_decode($result);
        curl_close($curl);

        Holiday::truncate();

        for ($i = 0; $i < count($result); $i++) {
            Holiday::create([
                'holiday_date' => $result[$i]->holiday_date,
                'holiday_name' => $result[$i]->holiday_name,
                'is_holiday_national' => $result[$i]->is_national_holiday,
            ]);
        }

        return 'ok sip';
    }
}

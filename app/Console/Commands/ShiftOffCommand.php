<?php

namespace App\Console\Commands;

use App\Models\ChangeShift;
use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Shift;
use Illuminate\Console\Command;
use App\Models\HistoryAttendance;

class ShiftOffCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shift_off_command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user_length = User::all()->count();
        for ($i = 1; $i < $user_length; $i++) {
            $user = User::where('id', $i)->first();
            $today = Carbon::today()->isoFormat('dddd');

            // cek apakah ada perubahan shift hari itu untuk user id yang absen?
            $check_shift_change_today = ChangeShift::where('user_id', $i)->where('approval', 'accept')->whereMonth('created_at', today())->whereYear('created_at', today())->get();
            if ($check_shift_change_today) {
                foreach ($check_shift_change_today as $cs_today) {
                    if (Shift::find($cs_today->shift_id)->shift_name == 'Shift Off') {
                        $period = new DatePeriod(
                            new DateTime($cs_today->start_shift_date),
                            new DateInterval('P1D'),
                            new DateTime(date('Y-m-d', strtotime($cs_today->end_shift_date . "+ 1 day")))
                        );
                        foreach ($period as $p) {
                            if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {
                                HistoryAttendance::create([
                                    'user_id' => $i,
                                    'shift_id' => $cs_today->shift_id, // shift off adalah id 1
                                    'checkin_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                                    'checkout_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                                    'total_working_hours' => '00:00:00',
                                    'checkin_image' => 'storage/img/default.jpg',
                                    'description' => 'shift off'
                                ]);
                            }
                        }
                    } else {
                        print_r('ada perubahan shift dari user id' . $i . ', namun bukan shift off');
                    }
                }
            } else {
                if ($user) {
                    if ($user->shiftschedule) {
                        $shift_schedule = $user->shiftschedule;
                        $today          = Carbon::today()->isoFormat('dddd');
                        $user_shift     = Shift::find($shift_schedule->$today);
                        if ($user_shift->shift_name == 'Shift Off') {
                            if (Carbon::now()->format('H:i:s') > $user_shift->end_shift_hours) {
                                HistoryAttendance::create([
                                    'user_id' => $i,
                                    'shift_id' => $user_shift->id, // shift off adalah id 1
                                    'checkin_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                                    'checkout_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                                    'total_working_hours' => '00:00:00',
                                    'checkin_image' => 'storage/img/default.jpg',
                                    'description' => 'shift off'
                                ]);
                            }
                        }
                    }
                }
            }
        }
    }
}

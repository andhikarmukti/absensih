<?php

namespace App\Console\Commands;

use DateTime;
use DatePeriod;
use DateInterval;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use App\Models\PaidLeaveHistory;
use App\Models\HistoryAttendance;

class PaidLeaveCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paid_leave';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $paid_leave = PaidLeaveHistory::where('approval', 'accept')->whereMonth('start_paid_leave_date', today())->get();
        foreach ($paid_leave as $pl) {
            $period = new DatePeriod(
                new DateTime($pl->start_paid_leave_date),
                new DateInterval('P1D'),
                new DateTime(date('Y-m-d', strtotime($pl->end_paid_leave_date . "+ 1 day")))
            );
            foreach ($period as $p) {
                if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {
                    print_r($pl->user_id);
                    HistoryAttendance::create([
                        'user_id' => $pl->user_id,
                        'shift_id' => 1,
                        'checkin_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                        'checkout_time' => Carbon::today()->format('Y-m-d') . " " . '00:00:00',
                        'total_working_hours' => '00:00:00',
                        'checkin_image' => 'storage/img/default.jpg',
                        'description' => 'paid leave'
                    ]);
                }
            }
        } 
    }
}

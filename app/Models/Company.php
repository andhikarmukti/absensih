<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends Model
{
    use HasFactory;

    protected $guarded = [
        'id'
    ];

    public function branch()
    {
        return $this->hasMany(BranchCompany::class);
    }

    public function entity()
    {
        return $this->hasManyThrough(Entity::class, BranchCompany::class, 'company_id', 'branch_id', 'id', 'id');
    }

    public function user($company_id)
    {
        return User::join('departments', 'users.department_id', '=', 'departments.id')
        ->join('entities', 'departments.entity_id', '=', 'entities.id')
        ->join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
        ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
        ->where('companies.id', '=', $company_id)
        ->select('users.*')
        ->get();
    }

    public function salary()
    {
        return $this->hasMany(Salary::class);
    }

    public function department($company_id)
    {
        return Department::join('entities', 'departments.entity_id', '=', 'entities.id')
        ->join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
        ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
        ->where('companies.id', '=', $company_id)
        ->select('departments.*')
        ->get();
    }

    public function shift()
    {
        return $this->hasMany(Shift::class);
    }

    public function group()
    {
        return $this->hasMany(Group::class);
    }

    public function allowance()
    {
        return $this->hasMany(Allowance::class);
    }

    public function paidleave()
    {
        return $this->hasMany(PaidLeave::class);
    }
}

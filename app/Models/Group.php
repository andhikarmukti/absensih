<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    // protected $table = "group";

    public function groupuser()
    {
        return $this->hasMany(GroupUser::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class, 'group_users');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->hasManyThrough(User::class, Department::class);
    }

    public function branch()
    {
        return $this->belongsTo(BranchCompany::class);
    }

    public function department()
    {
        return $this->hasMany(Department::class);
    }
}

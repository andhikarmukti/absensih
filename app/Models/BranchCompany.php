<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchCompany extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function entity()
    {
        return $this->hasMany(Entity::class);
    }

    public function user($branch_id)
    {
        return User::join('departments', 'users.department_id', '=', 'departments.id')
        ->join('entities', 'departments.entity_id', '=', 'entities.id')
        ->join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
        ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
        ->where('branch_companies.id', '=', $branch_id)
        ->select('users.*', 'entities.*', 'departments.*')
        ->get();
    }
}

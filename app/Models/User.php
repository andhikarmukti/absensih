<?php

namespace App\Models;

use App\Models\Group;
use App\Models\Department;
use App\Models\BranchCompany;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $guarded = [
        'id'
    ];
    // protected $table = "group";

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Status Employee
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function entity()
    {
        return $this->department->entity();
    }

    public function branch()
    {
        return $this->entity->branch();
    }

    public function company()
    {
        return $this->branch->company();
    }

    // Job level
    public function joblevel()
    {
        return $this->belongsTo(JobLevel::class, 'job_level_id');
    }

    // Salary
    public function salary()
    {
        return $this->belongsTo(Salary::class, 'salary_id');
    }

    // Shift
    public function shiftschedule()
    {
        return $this->belongsTo(ShiftSchedule::class, 'shift_schedule_id');
    }

    public function shiftpattern()
    {
        return $this->shiftschedule->shiftpattern();
    }

    // karna sudah tidak ada lagi keterangan shift_id di table shift_schedules
    // public function shift()
    // {
    //     return $this->shiftschedule->shift();
    // }

    // Group User
    public function groupuser()
    {
        return $this->hasMany(GroupUser::class);
    }
    public function group()
    {
        return $this->belongsToMany(Group::class, 'group_users');
    }

    // Organizational Structure
    public function organizationalstructure()
    {
        return $this->belongsTo(OrganizationalStructure::class, 'id', 'user_id');
    }

    // Payroll
    public function payroll()
    {
        return $this->hasMany(Payroll::class);
    }

    // Reimburse
    public function reimburse()
    {
        return $this->hasMany(Reimburse::class);
    }

    // Paid Leave
    public function paidleave()
    {
        return $this->belongsToMany(PaidLeave::class, 'paid_leave_users');
    }

    // Paid Leave History
    public function paidleavehistory()
    {
        return $this->hasMany(PaidLeaveHistory::class);
    }

    // History Attendance
    public function historyattendance()
    {
        return $this->hasMany(HistoryAttendance::class, 'user_id', 'id');
    }

    // supervisor id
    public function supervisor()
    {
        return $this->hasMany(OrganizationalStructure::class);
    }

    // change shift
    public function changeshift()
    {
        return $this->hasMany(ChangeShift::class, 'user_id');
    }

    // Employee allowance
    public function employeeallowance()
    {
        return $this->hasMany(EmployeeAllowance::class);
    }

    // Employee allowance
    public function allowance()
    {
        return $this->belongsToMany(Allowance::class, 'employee_allowances');
    }

    // Office Position
    public function officeposition()
    {
        return $this->belongsTo(OfficePosition::class, 'office_position_id');
    }

    // Overtime
    public function overtime()
    {
        return $this->hasMany(Overtime::class);
    }

    // Paid Leave
    public function paidleaveaccess()
    {
        return $this->belongsToMany(PaidLeave::class, 'paid_leave_accesses', 'user_id');
    }
}

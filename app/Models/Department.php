<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Department extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    public function branch()
    {
        return $this->entity->branch();
    }

    public function company()
    {
        return $this->branch->company();
    }

    public function shift()
    {
        return $this->company->shift();
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }
}

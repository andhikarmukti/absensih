<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaidLeave extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsToMany(User::class, 'paid_leave_users', 'paid_leave_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function paidleaveaccess()
    {
        return $this->belongsToMany(User::class, 'paid_leave_accesses');
    }
}

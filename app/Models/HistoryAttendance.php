<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryAttendance extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    // protected $cast = [
    //     'checkin_time' => 'datetime',
    //     'checkout_time' => 'datetime'
    // ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function shift()
    {
        return $this->belongsTo(Shift::class, 'shift_id', 'id');
    }

    public function company($company_id)
    {
        return HistoryAttendance::join('users', 'history_attendances.user_id', '=', 'users.id')
        ->join('departments', 'users.department_id', '=', 'departments.id')
        ->join('entities', 'departments.entity_id', '=', 'entities.id')
        ->join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
        ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
        ->where('companies.id', '=', $company_id)
        ->select('history_attendances.*')
        ->get();
    }
}

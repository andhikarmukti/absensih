<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Allowance extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function formula()
    {
        return $this->belongsTo(Formula::class, 'formula_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function user()
    {
        return $this->belongsToMany(User::class, 'employee_allowances');
    }
}

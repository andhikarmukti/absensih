<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function shiftpattern()
    {
        return $this->hasMany(ShiftPattern::class);
    }

    public function shiftschedule()
    {
        return $this->hasOneThrough(ShiftSchedule::class, ShiftPattern::class);
    }
}

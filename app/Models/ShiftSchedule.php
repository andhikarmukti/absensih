<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ShiftSchedule extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function shiftPattern()
    {
        return $this->hasMany(ShiftPattern::class, 'shift_id', 'shift_id');
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }

    // public function shift()
    // {
    //     return $this->belongsTo(Shift::class, 'shift_id');
    // }
}

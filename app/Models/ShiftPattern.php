<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShiftPattern extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function shift()
    {
        return $this->belongsTo(Shift::class, 'shift_id');
    }
}

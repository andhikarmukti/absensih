<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->token == 'd03ff95ab35bb233d3f05f92b4cf65c1') {
            return $next($request);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'bad api-auth'
            ], 401);
        }
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Models\User;
use App\Models\Shift;
use App\Models\ChangeShift;
use Illuminate\Http\Request;
use App\Models\ShiftSchedule;
use Illuminate\Support\Carbon;
use App\Models\HistoryAttendance;

class LocationCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $latitude2 = $request->latitude;
        $longitude2 = $request->longitude;
        $user = User::find($request->user_id);

        // cek apakah ada perubahan shift hari itu untuk user id yang absen?
        if (!$user->changeshift->isEmpty()) {
            $check_shift_change_today = $user->changeshift->where('approval', 'accept');
            foreach ($check_shift_change_today as $cs_today) {
                $period = new DatePeriod(
                    new DateTime($cs_today->start_shift_date),
                    new DateInterval('P1D'),
                    new DateTime(date('Y-m-d', strtotime($cs_today->end_shift_date . "+ 1 day")))
                );
                foreach ($period as $p) {
                    if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {
                        $shift_location_check = Shift::where('id', $cs_today->shift_id)->first()->location_check;
                        if ($shift_location_check != "on") {
                            return $next($request);
                        }
                    }
                }
            }
        }

        //pengecekan kondisi jika jarak terlalu jauh
        $shift_schedule       = ShiftSchedule::where('id', $user->shift_schedule_id)->first();
        if ($shift_schedule == null) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Kamu belum memiliki shift schedule, silahkan hubungi admin'
            ], 401);
        }
        $today                = Carbon::today()->isoFormat('dddd');
        $shift_id             = $shift_schedule->$today;
        $shift_location_check = Shift::find($shift_id)->location_check;
        if ($shift_location_check != "on") {
            return $next($request);
        }

        // pengecekan lokasi absen
        $latitude1 = "-6.026194";
        $longitude1 =  "106.046944";
        $earth_radius = 6371; // earth radius in km

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * asin(sqrt($a));
        $distance = $earth_radius * $c;
        $distance = $distance * 1000; // satuan meter

        if ($distance > 100) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Jarak terlalu jauh dari batas jangkauan',
                'distance' => $distance,
                'a' => $a,
                'c' => $c,
                'dLat' => $dLat,
                'dLon' => $dLon,
                'latitude2' => $latitude2,
                'longitude2' => $longitude2,
            ], 200);
        }
        return $next($request);
    }
}

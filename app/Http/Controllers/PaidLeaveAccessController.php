<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\PaidLeave;
use Illuminate\Http\Request;
use App\Models\PaidLeaveAccess;

class PaidLeaveAccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $users       = auth()->user()->company->user(auth()->user()->company->id);
        $paid_leaves = PaidLeave::where('company_id', auth()->user()->company->id)->get();
        $paid_leaves_join = PaidLeave::join('paid_leave_accesses', 'paid_leaves.id', '=', 'paid_leave_accesses.paid_leave_id')
                                        ->where('paid_leaves.company_id', auth()->user()->company->id)
                                        ->get();

        if($request->user_id){
        $user             = User::find($request->user_id);
        $paid_leaves_join = PaidLeave::join('paid_leave_accesses', 'paid_leaves.id', '=', 'paid_leave_accesses.paid_leave_id')
                                        ->where('paid_leave_accesses.user_id', $user->id)
                                        ->get();
        }

        return view('paid-leave.paid-leave-access.create', compact(
            'users',
            'paid_leaves',
            'paid_leaves_join'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find($request->user_id);
        $paid_leaves = PaidLeave::where('company_id', $user->company->id)->get();;
        PaidLeaveAccess::where('user_id', $user->id)->delete();
        foreach($paid_leaves as $paid_leave){
            $paid_leave_name = $paid_leave->paid_leave_name;
            $paid_leave_name = str_replace(" ", "_", $paid_leave_name);
            $paid_leave_id = $request->$paid_leave_name;
            if($paid_leave_id){
                if(PaidLeaveAccess::where('user_id', $user->id)->where('paid_leave_id', $paid_leave_id)->count() == 0){
                    PaidLeaveAccess::create([
                        'user_id' => $user->id,
                        'paid_leave_id' => $paid_leave_id
                    ]);
                }
            }
        }
        return back()->with('berhasil', 'Berhasil update data cuti akses');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaidLeaveAccess  $paidLeaveAccess
     * @return \Illuminate\Http\Response
     */
    public function show(PaidLeaveAccess $paidLeaveAccess)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaidLeaveAccess  $paidLeaveAccess
     * @return \Illuminate\Http\Response
     */
    public function edit(PaidLeaveAccess $paidLeaveAccess)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaidLeaveAccess  $paidLeaveAccess
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaidLeaveAccess $paidLeaveAccess)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaidLeaveAccess  $paidLeaveAccess
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaidLeaveAccess $paidLeaveAccess)
    {
        //
    }
}

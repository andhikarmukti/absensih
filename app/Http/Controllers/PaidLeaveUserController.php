<?php

namespace App\Http\Controllers;

use App\Models\PaidLeaveUser;
use Illuminate\Http\Request;

class PaidLeaveUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaidLeaveUser  $paidLeaveUser
     * @return \Illuminate\Http\Response
     */
    public function show(PaidLeaveUser $paidLeaveUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaidLeaveUser  $paidLeaveUser
     * @return \Illuminate\Http\Response
     */
    public function edit(PaidLeaveUser $paidLeaveUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaidLeaveUser  $paidLeaveUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaidLeaveUser $paidLeaveUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaidLeaveUser  $paidLeaveUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaidLeaveUser $paidLeaveUser)
    {
        //
    }
}

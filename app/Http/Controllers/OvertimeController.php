<?php

namespace App\Http\Controllers;

use App\Models\Overtime;
use Illuminate\Http\Request;

class OvertimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('overtime.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // mendapatkan total berapa jam lemburnya
        $total_overtime_hour = strtotime($request->end_overtime_hour) - strtotime($request->start_overtime_hour);
        $total_overtime_hour = explode(":", gmdate("H:i:s", $total_overtime_hour));
        $total_overtime_hour = ($total_overtime_hour[0] * 60 + $total_overtime_hour[1]) / 60;
        // dd(number_format($total_overtime_hour,2));

        // // gaji per jam
        // $salary_per_hour = auth()->user()->salary->salary_value * (1 / 173);

        // // total nilai lembur
        // if($total_overtime_hour > 6){
        //     $nominal_lembur = $total_overtime_hour * $salary_per_hour * 3;
        // }else{
        //     $nominal_lembur = $total_overtime_hour * $salary_per_hour * 2;
        // }

        Overtime::create([
            'user_id' => auth()->user()->id,
            'start_overtime_date' => $request->start_overtime_date,
            'end_overtime_date' => $request->end_overtime_date,
            'start_overtime_hour' => $request->start_overtime_hour,
            'end_overtime_hour' => $request->end_overtime_hour,
            'total_overtime_hour' => number_format($total_overtime_hour, 2),
            'description' => $request->description,
            'supervisor_id' => auth()->user()->supervisor->first()->supervisor_id
        ]);
        return back()->with('berhasil', 'berhasil menambahkan data overtime');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Overtime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $users = Overtime::where('supervisor_id', auth()->user()->id)
            ->where('approval', 'pending')
            ->get();

        return view('overtime.show', compact(
            'users'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Overtime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function edit(Overtime $overtime)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Overtime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // Approval accept or decline
        if ($request->accept) {
            $id       = $request->accept;
            $approval = 'accept';
        } else {
            $id       = $request->decline;
            $approval = 'decline';
        }
        Overtime::where('id', $id)->update([
            'approval' => $approval
        ]);
        if ($approval == 'accept') {
            return back()->with('accept', 'pengajuan lembur telah disetujui');
        } else {
            return back()->with('decline', 'pengajuan lembur ditolak');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Overtime  $overtime
     * @return \Illuminate\Http\Response
     */
    public function destroy(Overtime $overtime)
    {
        //
    }
}

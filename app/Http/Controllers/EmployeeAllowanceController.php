<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Allowance;
use Illuminate\Http\Request;
use App\Models\EmployeeAllowance;

class EmployeeAllowanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_base_on_allowance(Request $request)
    {
        $allowances    = Allowance::where('company_id', auth()->user()->company->id)->get();
        $users_company = auth()->user()->company->user(auth()->user()->company->id);
        // $users_allowance = EmployeeAllowance::where('allowance_id', 3)->first();;
        //     dd($users_allowance);
        
        if($request->allowance_id){
            if($employee_allowance = EmployeeAllowance::where('allowance_id', $request->allowance_id)->first()){
                $users_allowance = $employee_allowance->allowance->user;
                
                return response()->json($users_allowance);
            }else{
                $users_allowance = 'kosong';
    
                return response()->json($users_allowance);
            }
        }

        return view('employee-allowance.create-base-on-allowance', compact(
            'allowances',
            'users_company'
        ));
    }

    public function create_base_on_user(Request $request)
    {
        $allowances    = Allowance::where('company_id', auth()->user()->company->id)->get();
        $users_company = auth()->user()->company->user(auth()->user()->company->id);
        // $users_allowance = EmployeeAllowance::where('allowance_id', 3)->first();;
        //     dd($users_allowance);
        
        if($request->user_id){
            if($employee_allowance = EmployeeAllowance::where('user_id', $request->user_id)->first()){
                $users_allowance = $employee_allowance->user->allowance;
                
                return response()->json($users_allowance);
            }else{
                $users_allowance = 'kosong';
    
                return response()->json($users_allowance);
            }
        }

        return view('employee-allowance.create-base-on-user', compact(
            'allowances',
            'users_company'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user       = User::where('id', $request->user_id)->first();
        $allowance  = Allowance::where('id', $request->allowance_id)->first();
        $user_check = EmployeeAllowance::where('user_id', $request->user_id)
                                        ->where('allowance_id', $request->allowance_id)
                                        ->count();
        if($user_check > 0){
            return back()->with('sudah ada', $user->full_name . ' sudah memiliki ' . $allowance->allowance_name);
        }

        EmployeeAllowance::create([
            'user_id' => $request->user_id,
            'allowance_id' => $request->allowance_id
        ]); return back()->with('berhasil', 'Berhasil menambahkan tunjangan ke user ' . $user->full_name);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeAllowance  $employeeAllowance
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeAllowance $employeeAllowance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeAllowance  $employeeAllowance
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeAllowance $employeeAllowance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeAllowance  $employeeAllowance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeAllowance $employeeAllowance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeAllowance  $employeeAllowance
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeAllowance $employeeAllowance)
    {
        //
    }
}

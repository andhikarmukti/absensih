<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\User;
use App\Models\Shift;
use Illuminate\Http\Request;
use App\Models\ShiftSchedule;

class ShiftScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $shifts = Shift::where('company_id', auth()->user()->company->id)->get();
        $shift = new Shift;
        $shiftSchedules = ShiftSchedule::where('company_id', auth()->user()->company->id)->get();

        return view('shiftschedule.create', compact([
            'users',
            'shifts',
            'shift',
            'shiftSchedules'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('id', auth()->user()->id)->first();

        ShiftSchedule::create([
            'company_id' => $user->company->id,
            'Monday' => $request->monday,
            'Tuesday' => $request->tuesday,
            'Wednesday' => $request->wednesday,
            'Thursday' => $request->thursday,
            'Friday' => $request->friday,
            'Saturday' => $request->saturday,
            'Sunday' => $request->sunday,
            'shift_schedule_name' => $request->shift_schedule_name,
            'start_shift_pattern_date' => $request->start_shift_pattern_date,
            'end_shift_pattern_date' => $request->end_shift_pattern_date,
        ]);
        return back()->with('berhasil', 'Shift Schedule berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShiftSchedule  $shiftSchedule
     * @return \Illuminate\Http\Response
     */
    public function show(ShiftSchedule $shiftSchedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShiftSchedule  $shiftSchedule
     * @return \Illuminate\Http\Response
     */
    public function edit($id, ShiftSchedule $shiftSchedule)
    {
        $shift_schedule = ShiftSchedule::find($id);
        $shifts = Shift::where('company_id', auth()->user()->company->id)->get();
        $shift = new Shift;

        return view('shiftschedule.edit', compact(
            'shift_schedule',
            'shifts',
            'shift'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShiftSchedule  $shiftSchedule
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, ShiftSchedule $shiftSchedule)
    {
        ShiftSchedule::find($id)->update([
            'shift_schedule_name' => $request->shift_schedule_name,
            'start_shift_pattern_date' => $request->start_shift_pattern_date,
            'end_shift_pattern_date' => $request->end_shift_pattern_date,
            'shift_id' => $request->shift_id,
            'Monday' => $request->monday,
            'Tuesday' => $request->tuesday,
            'Wednesday' => $request->wednesday,
            'Thursday' => $request->thursday,
            'Friday' => $request->friday,
            'Saturday' => $request->saturday,
            'Sunday' => $request->sunday,
        ]);

        return redirect('/shift-schedule')->with('update', 'Berhasil update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShiftSchedule  $shiftSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShiftSchedule::find($id)->delete();

        return back()->with('delete', 'Berhasil hapus data');
    }
}

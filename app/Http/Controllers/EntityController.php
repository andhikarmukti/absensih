<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Entity;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Models\BranchCompany;
use App\Models\Department;

class EntityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_company   = auth()->user()->company;
        $branches       = BranchCompany::where('company_id', $user_company->id)->get();
        $entities       = Entity::join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
                                    ->select('entities.*', 'branch_companies.branch_name')
                                    ->where('branch_companies.company_id', auth()->user()->company->id)
                                    ->get();

        return view('entity.create', compact(
            'branches',
            'entities'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Entity::create([
            'branch_id' => $request->branch_id,
            'entity_name' => $request->entity_name
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan entity baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Entity $entity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Entity $entity)
    {
        $entity   = $entity->find($id);
        $branches = BranchCompany::where('company_id', auth()->user()->company->id)->get();

        return view('entity.edit', compact(
            'entity',
            'id',
            'branches'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Entity $entity)
    {
        $entity->find($id)->update([
            'branch_id' => $request->branch_id,
            'entity_name' => $request->entity_name
        ]);
        return redirect('/entity')->with('update', 'Berhasil merubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Entity $entity)
    {
        $entity = $entity->where('id', $id)->first();
        foreach($entity->user as $eu){
            User::where('email', $eu->email)->delete();
        }
        Department::where('entity_id', $entity->id)->delete();
        $entity->delete();

        return back()->with('delete', 'Berhasil menghapus data');

    }
}

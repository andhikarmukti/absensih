<?php

namespace App\Http\Controllers;

use App\Models\PaidLeave;
use Illuminate\Http\Request;

class PaidLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paid_leaves = PaidLeave::where('company_id', auth()->user()->company->id)->get();

        return view('paid-leave.create', compact(
            'paid_leaves'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($company_id, Request $request)
    {
        $paid_leave = PaidLeave::where('company_id', $company_id)->get();
        foreach ($paid_leave as $pl) {
            if (strtolower($request->paid_leave_name) == strtolower($pl->paid_leave_name)) {
                return back()->with('nama cuti sama', 'Maaf, ' . $request->paid_leave_name . ' sudah ada. Silahkan coba nama cuti yang lain');
            }
        }


        PaidLeave::create([
            'paid_leave_name' => $request->paid_leave_name,
            'company_id' => $company_id
        ]);

        return back()->with('store', 'Berhasil menambahkan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaidLeave  $paidLeave
     * @return \Illuminate\Http\Response
     */
    public function show(PaidLeave $paidLeave)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaidLeave  $paidLeave
     * @return \Illuminate\Http\Response
     */
    public function edit($id, PaidLeave $paidLeave)
    {
        $paid_leave = PaidLeave::find($id);

        return view('paid-leave.edit', compact(
            'paid_leave'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaidLeave  $paidLeave
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        PaidLeave::find($id)->update([
            'paid_leave_name' => $request->paid_leave_name
        ]);

        return redirect('/paid-leave/add-paid-leave')->with('update', 'Berhasil merubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaidLeave  $paidLeave
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, PaidLeave $paidLeave)
    {
        PaidLeave::find($id)->delete();
        return back()->with('delete', 'Berhasil menghapus data');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\PaidLeave;
use App\Models\PaidLeaveAccess;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\PaidLeaveUser;
use App\Models\PaidLeaveHistory;

class PaidLeaveHistoryApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // end_paid_leave_date, end_paid_leave_date, user_id, token, paid_leave_id, description

        $start_date           = date_create($request->start_paid_leave_date);
        $end_date             = date_create($request->end_paid_leave_date);
        $total_day_paid_leave = (date_diff($start_date, $end_date)->days + 1);
        $user                 = User::find($request->user_id);
        $paid_leave_id        = $request->paid_leave_id;
        $description          = $request->description;

        if($start_date && $end_date && $user && $paid_leave_id && $description){
            try{
                $paid_leave_user_model = new PaidLeaveUser;
                $paid_leave_user       = $paid_leave_user_model->where('user_id', $user->id)->first();
                $paid_leave_quota      = $paid_leave_user->paid_leave_quota;
        
                if($total_day_paid_leave > $paid_leave_quota){
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'quota is not enough', 'sorry, the number of leave you take is ' . $total_day_paid_leave . ' day. It exceeds the leave quota you have'
                    ], 200);
                }
        
                if($user->supervisor->first()){
                    PaidLeaveHistory::create([
                        'user_id' => $user->id,
                        'paid_leave_id' => $request->paid_leave_id,
                        'start_paid_leave_date' => $request->start_paid_leave_date,
                        'end_paid_leave_date' => $request->end_paid_leave_date,
                        'description' => $request->description,
                        'supervisor_id' => $user->supervisor->first()->supervisor_id
                    ]);
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Successfully submitted a paid leave request'
                    ], 200);
                }else{
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'Sorry, you dont have a supervisor yet. Please contact the related admin'
                    ], 200);
                }
            }catch(\Exception $e){
                $e->getMessage();
            }
        }else{
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = User::find($request->user_id);

        if($user){
            try{
                $paid_leaves = PaidLeaveAccess::where('user_id', $user->id)->get();
                return response()->json($paid_leaves);
            }catch(\Exception $e){
                $e->getMessage();
            }
        }else{
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth',
            ], 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

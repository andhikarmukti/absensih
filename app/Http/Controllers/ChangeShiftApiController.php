<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\ChangeShift;
use Illuminate\Http\Request;

class ChangeShiftApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //user_id, start_shift_date, end_shift_date, shift_id
        $user             = User::find($request->user_id);
        $start_shift_date = $request->start_shift_date;
        $end_shift_date   = $request->end_shift_date;
        $shift_id         = $request->shift_id;

        if($user && $start_shift_date && $end_shift_date && $shift_id){
            if($user->supervisor->first()){
                try{
                    ChangeShift::create([
                        'user_id' => $user->id,
                        'start_shift_date' => $start_shift_date,
                        'end_shift_date' => $end_shift_date,
                        'shift_id' => $shift_id,
                        'supervisor_id' => $user->supervisor->first()->supervisor_id
                    ]);
                    return response()->json([
                        'status' => 'success',
                        'message' => 'successful request change shift',
                    ], 200);
                }catch (\Exception $e){
                    $e->getMessage();
                }
            }else{
                return response()->json([
                    'status' => 'failed',
                    'message' => 'user tersebut belum memiliki supervisor untuk acc request',
                ]);
            }
        }else{
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth',
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

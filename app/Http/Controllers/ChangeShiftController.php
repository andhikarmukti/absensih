<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Shift;
use App\Models\ChangeShift;
use Illuminate\Http\Request;

class ChangeShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shifts_company = auth()->user()->company->shift;

        return view('changeshift.create', compact(
            'shifts_company'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ChangeShift::create([
            'user_id' => auth()->user()->id,
            'start_shift_date' => $request->start_shift_date,
            'end_shift_date' => $request->end_shift_date,
            'shift_id' => $request->shift_id,
            'supervisor_id' => auth()->user()->supervisor->first()->supervisor_id
        ]);
        return back()->with('berhasil', 'Berhasil mengajukan change shift');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChangeShift  $changeShift
     * @return \Illuminate\Http\Response
     */
    public function show(ChangeShift $changeShift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChangeShift  $changeShift
     * @return \Illuminate\Http\Response
     */
    public function edit(ChangeShift $changeShift)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ChangeShift  $changeShift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChangeShift $changeShift)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChangeShift  $changeShift
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChangeShift $changeShift)
    {
        //
    }

    public function list_request_change_shift()
    {
        $list_request_change_shift = ChangeShift::where('supervisor_id', auth()->user()->id)
            ->where('approval', 'pending')
            ->get();
        $shift = new Shift;

        return view('changeshift.list_request_change_shift', compact(
            'list_request_change_shift',
            'shift'
        ));
    }
}

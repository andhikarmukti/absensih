<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Entity;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $entities    = auth()->user()->company->entity;
        $departments = Department::join('entities', 'departments.entity_id', '=', 'entities.id')
                                    ->join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
                                    ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
                                    ->select('departments.*', 'entities.entity_name')
                                    ->where('companies.id', auth()->user()->company->id)
                                    ->get();

        return view('department.create', compact(
            'entities',
            'departments'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Department::create([
            'entity_id' => $request->entity_id,
            'department_name' => $request->department_name,
        ]); return back()->with('berhasil', 'Berhasil menambahkan department baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Department $department)
    {
        $department  = Department::find($id);
        $entities    = auth()->user()->company->entity;

        return view('department.edit', compact(
            'id',
            'department',
            'entities'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Department $department)
    {
        $department->find($id)->update([
            'entity_id' => $request->entity_id,
            'department_name' => $request->department_name
        ]);
        return redirect('/department')->with('udpate', 'Berhasil merubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Department $department)
    {
        $department = $department->where('id', $id)->first();
        foreach($department->user as $du){
            User::where('email', $du->email)->delete();
        }
        $department->delete();

        return back()->with('delete', 'Berhasil hapus data');
    }
}

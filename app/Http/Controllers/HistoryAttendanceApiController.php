<?php

namespace App\Http\Controllers;

use DateTime;
use DatePeriod;
use DateInterval;
use App\Models\User;
use App\Models\Shift;
use App\Models\Overtime;
use App\Models\ChangeShift;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\HistoryAttendance;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class HistoryAttendanceApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // image, user_id, latitude, longitude, token

        $image = $request->image;
        $user = User::find($request->user_id);
        $latitude2 = $request->latitude;
        $longitude2 = $request->longitude;

        if ($image && $user && $latitude2 && $longitude2) {
            try {
                // pengecekan apabila tidak melakukan foto
                if ($image == null) {
                    return back()->with('noImage', 'Maaf, silahkan lakukan pengambilan gambar terlebih dahulu');
                }

                // pengecekan apakah hari ini adalah libur nasional, dan apakah user yang submit mendapati libur?
                $attendance_check_today = $user->historyattendance()->where('description', 'holiday national')->whereDate('checkin_time', today())->get()->count();
                if ($attendance_check_today > 0) {
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'cant take attendance, because today is a national holiday'
                    ], 200);
                }
                // pengecekan apakah hari ini ada cuti
                $attendance_check_today = $user->historyattendance()->where('description', 'paid leave')->whereDate('checkin_time', today())->get()->count();
                if ($attendance_check_today > 0) {
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'cant do attendance, because today you are on leave'
                    ], 200);
                }

                // upload data base64
                $img = $request->image;
                $folderPath = "storage/img/";
                $image_parts = explode(";base64,", $img);
                $image_base64 = base64_decode($image_parts[1]);
                $fileName = uniqid() . '.png';
                $file = $folderPath . $fileName;

                //pengecekan apakah sudah ada checkin hari ini?
                $attendance_check_today = $user->historyattendance()->whereDate('checkin_time', today())->get()->count();

                if ($attendance_check_today > 0) {
                    // Cek apakah kolom deskripsi sudah terisi selain late dan on time? agar user tidak bisa absen ketika libur nasiona, cuti, atau bolos kerja
                    $description = $user->historyattendance()->whereDate('checkin_time', today())->first()->description;
                    if ($description == 'absent from work') {
                        return response()->json([
                            'status' => 'failed',
                            'message' => 'Sorry you have been absent from work today'
                        ], 200);
                    }
                    if ($description == 'holiday national') {
                        return response()->json([
                            'status' => 'failed',
                            'message' => 'Today is a national holiday'
                        ], 200);
                    }
                    if ($description == 'paid leave') {
                        return response()->json([
                            'status' => 'failed',
                            'message' => 'You are on paid leave today'
                        ], 200);
                    }
                    if ($description == 'shift off') {
                        return response()->json([
                            'status' => 'failed',
                            'message' => 'You are on holiday schedule today'
                        ], 200);
                    }

                    // menghitung selisih waktu checkin terakhir dengan checkout saat ini sebagai nilai dari total_working_hours
                    $difference_time = date_diff(date_create($user->historyattendance->last()->checkin_time), date_create(Carbon::now()));
                    $total_working_hours = $difference_time->h . ':' . $difference_time->i . ':' . $difference_time->s;

                    // pengecekan apakah ada foto checkout yang lama? jika ada, maka hapus dulu foto checkout yang lama
                    $check_photo_checkout = $user->historyattendance()->whereDate('checkin_time', today())->get()->first();
                    if ($check_photo_checkout->checkout_image != 'storage/img/default.jpg') {
                        Storage::delete('img/' . explode('/', $check_photo_checkout->checkout_image)[2]);
                    }

                    file_put_contents($file, $image_base64);
                    $user->historyattendance()->whereDate('checkin_time', today())->get()->first()->update([
                        'checkout_time' => Carbon::now(),
                        'total_working_hours' => $total_working_hours,
                        'checkout_image' => $file,
                        'latitude_checkout' => $latitude2,
                        'longitude_checkout' => $longitude2,
                        'description' => ""
                    ]);

                    // Pengecekan apakah sudah mencapai total working hours atau belum ketika melakukan checkout
                    $today = Carbon::now()->format('l');
                    $shiftID_today = $user->shiftschedule->$today;
                    $totalWorkingHours = $user->historyattendance()->whereDate('checkin_time', today())->get()->first()->total_working_hours;

                    if (date_format(date_create($totalWorkingHours), 'H:i:s') < Shift::find($shiftID_today)->total_working_hours) {
                        $user->historyattendance()->whereDate('checkin_time', today())->get()->first()->update([
                            'description' => "jam kerja kurang dari " . Shift::find($shiftID_today)->total_working_hours . " jam",
                        ]);
                        return response()->json([
                            'status' => 'success',
                            'message' => 'successful attendance checkout submit',
                            'warning' => 'you have been working ' . $totalWorkingHours . ' hours, your working hours are still less than ' . Shift::find($shiftID_today)->total_working_hours . ' hours'
                        ], 200);
                    }

                    return response()->json([
                        'status' => 'success',
                        'message' => 'successful attendance checkout submit'
                    ], 200);
                } else {
                    file_put_contents($file, $image_base64);

                    // cek apakah ada perubahan shift hari itu untuk user id yang absen?
                    $check_shift_change_today = $user->changeshift->where('approval', 'accept');
                    foreach ($check_shift_change_today as $cs_today) {
                        $period = new DatePeriod(
                            new DateTime($cs_today->start_shift_date),
                            new DateInterval('P1D'),
                            new DateTime(date('Y-m-d', strtotime($cs_today->end_shift_date . "+ 1 day")))
                        );
                        foreach ($period as $p) {
                            if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {

                                // pengecekan untuk nilai column arrival_status
                                $shift = Shift::where('id', $cs_today->shift_id)->first();
                                // dd($shift->id);
                                if ($shift->start_shift_hours > Carbon::now()->format('H:i:s')) {
                                    $arrival_status = 'on time';
                                } else {
                                    $arrival_status = 'late';
                                }
                                if ($shift->start_shift_hours == null) {
                                    $arrival_status = 'on time';
                                }
                                //store to database (checkin)
                                HistoryAttendance::create([
                                    'user_id' => $user->id,
                                    'shift_id' => $shift->id,
                                    'checkin_time' => Carbon::now(),
                                    'checkin_image' => $file,
                                    'arrival_status' => $arrival_status,
                                    'latitude_checkin' => $latitude2,
                                    'longitude_checkin' => $longitude2
                                ]);
                                ChangeShift::where('end_shift_date', Carbon::today()->format('Y-m-d'))->update([
                                    'approval' => 'done'
                                ]);
                                return response()->json([
                                    'status' => 'success',
                                    'message' => 'successful attendance checkin submit'
                                ], 200);
                            }
                        }
                    }

                    // cek apakah user tersebut sudah memiliki shift schedule atau belum?
                    if (!$user->shiftschedule) {
                        return response()->json([
                            'status' => 'failed',
                            'message' => 'The user doesnt have a shift schedule yet'
                        ], 401);
                    }

                    // ambil tiap tanggal berdasarkan start dan end date yang telah ditentukan di table shift schedule table
                    $shift_schedule = $user->shiftschedule;
                    $start_shift_pattern_date = $shift_schedule->start_shift_pattern_date;
                    $end_shift_pattern_date = $shift_schedule->end_shift_pattern_date;
                    $period = new DatePeriod(
                        new DateTime($start_shift_pattern_date),
                        new DateInterval('P1D'),
                        new DateTime(date('Y-m-d', strtotime($end_shift_pattern_date . "+ 1 day")))
                    );
                    // cek jadwal shift berdasarkan shift schedule
                    foreach ($period as $p) {
                        if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {

                            // pengecekan untuk nilai column arrival_status
                            // pengecekan juga shift id berapa yang digunakan pada hari ini
                            $user_shiftschedule = $user->shiftschedule;
                            $check_today = Carbon::today()->isoFormat('dddd');
                            $user_shift = Shift::where('id', $user_shiftschedule->$check_today)->first();
                            if ($user_shift->start_shift_hours > Carbon::now()->format('H:i:s')) {
                                $arrival_status = 'on time';
                            } else {
                                $arrival_status = 'late';
                            }
                            if ($user_shift->start_shift_hours == null) {
                                $arrival_status = 'on time';
                            }

                            // pengecekan apakah hari ini checkin lembur atau bukan
                            $check_overtime = Overtime::where('user_id', $user->id)->whereMonth('start_overtime_date', Carbon::today())->whereYear('start_overtime_date', Carbon::today())->where('approval', 'accept')->get();
                            foreach ($check_overtime as $co_today) {
                                $period = new DatePeriod(
                                    new DateTime($co_today->start_overtime_date),
                                    new DateInterval('P1D'),
                                    new DateTime(date('Y-m-d', strtotime($co_today->end_overtime_date . "+ 1 day")))
                                );
                                foreach ($period as $p) {
                                    if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {
                                        // pengecekan untuk nilai column arrival_status
                                        if ($co_today->start_overtime_hour > Carbon::now()->format('H:i:s')) {
                                            $arrival_status = 'on time';
                                        } else {
                                            $arrival_status = 'late';
                                        }
                                        $description = 'overtime';

                                        //store to database (checkin)
                                        HistoryAttendance::create([
                                            'user_id' => $user->id,
                                            'checkin_time' => Carbon::now(),
                                            'checkin_image' => $file,
                                            'arrival_status' => $arrival_status,
                                            'description' => $description
                                        ]);
                                        return response()->json([
                                            'status' => 'success',
                                            'message' => 'successful attendance checkin submit'
                                        ], 200);
                                    }
                                }
                            }

                            // Cek apakah shiftnya adalah shift fleksibel?
                            $today = Carbon::now()->format('l');
                            $shiftID_today = $user->shiftschedule->$today;

                            if (Shift::find($shiftID_today)->is_flexible == "on") {
                                HistoryAttendance::create([
                                    'user_id' => $user->id,
                                    'shift_id' => $shiftID_today,
                                    'checkin_time' => Carbon::now(),
                                    'checkin_image' => $file,
                                    'arrival_status' => "on time",
                                    'description' => "flexible " . Shift::find($shiftID_today)->total_working_hours
                                ]);
                                return response()->json([
                                    'status' => 'success',
                                    'message' => 'successful attendance checkin submit'
                                ], 200);
                            }

                            //store to database (checkin)
                            HistoryAttendance::create([
                                'user_id' => $user->id,
                                'shift_id' => $user_shiftschedule->$check_today,
                                'checkin_time' => Carbon::now(),
                                'checkin_image' => $file,
                                'arrival_status' => $arrival_status,
                                'latitude_checkin' => $latitude2,
                                'longitude_checkin' => $longitude2,
                            ]);
                            return response()->json([
                                'status' => 'success',
                                'message' => 'successful attendance checkin submit'
                            ], 200);
                        }
                    }
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'Sorry, your shift schedule has expired. Please contact admin'
                    ], 200);
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            return response()->json([
                'status' => 'bad auth'
            ], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // user_id

        $user_id = $request->user_id;

        if ($user_id) {
            try {
                $history_attendance = HistoryAttendance::join('shifts', 'history_attendances.shift_id', '=', 'shifts.id')
                    ->where('user_id', $user_id)
                    ->orderBy('checkin_time', 'DESC')
                    ->select('shifts.*', 'history_attendances.*')
                    ->limit(5)
                    ->get();
                return response()->json($history_attendance, 200);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ], 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

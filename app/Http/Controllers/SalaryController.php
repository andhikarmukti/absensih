<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Salary;
use Illuminate\Http\Request;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $salaries = auth()->user()->company->salary;

        return view('salary.create', compact(
            'salaries'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Salary::create([
            'company_id' => auth()->user()->company->id,
            'salary_name' => $request->salary_name,
            'salary_value' => str_replace('.', '', $request->salary_value),
        ]); return back()->with('berhasil', 'Berhasil menambahkan salary baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function show(Salary $salary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Salary $salary)
    {
        $salary = Salary::find($id);

        return view('salary.edit', compact(
            'salary'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Salary $salary)
    {
        Salary::find($id)->update([
            'salary_name' => $request->salary_name,
            'salary_value' => str_replace(".", "", $request->salary_value)
        ]);

        return redirect('/salary')->with('update', 'Berhasil update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Salary $salary)
    {
        Salary::find($id)->delete();
        User::where('salary_id', $id)->update([
            'salary_id' => null
        ]);

        return back()->with('delete', 'Berhasil menghapus data salary');
    }
}

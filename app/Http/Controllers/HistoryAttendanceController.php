<?php

namespace App\Http\Controllers;

use DateTime;
use DataTables;
use DatePeriod;
use DateInterval;
use App\Models\User;
use App\Models\Shift;
use App\Models\Company;
use App\Models\Holiday;
use App\Models\Overtime;
use App\Models\ChangeShift;
use Illuminate\Http\Request;
use App\Models\ShiftSchedule;
use Illuminate\Support\Carbon;
use App\Models\HistoryAttendance;
use Illuminate\Support\Facades\DB;
use App\Models\AttendanceCorrection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\DeduplicationHandler;
use Psy\Command\HistoryCommand;

class HistoryAttendanceController extends Controller
{
    private $user;
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();

            return $next($request);
        });
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('attendance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        // pengecekan apabila tidak melakukan foto
        if ($request->image == null) {
            return back()->with('noImage', 'Maaf, silahkan lakukan pengambilan gambar terlebih dahulu');
        }

        // pengecekan apakah hari ini adalah libur nasional, dan apakah user yang submit mendapati libur?
        $attendance_check_today = $this->user->historyattendance()->where('description', 'holiday national')->whereDate('checkin_time', today())->get()->count();
        if ($attendance_check_today > 0) {
            return 'tidak bisa melakukan absensi, karena hari ini adalah hari libur nasional. Atau apakah anda lembur hari ini?';
        }

        // pengecekan apakah hari ini ada cuti
        $attendance_check_today = $this->user->historyattendance()->where('description', 'paid leave')->whereDate('checkin_time', today())->get()->count();
        if ($attendance_check_today > 0) {
            return back()->with('sedang cuti', 'Maaf Anda sedang cuti hari ini');
        }

        // upload data base64
        $img = $request->image;
        $folderPath = "storage/img/";
        $image_parts = explode(";base64,", $img);
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.png';
        $file = $folderPath . $fileName;

        // pengecekan apakah sudah ada checkin hari ini? jika sudah maka langsung checkout
        $attendance_check_today = $this->user->historyattendance()->whereDate('checkin_time', today())->get()->count();

        // Cek apakah kolom deskripsi sudah terisi selain late dan on time? agar user tidak bisa absen ketika libur nasiona, cuti, atau bolos kerja
        if ($attendance_check_today > 0) {
            $description = $this->user->historyattendance()->whereDate('checkin_time', today())->first()->description;
            if ($description == 'absent from work') {
                return back()->with('tidak bisa absen', 'Maaf Anda sudah absent from work hari ini');
            }
            if ($description == 'holiday national') {
                return back()->with('tidak bisa absen', 'Hari ini adalah libur nasional');
            }
            if ($description == 'paid leave') {
                return back()->with('tidak bisa absen', 'Anda sedang cuti hari ini');
            }
            if ($description == 'shift off') {
                return back()->with('tidak bisa absen', 'Anda sedang jadwal libur hari ini');
            }

            // menghitung selisih waktu checkin terakhir dengan checkout saat ini sebagai nilai dari total_working_hours
            $difference_time = date_diff(date_create($this->user->historyattendance->last()->checkin_time), date_create(Carbon::now()));
            $total_working_hours = $difference_time->h . ':' . $difference_time->i . ':' . $difference_time->s;

            // pengecekan apakah ada foto checkout yang lama? jika ada, maka hapus dulu foto checkout yang lama
            $check_photo_checkout = $this->user->historyattendance()->whereDate('checkin_time', today())->get()->first();
            if ($check_photo_checkout->checkout_image != 'storage/img/default.jpg') {
                Storage::delete('img/' . explode('/', $check_photo_checkout->checkout_image)[2]);
            }

            file_put_contents($file, $image_base64);

            $this->user->historyattendance()->whereDate('checkin_time', today())->get()->first()->update([
                'checkout_time' => Carbon::now(),
                'total_working_hours' => $total_working_hours,
                'checkout_image' => $file
            ]);

            // Pengecekan apakah sudah mencapai total working hours atau belum ketika melakukan checkout
            $today = Carbon::now()->format('l');
            $shiftID_today = $this->user->shiftschedule->$today;
            $totalWorkingHours = $this->user->historyattendance()->whereDate('checkin_time', today())->get()->first()->total_working_hours;

            if (date_format(date_create($totalWorkingHours), 'H:i:s') < Shift::find($shiftID_today)->total_working_hours) {
                $this->user->historyattendance()->whereDate('checkin_time', today())->get()->first()->update([
                    'description' => "jam kerja kurang dari " . Shift::find($shiftID_today)->total_working_hours . " jam",
                ]);
                return back()->with('jam kerja kurang', 'you have been working,' . $totalWorkingHours . ' your working hours are still less than' . Shift::find($shiftID_today)->total_working_hours . ' hour');
            }

            return back()->with('checkout', 'Berhasil melakukan checkout');
        } else {
            file_put_contents($file, $image_base64);

            // cek apakah ada perubahan shift hari itu untuk user id yang absen?
            $check_shift_change_today = $this->user->changeshift->where('approval', 'accept');
            foreach ($check_shift_change_today as $cs_today) {
                $period = new DatePeriod(
                    new DateTime($cs_today->start_shift_date),
                    new DateInterval('P1D'),
                    new DateTime(date('Y-m-d', strtotime($cs_today->end_shift_date . "+ 1 day")))
                );
                foreach ($period as $p) {
                    if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {

                        // pengecekan untuk nilai column arrival_status
                        $shift = Shift::where('id', $cs_today->shift_id)->first();
                        if ($shift->start_shift_hours > Carbon::now()->format('H:i:s')) {
                            $arrival_status = 'on time';
                        } else {
                            $arrival_status = 'late';
                        }
                        if ($shift->start_shift_hours == null) {
                            $arrival_status = 'on time';
                        }

                        //store to database (checkin)
                        HistoryAttendance::create([
                            'user_id' => $this->user->id,
                            'shift_id' => $shift->id,
                            'checkin_time' => Carbon::now(),
                            'checkin_image' => $file,
                            'arrival_status' => $arrival_status
                        ]);
                        ChangeShift::where('end_shift_date', Carbon::today()->format('Y-m-d'))->update([
                            'approval' => 'done'
                        ]);
                        return back()->with('checkin', 'Berhasil melakukan checkin');
                    }
                }
            }

            // cek apakah user tersebut sudah memiliki shift schedule atau belum?
            if (!$this->user->shiftschedule) {
                return back()->with('tidak ada shift schedule', 'maaf, kamu belum memiliki shift schedule. Silahkan hubungi admin kantor');
            }

            // ambil tiap tanggal berdasarkan start dan end date yang telah ditentukan di table shift schedule table
            $shift_schedule = $this->user->shiftschedule;
            $start_shift_pattern_date = $shift_schedule->start_shift_pattern_date;
            $end_shift_pattern_date = $shift_schedule->end_shift_pattern_date;
            $period = new DatePeriod(
                new DateTime($start_shift_pattern_date),
                new DateInterval('P1D'),
                new DateTime(date('Y-m-d', strtotime($end_shift_pattern_date . "+ 1 day")))
            );
            // cek jadwal shift berdasarkan shift schedule
            foreach ($period as $p) {
                if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {

                    // pengecekan untuk nilai column arrival_status
                    // pengecekan juga shift id berapa yang digunakan pada hari ini
                    $user_shiftschedule = $this->user->shiftschedule;
                    $check_today = Carbon::today()->isoFormat('dddd');
                    $user_shift = Shift::where('id', $user_shiftschedule->$check_today)->first();
                    if ($user_shift->start_shift_hours > Carbon::now()->format('H:i:s')) {
                        $arrival_status = 'on time';
                    } else {
                        $arrival_status = 'late';
                    }
                    if ($user_shift->start_shift_hours == null) {
                        $arrival_status = 'on time';
                    }

                    // pengecekan apakah hari ini checkin lembur atau bukan
                    $check_overtime = Overtime::where('user_id', auth()->user()->id)->whereMonth('start_overtime_date', Carbon::today())->whereYear('start_overtime_date', Carbon::today())->where('approval', 'accept')->get();
                    foreach ($check_overtime as $co_today) {
                        $period = new DatePeriod(
                            new DateTime($co_today->start_overtime_date),
                            new DateInterval('P1D'),
                            new DateTime(date('Y-m-d', strtotime($co_today->end_overtime_date . "+ 1 day")))
                        );
                        foreach ($period as $p) {
                            if ($p->format('Y-m-d') == Carbon::today()->format('Y-m-d')) {
                                // pengecekan untuk nilai column arrival_status
                                if ($co_today->start_overtime_hour > Carbon::now()->format('H:i:s')) {
                                    $arrival_status = 'on time';
                                } else {
                                    $arrival_status = 'late';
                                }
                                $description = 'overtime';

                                //store to database (checkin)
                                HistoryAttendance::create([
                                    'user_id' => $this->user->id,
                                    'checkin_time' => Carbon::now(),
                                    'checkin_image' => $file,
                                    'arrival_status' => $arrival_status,
                                    'description' => $description
                                ]);
                                return back()->with('checkin', 'Berhasil melakukan checkin');
                            }
                        }
                    }

                    // Cek apakah shiftnya adalah shift fleksibel?
                    $today = Carbon::now()->format('l');
                    $shiftID_today = $this->user->shiftschedule->$today;

                    if (Shift::find($shiftID_today)->is_flexible == "on") {
                        HistoryAttendance::create([
                            'user_id' => $this->user->id,
                            'shift_id' => $shiftID_today,
                            'checkin_time' => Carbon::now(),
                            'checkin_image' => $file,
                            'arrival_status' => "flexible",
                            'description' => "flexible " . Shift::find($shiftID_today)->total_working_hours
                        ]);
                        return back()->with('checkin', 'Berhasil melakukan checkin');
                    }

                    //store to database (checkin)
                    HistoryAttendance::create([
                        'user_id' => $this->user->id,
                        'shift_id' => $user_shiftschedule->$check_today,
                        'checkin_time' => Carbon::now(),
                        'checkin_image' => $file,
                        'arrival_status' => $arrival_status
                    ]);
                    return back()->with('checkin', 'Berhasil melakukan checkin');;
                }
            }
            return 'maaf, shift schedule anda expired. Silahkan hubungi admin.';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HistoryAttendance  $historyAttendance
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $history_attendance = new HistoryAttendance;
        $attendances        = $history_attendance->company($this->user->company->id);
        $companies          = Company::all();
        $company_id         = $request->company_id;

        if ($request->company_id) {
            if (auth()->user()->role != 'superadmin') {
                return abort(403);
            }
            $attendances = $history_attendance->company($company_id);
        }

        return view('attendance.show', compact([
            'attendances',
            'companies'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HistoryAttendance  $historyAttendance
     * @return \Illuminate\Http\Response
     */
    public function edit(HistoryAttendance $historyAttendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HistoryAttendance  $historyAttendance
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $user = User::find($request->user_id);
        // pengecekan untuk nilai column arrival_status
        $history_attendance = HistoryAttendance::where('id', $request->history_attendance_id)->first();

        // cek apakah ada perubahan shift hari itu untuk user id yang absen?
        $check_shift_change_today = ChangeShift::where('user_id', $request->user_id)->whereMonth('created_at', today())->whereYear('created_at', today())->where('approval', 'accept')->get();
        if ($check_shift_change_today->first()) {
            foreach ($check_shift_change_today as $cs_today) {
                $period = new DatePeriod(
                    new DateTime($cs_today->start_shift_date),
                    new DateInterval('P1D'),
                    new DateTime(date('Y-m-d', strtotime($cs_today->end_shift_date . "+ 1 day")))
                );
                foreach ($period as $p) {
                    if ($p->format('Y-m-d') == date_format(date_create($history_attendance->checkin_time), 'Y-m-d')) {
                        // pengecekan untuk nilai column arrival_status
                        $shift = Shift::where('id', $cs_today->shift_id)->first();
                        if ($shift->start_shift_hours > $request->checkin_time_correction) {
                            $arrival_status = 'on time';
                        } else {
                            $arrival_status = 'late';
                        }
                        if ($shift->start_shift_hours == null) {
                            $arrival_status = 'on time';
                        }
                        // explode nilai time pada column checkin_time
                        $checkin_date        = date('Y-m-d', strtotime($history_attendance->checkin_time));
                        $checkin_time_update = $checkin_date . " " . $request->checkin_time_correction;

                        // menghitung selisih waktu checkin terakhir dengan checkout saat ini sebagai nilai dari total_working_hours
                        $difference_time = date_diff(date_create($request->checkin_time_correction), date_create($history_attendance->checkout_time));
                        $total_working_hours = $difference_time->h . ':' . $difference_time->i . ':' . $difference_time->s;

                        HistoryAttendance::find($request->history_attendance_id)->update([
                            'total_working_hours' => $total_working_hours,
                            'checkin_time' => $checkin_time_update,
                            'arrival_status' => $arrival_status . '*',
                            'shift_id' => $cs_today->shift_id
                        ]);
                        AttendanceCorrection::find($id)->update([
                            'approval' => 'accept'
                        ]);
                        return back()->with('berhasil', 'Berhasil accept request');
                    }
                }
            }
        }

        $shift_schedule = $user->shiftschedule;
        $start_shift_pattern_date = $shift_schedule->start_shift_pattern_date;
        $end_shift_pattern_date = $shift_schedule->end_shift_pattern_date;
        $period = new DatePeriod(
            new DateTime($start_shift_pattern_date),
            new DateInterval('P1D'),
            new DateTime(date('Y-m-d', strtotime($end_shift_pattern_date . "+ 1 day")))
        );
        // cek jadwal shift berdasarkan shift schedule
        foreach ($period as $p) {
            if ($p->format('Y-m-d') == date_format(date_create($history_attendance->checkin_time), 'Y-m-d')) {

                // pengecekan untuk nilai column arrival_status
                // pengecekan juga shift id berapa yang digunakan pada hari ini
                $user_shiftschedule = HistoryAttendance::find($request->history_attendance_id)->shift_id;
                // $check_today = Carbon::today()->isoFormat('dddd');
                $user_shift = Shift::where('id', $user_shiftschedule)->first();
                if ($user_shift->start_shift_hours > $request->checkin_time_correction) {
                    $arrival_status = 'on time';
                } else {
                    $arrival_status = 'late';
                }
                if ($user_shift->start_shift_hours == null) {
                    $arrival_status = 'on time';
                }
                // explode nilai time pada column checkin_time
                $checkin_date        = date('Y-m-d', strtotime($history_attendance->checkin_time));
                $checkin_time_update = $checkin_date . " " . $request->checkin_time_correction;

                // menghitung selisih waktu checkin terakhir dengan checkout saat ini sebagai nilai dari total_working_hours
                $difference_time = date_diff(date_create($request->checkin_time_correction), date_create($history_attendance->checkout_time));
                $total_working_hours = $difference_time->h . ':' . $difference_time->i . ':' . $difference_time->s;
                HistoryAttendance::find($request->history_attendance_id)->update([
                    'total_working_hours' => $total_working_hours,
                    'checkin_time' => $checkin_time_update,
                    'arrival_status' => $arrival_status
                ]);
                AttendanceCorrection::find($id)->update([
                    'approval' => 'accept'
                ]);
                return back()->with('berhasil', 'Berhasil accept request');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HistoryAttendance  $historyAttendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(HistoryAttendance $historyAttendance)
    {
        //
    }

    public function historyAttendanceV2(Request $request)
    {
        $histories = HistoryAttendance::join('users', 'history_attendances.user_id', '=', 'users.id')
            ->join('shifts', 'history_attendances.shift_id', '=', 'shifts.id')
            ->select('history_attendances.*', 'users.full_name', 'shifts.shift_name')
            ->get();

        return view('admin-absensih.historyAttendanceV2', compact(
            'histories'
        ));
    }
}

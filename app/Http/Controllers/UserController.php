<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Shift;
use App\Models\Salary;
use App\Models\Company;
use App\Models\JobLevel;
use Illuminate\Http\Request;
use App\Models\ShiftSchedule;
use App\Models\OfficePosition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches         = auth()->user()->company->branch;
        $entities         = auth()->user()->company->entity;
        $departments      = auth()->user()->company->department(auth()->user()->company->id);
        $user_companies   = auth()->user()->company->user(auth()->user()->company->id);
        $office_positions = OfficePosition::where('company_id', auth()->user()->company->id)->get();
        $job_levels       = JobLevel::orderBy('level_value', 'ASC')->where('company_id', auth()->user()->company->id)->get();

        if (auth()->user()->role == 'superadmin') {
            $user_companies   = User::all();
        }

        return view('user.create', compact(
            'branches',
            'entities',
            'departments',
            'user_companies',
            'office_positions',
            'job_levels'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $password      = 'absensih';

        User::create([
            'full_name'             => $request->full_name,
            'username'              => $request->username,
            'email'                 => $request->email,
            'department_id'         => $request->department_id,
            'office_position_id'    => $request->office_position_id,
            'password'              => Hash::make($password),
            'office_position_id'    => $request->office_position_id,
            'job_level_id'          => $request->job_level_id,
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan user baru ( ' . $request->full_name . ' )');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /////////////////////////////////////////

    public function add_shift_schedule_create(Request $request)
    {
        $company            = Company::where('id', auth()->user()->company->id)->get()->first();
        $users              = $company->user(auth()->user()->company->id);
        $shiftSchedules     = ShiftSchedule::where('company_id', auth()->user()->company->id)->get();
        $shiftScheduleLists = User::join('shift_schedules', 'users.shift_schedule_id', '=', 'shift_schedules.id')
            ->select('users.*', 'shift_schedules.*')
            ->where('shift_schedules.company_id', '=', auth()->user()->company->id)
            ->get();

        if ($request->shift_schedule_id) {
            $shift_schedule = ShiftSchedule::where('id', $request->shift_schedule_id)->get()->first();
            $shift = new Shift;
            return response()->json([
                'shift_schedule' => $shift_schedule,
                'monday' => $shift->where('id', $shift_schedule->Monday)->first()->shift_name,
                'tuesday' => $shift->where('id', $shift_schedule->Tuesday)->first()->shift_name,
                'wednesday' => $shift->where('id', $shift_schedule->Wednesday)->first()->shift_name,
                'thursday' => $shift->where('id', $shift_schedule->Thursday)->first()->shift_name,
                'friday' => $shift->where('id', $shift_schedule->Friday)->first()->shift_name,
                'saturday' => $shift->where('id', $shift_schedule->Saturday)->first()->shift_name,
                'sunday' => $shift->where('id', $shift_schedule->Sunday)->first()->shift_name,
            ]);
        }

        return view('user.add-shift-schedule', compact(
            'users',
            'shiftSchedules',
            'shiftScheduleLists'
        ));
    }

    public function add_shift_schedule_store(Request $request)
    {
        $user_full_name = User::where('id', $request->user_id)->first()->full_name;

        User::find($request->user_id)->update([
            'shift_schedule_id' => $request->shift_schedule_id
        ]);
        return back()->with('berhasil', 'Data shift schedule berhasil ditambahkan ke user ' . $user_full_name);
    }

    public function add_salary_create(Request $request)
    {
        $users         = auth()->user()->company->user(auth()->user()->company->id);
        $salaries      = auth()->user()->company->salary;
        $user_salaries = User::join('salaries', 'users.salary_id', '=', 'salaries.id')
            ->join('departments', 'departments.id', '=', 'users.department_id')
            ->join('entities', 'entities.id', '=', 'departments.entity_id')
            ->join('branch_companies', 'branch_companies.id', '=', 'entities.branch_id')
            ->join('companies', 'companies.id', '=', 'branch_companies.company_id')
            ->where('companies.id', auth()->user()->company->id)
            ->select('users.full_name', 'salaries.*')
            ->get();

        // jquery
        if ($request->salary_id) {
            $nominal = Salary::where('id', $request->salary_id)->first();
            return response()->json($nominal);
        }

        return view('user.add-salary', compact(
            'users',
            'salaries',
            'user_salaries'
        ));
    }

    public function add_salary_store(Request $request)
    {
        User::find($request->user_id)->update([
            'salary_id' => $request->salary_id
        ]);
        return back()->with('berhasil', 'Berhasil update data salary pada user');
    }

    public function edit_profile()
    {
        return view('user.edit-profile');
    }

    public function edit_profile_update(Request $request)
    {
        if (Auth::attempt(['username' => auth()->user()->username, 'password' => $request->old_password])) {
            User::find(auth()->user()->id)->update([
                'password' => Hash::make($request->new_password)
            ]);
            return back()->with('berhasil', 'Berhasil merubah password');
        } else {
            return back()->with('gagal', 'Pastikan password lama kamu sudah benar');
        }
    }

    public function create_job_level()
    {
        $users           = auth()->user()->company->user(auth()->user()->company->id);
        $job_levels      = JobLevel::where('company_id', auth()->user()->company->id)->get();
        $user_job_levels = User::join('job_levels', 'users.job_level_id', '=', 'job_levels.id')
            ->where('job_levels.company_id', '=', auth()->user()->company->id)
            ->get();

        return view('user.add-job-level', compact(
            'users',
            'job_levels',
            'user_job_levels'
        ));
    }

    public function store_job_level(Request $request)
    {
        $user = User::find($request->user_id);

        User::find($request->user_id)->update([
            'job_level_id' => $request->job_level_id
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan job level ke user ' . $user->full_name);
    }

    public function delete_shift_schedule_create($email)
    {
        User::where('email', $email)->update([
            'shift_schedule_id' => null
        ]);

        return back()->with('delete', 'Berhasil menghapus data');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Reimburse;
use Illuminate\Http\Request;

class ReimburseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reimburse.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $photo_proof = $request->file('photo_proof')->store('img/reimburse');
        Reimburse::create([
            'user_id'           => auth()->user()->id,
            'reimburse_date'    => $request->reimburse_date,
            'reimburse_value'   => str_replace(".", "", $request->reimburse_value),
            'photo_proof'       => $photo_proof,
            'description'       => $request->description,
            'supervisor_id'     => auth()->user()->supervisor->first()->supervisor_id,
        ]); return back()->with('berhasil', 'Berhasil mengajukan reimburse');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reimburse  $reimburse
     * @return \Illuminate\Http\Response
     */
    public function show(Reimburse $reimburse, User $user)
    {
        $list_reimburses = $reimburse->where('approval', 'pending')->where('supervisor_id', auth()->user()->supervisor->first()->supervisor_id)->get();

        return view('reimburse.show', compact(
            'list_reimburses',
            'user'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reimburse  $reimburse
     * @return \Illuminate\Http\Response
     */
    public function edit(Reimburse $reimburse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reimburse  $reimburse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reimburse $reimburse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reimburse  $reimburse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reimburse $reimburse)
    {
        //
    }
}

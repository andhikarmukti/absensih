<?php

namespace App\Http\Controllers;

use App\Models\JobLevel;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\OrganizationalStructure;
use Illuminate\Contracts\Queue\Job;

class OrganizationalStructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users_company            = auth()->user()->company->user(auth()->user()->company->id);
        $organizationalStructures = OrganizationalStructure::join('users', 'organizational_structures.user_id', '=', 'users.id')
            ->join('departments', 'users.department_id', '=', 'departments.id')
            ->join('entities', 'departments.entity_id', '=', 'entities.id')
            ->join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
            ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
            ->select('companies.id', 'organizational_structures.*', 'users.full_name')
            ->where('companies.id', auth()->user()->company->id)
            ->get();
        // dd($organizationalStructures);
        $supervisor = new User;

        return view('organizational-structure.create', compact(
            'users_company',
            'organizationalStructures',
            'supervisor'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user                = User::find($request->user_id);
        $supervisor_id_level = User::find($request->supervisor_id);

        if ($user->joblevel == null) {
            return back()->with('kuning', 'Maaf, user ' . $user->full_name . ' belum memiliki job level. Silahkan tambahkan job level ');
        } else {
            if ($supervisor_id_level->joblevel == null) {
                return back()->with('supervisor level error', 'The selected supervisor does not yet have a job level');
            }
            $difference_level    = $user->joblevel->level_value - $supervisor_id_level->joblevel->level_value;
        }

        //Cek apakah user tersebut sudah dibuat struktur organisasinya? jika sudah maka tidak bisa buat lagi, arahkan untuk melakukan edit
        $check_user = OrganizationalStructure::where('user_id', $request->user_id)->get();
        if ($check_user->count() > 0) {
            return back()->with('sudah ada', 'Maaf, user tersebut sudah memiliki struktur organisasi. Silahkan klik edit jika ingin mengubahnya');
        }
        if ($difference_level < 0) {
            return back()->with('level tidak sesuai', 'Maaf, user tersebut memiliki job level lebih tinggi dari supervisor yang dipilih');
        }

        OrganizationalStructure::create([
            'user_id'           => $request->user_id,
            'supervisor_id'     => $request->supervisor_id
            // 'difference_level'  => $difference_level
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan data struktur organisasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrganizationalStructure  $organizationalStructure
     * @return \Illuminate\Http\Response
     */
    public function show(OrganizationalStructure $organizationalStructure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrganizationalStructure  $organizationalStructure
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users_company            = auth()->user()->company->user(auth()->user()->company->id);
        $organizationalStructure  = OrganizationalStructure::find($id);
        $user                     = User::where('id', $organizationalStructure->user_id)->first();
        $organizationalStructures = OrganizationalStructure::join('users', 'organizational_structures.user_id', '=', 'users.id')
            ->join('departments', 'users.department_id', '=', 'departments.id')
            ->join('entities', 'departments.entity_id', '=', 'entities.id')
            ->join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
            ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
            ->select('companies.id', 'organizational_structures.*', 'users.full_name')
            ->where('companies.id', auth()->user()->company->id)
            ->get();

        return view('organizational-structure.edit', compact(
            'id',
            'organizationalStructure',
            'users_company',
            'user'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrganizationalStructure  $organizationalStructure
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        OrganizationalStructure::where('id', $id)->update([
            'user_id' => $request->user_id,
            'supervisor_id' => $request->supervisor_id
        ]);

        return redirect('/organizational-structure')->with('update', 'Berhasil mengubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrganizationalStructure  $organizationalStructure
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OrganizationalStructure::find($id)->delete();

        return back()->with('delete', 'Berhasil hapus data');
    }
}

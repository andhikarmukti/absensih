<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\Shift;
use Illuminate\Http\Request;
use App\Models\ShiftSchedule;

class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $shifts = Shift::where('company_id', auth()->user()->company->id)->get();
        $start_hour = '00:00';
        $end_hour = '00:00';

        if ($request->end_hour) {
            $start_hour = $request->start_hour;
            $end_hour = $request->end_hour;
            $diff_time = strtotime($end_hour) - strtotime($start_hour);
            $total_working_hours = strtotime(date('Y-m-d H:i:s', strtotime($end_hour))) - strtotime(date('Y-m-d H:i:s', strtotime($start_hour)));
            $bantuanAngkaJam = floor($total_working_hours / (60 * 60)) < 10 ? '0' : '';
            $bantuanAngkaMenit = ($total_working_hours % (60 * 60)) / 60 < 10 ? '0' : '';
            $total_working_hours = $bantuanAngkaJam . floor($total_working_hours / (60 * 60)) . ':' . $bantuanAngkaMenit . ($total_working_hours % (60 * 60)) / 60;
            return response()->json($total_working_hours);
        }

        return view('shift.create', compact(
            'shifts'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        if (!$request->location_check) {
            $location_check = "off";
        } else {
            $location_check = $request->location_check;
        }

        $is_flexible = $request->is_flexible;

        Shift::create([
            'company_id' => $request->company_id,
            'shift_name' => $request->shift_name,
            'start_shift_hours' => $request->start_shift_hours,
            'end_shift_hours' => $request->end_shift_hours,
            'location_check' => $location_check,
            'is_flexible' => $is_flexible,
            'total_working_hours' => $request->total_working_hours
        ]);
        return back()->with('berhasil', 'Data shift baru telah berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function show(Shift $shift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Shift $shift)
    {
        $shift = Shift::find($id);

        return view('shift.edit', compact(
            'shift'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Shift $shift)
    {
        $shift->find($id)->update([
            'shift_name' => $request->shift_name,
            'start_shift_hours' => $request->start_shift_hours,
            'end_shift_hours' => $request->end_shift_hours,
            'location_check' => $request->location_check
        ]);

        return redirect('/shift')->with('update', 'Berhasil merubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Shift $shift)
    {
        // ShiftSchedule::where('shift_id', $id)->delete(); karna sudah tidak ada kolom shift_id di table shift_schedules
        $shift->find($id)->delete();

        return back()->with('delete', 'Data berhasil dihapus');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\ShiftSchedule;
use App\Models\DynamicShiftChange;

class DynamicShiftChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users                 = auth()->user()->company->user(auth()->user()->company->id);
        $shift_schedules       = ShiftSchedule::where('company_id', auth()->user()->company->id)->get();
        $dynamic_shift_changes = DynamicShiftChange::where('company_id', auth()->user()->company->id)->get();

        $user           = new User;
        $shift_schedule = new ShiftSchedule;

        return view('dynamic-shift-change.create', compact(
            'users',
            'shift_schedules',
            'dynamic_shift_changes',
            'user',
            'shift_schedule'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'rotation_shift' => 'required',
            'from_shift_schedule_id' => 'required',
            'to_shift_schedule_id' => 'required',
        ]);

        if ($request->from_shift_schedule_id == $request->to_shift_schedule_id) {
            return back()->with('data kembar', 'From dan To tidak boleh sama');
        }

        if (DynamicShiftChange::where('user_id', $request->user_id)->first()) {
            return back()->with('user already', 'User tersebut sudah memiliki dynamic shift schedule');
        }

        DynamicShiftChange::create([
            'company_id' => auth()->user()->company->id,
            'user_id' => $request->user_id,
            'rotation_shift' => $request->rotation_shift,
            'from_shift_schedule_id' => $request->from_shift_schedule_id,
            'to_shift_schedule_id' => $request->to_shift_schedule_id
        ]);

        return back()->with('store', 'Berhasil menambahkan data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DynamicShiftChange  $dynamicShiftChange
     * @return \Illuminate\Http\Response
     */
    public function show(DynamicShiftChange $dynamicShiftChange)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DynamicShiftChange  $dynamicShiftChange
     * @return \Illuminate\Http\Response
     */
    public function edit(DynamicShiftChange $dynamicShiftChange)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DynamicShiftChange  $dynamicShiftChange
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DynamicShiftChange $dynamicShiftChange)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DynamicShiftChange  $dynamicShiftChange
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DynamicShiftChange::destroy($id);

        return back()->with('delete', 'data berhasil dihapus');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\BranchCompany;
use App\Models\Department;
use App\Models\Entity;

class BranchCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company_id         = auth()->user()->company->id;
        $branch_companies   = BranchCompany::where('company_id', $company_id)->get();

        return view('branch-company.create', compact(
            'company_id',
            'branch_companies'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        BranchCompany::create([
            'company_id' => $request->company_id,
            'branch_name' => $request->branch_name,
            'branch_longitude' => $request->branch_longitude,
            'branch_latitude' => $request->branch_latitude,
        ]); return back()->with('berhasil', 'Branch baru berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BranchCompany  $branchCompany
     * @return \Illuminate\Http\Response
     */
    public function show(BranchCompany $branchCompany)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BranchCompany  $branchCompany
     * @return \Illuminate\Http\Response
     */
    public function edit($id, BranchCompany $branchCompany)
    {
        $branch_company = BranchCompany::find($id);

        return view('branch-company.edit', compact(
            'branch_company',
            'id'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BranchCompany  $branchCompany
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, BranchCompany $branchCompany)
    {
        $branchCompany->find($id)->update([
            'company_id' => BranchCompany::where('id', $id)->first()->company_id,
            'branch_name' => $request->branch_name,
            'branch_longitude' => $request->branch_longitude,
            'branch_latitude' => $request->branch_latitude
        ]);
        return redirect('/branch-company')->with('update', 'berhasil merubah data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BranchCompany  $branchCompany
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, BranchCompany $branchCompany)
    {
        $branch = $branchCompany->where('id', $id)->first();
        foreach($branchCompany->user($id) as $bc){
            User::where('email', $bc->email)->first()->delete();
        }
        Entity::where('branch_id', $bc->branch_id)->delete();
        Department::where('entity_id', $bc->entity_id)->delete();
        $branch->delete();

        return back()->with('delete', 'Data berhasil dihapus');
    }
}

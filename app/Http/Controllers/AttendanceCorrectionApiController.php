<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\AttendanceCorrection;

class AttendanceCorrectionApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $user_id                = $request->user_id;
        $new_checkin_time       = $request->new_checkin_time;
        $history_attendance_id  = $request->history_attendance_id;

        if ($user_id && $new_checkin_time && $history_attendance_id) {
            try {
                // pengecekan apakah user tersebut sudah memiliki supervisor_id atau belum
                $user = User::where('id', $user_id)->first();
                if ($user->supervisor->first() == null) {
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'No supervisor id, Sorry, you dont have a supervisor yet. No attendance correction allowed. Please contact the admin concerned.'
                    ], 400);
                }

                AttendanceCorrection::create([
                    'user_id' => $user->id,
                    'history_attendance_id' => $request->history_attendance_id,
                    'checkin_time_correction' => $new_checkin_time,
                    'description' => $request->description,
                    'supervisor_id' => $user->supervisor->first()->supervisor_id
                ]);
                return response()->json([
                    'status' => 'success',
                    'message' => 'Successful attendance correction'
                ]);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

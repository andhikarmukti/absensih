<?php

namespace App\Http\Controllers;

use DateTime;
use DatePeriod;
use DateInterval;
use App\Models\User;
use App\Models\PaidLeave;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\PaidLeaveHistory;
use App\Http\Controllers\Controller;
use App\Models\PaidLeaveUser;

class PaidLeaveHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paid_leave_user_model = new PaidLeaveUser;
        $paid_leave_user       = $paid_leave_user_model->where('user_id', auth()->user()->id)->first();
        if ($paid_leave_user == null) {
            $paid_leave_quota      = 0;
        } else {
            $paid_leave_quota      = $paid_leave_user->paid_leave_quota;
        }

        $paid_leave_types = auth()->user()->company->paidleave;
        return view('paid-leave-history.create', compact(
            'paid_leave_types',
            'paid_leave_quota'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start_date = date_create($request->start_paid_leave_date);
        $end_date = date_create($request->end_paid_leave_date);
        $total_day_paid_leave = (date_diff($start_date, $end_date)->days + 1);

        $paid_leave_user_model = new PaidLeaveUser;
        $paid_leave_user       = $paid_leave_user_model->where('user_id', auth()->user()->id)->first();
        $paid_leave_quota      = $paid_leave_user->paid_leave_quota;

        if ($total_day_paid_leave > $paid_leave_quota) {
            return back()->with('kuota tidak cukup', 'maaf, jumlah cuti yang kamu ambil sebanyak ' . $total_day_paid_leave . ' hari. Itu melampaui kuota cuti yang kamu miliki');
        }


        if (auth()->user()->supervisor->first()) {
            PaidLeaveHistory::create([
                'user_id' => auth()->user()->id,
                'paid_leave_id' => $request->paid_leave_id,
                'start_paid_leave_date' => $request->start_paid_leave_date,
                'end_paid_leave_date' => $request->end_paid_leave_date,
                'description' => $request->description,
                'supervisor_id' => auth()->user()->supervisor->first()->supervisor_id
            ]);
            return back()->with('berhasil', 'Berhasil mengajukan permintaan paid leave (cuti)');
        } else {
            return back()->with('gagal', 'maaf, anda belum memiliki supervisor. Silahkan hubungi admin terkait');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaidLeaveHistory  $paidLeaveHistory
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $list_users = PaidLeaveHistory::where('approval', 'pending')->where('supervisor_id', auth()->user()->id)->get();
        $user = new User;
        $paidleave = new PaidLeave();

        return view('paid-leave-history.show', compact(
            'list_users',
            'user',
            'paidleave'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaidLeaveHistory  $paidLeaveHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(PaidLeaveHistory $paidLeaveHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaidLeaveHistory  $paidLeaveHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $paid_leave_history = PaidLeaveHistory::find($id);
        $paid_leave_history->update([
            'approval' => 'accept'
        ]);

        $start_date = date_create($paid_leave_history->start_paid_leave_date);
        $end_date = date_create($paid_leave_history->end_paid_leave_date);
        $total_day_paid_leave = (date_diff($start_date, $end_date)->days + 1);

        $paid_leave_user_model       = new PaidLeaveUser;
        $paid_leave_user       = $paid_leave_user_model->where('user_id', $paid_leave_history->user_id)->first();
        $paid_leave_quota      = $paid_leave_user->paid_leave_quota;
        $diff_paid_leave_quota = $paid_leave_quota - $total_day_paid_leave;
        $paid_leave_user_model->where('user_id', $paid_leave_history->user_id)->update([
            'paid_leave_quota' => $diff_paid_leave_quota
        ]);

        return back()->with('accept', 'Berhasil menyetujui request cuti');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaidLeaveHistory  $paidLeaveHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaidLeaveHistory $paidLeaveHistory)
    {
        //
    }
}

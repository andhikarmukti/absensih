<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\HistoryAttendance;
use App\Models\AttendanceCorrection;
use Database\Seeders\HistoryAttendanceSeeder;

class AttendanceCorrectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attendances = HistoryAttendance::where('user_id', auth()->user()->id)->get();

        return view('attendance-correction.create', compact(
            'attendances'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        // pengecekan apakah user tersebut sudah memiliki supervisor_id atau belum
        if (auth()->user()->supervisor->first() == null) {
            return back()->with('belum ada supervisor id', 'Maaf, kamu belum memiliki supervisor. Koreksi absensi tidak ada yang mengizinkan. Silahkan hubungi admin yang bersangkutan.');
        }

        // $old_time         = date("H:i:s", strtotime($request->old_checkin_time));
        $old_date         = date("Y-m-d", strtotime($request->old_checkin_time));
        $new_time         = $request->checkin_time;
        $new_checkin_time = $old_date . " " . $new_time;

        AttendanceCorrection::create([
            'user_id' => auth()->user()->id,
            'history_attendance_id' => $id,
            'checkin_time_correction' => $new_time,
            'description' => $request->description,
            'supervisor_id' => auth()->user()->supervisor->first()->supervisor_id
        ]);
        return back()->with('berhasil', 'Berhasil mengajukan koreksi kehadiran');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AttendanceCorrection  $attendanceCorrection
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $list_request = AttendanceCorrection::join('users', 'users.id', '=', 'attendance_corrections.user_id')
            ->join('history_attendances', 'history_attendances.id', '=', 'attendance_corrections.history_attendance_id')
            ->where('attendance_corrections.supervisor_id', '=', auth()->user()->id)
            ->where('attendance_corrections.approval', '=', 'pending')
            ->select('users.full_name', 'history_attendances.*', 'attendance_corrections.*')
            ->get();
        return view('attendance-correction.show', compact(
            'list_request'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AttendanceCorrection  $attendanceCorrection
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $history_attendance = HistoryAttendance::where('id', $id)->first();

        return view('attendance-correction.edit', compact(
            'history_attendance'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AttendanceCorrection  $attendanceCorrection
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        AttendanceCorrection::find($id)->update([
            'approval' => 'decline'
        ]);
        return back()->with('decline', 'Request ditolak');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AttendanceCorrection  $attendanceCorrection
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttendanceCorrection $attendanceCorrection)
    {
        //
    }
}

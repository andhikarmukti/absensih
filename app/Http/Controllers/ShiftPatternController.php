<?php

namespace App\Http\Controllers;

use App\Models\ShiftPattern;
use Illuminate\Http\Request;

class ShiftPatternController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ShiftPattern  $shiftPattern
     * @return \Illuminate\Http\Response
     */
    public function show(ShiftPattern $shiftPattern)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShiftPattern  $shiftPattern
     * @return \Illuminate\Http\Response
     */
    public function edit(ShiftPattern $shiftPattern)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ShiftPattern  $shiftPattern
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShiftPattern $shiftPattern)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShiftPattern  $shiftPattern
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShiftPattern $shiftPattern)
    {
        //
    }
}

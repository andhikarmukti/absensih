<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Overtime;
use Illuminate\Http\Request;

class OvertimeApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //user_id, start_overtime_date, end_overtime_date, start_overtime_hour, end_overtime_hour, description
        $user = User::find($request->user_id);
        $start_overtime_date = $request->start_overtime_date;
        $end_overtime_date = $request->end_overtime_date;
        $start_overtime_hour = $request->start_overtime_hour;
        $end_overtime_hour = $request->end_overtime_hour;
        $description = $request->description;

        if($user && $start_overtime_date && $end_overtime_date && $start_overtime_hour && $end_overtime_hour && $description){
            try{
                // mendapatkan total berapa jam lemburnya
                $total_overtime_hour = strtotime($end_overtime_hour) - strtotime($start_overtime_hour);
                $total_overtime_hour = explode(":", gmdate("H:i:s",$total_overtime_hour));
                $total_overtime_hour = ($total_overtime_hour[0] * 60 + $total_overtime_hour[1])/60;

                Overtime::create([
                    'user_id' => $user->id,
                    'start_overtime_date' => $start_overtime_date,
                    'end_overtime_date' => $end_overtime_date,
                    'start_overtime_hour' => $start_overtime_hour,
                    'end_overtime_hour' => $end_overtime_hour,
                    'total_overtime_hour' => number_format($total_overtime_hour,2),
                    'description' => $description,
                    'supervisor_id' => $user->supervisor->first()->supervisor_id
                ]);
                return response()->json([
                    'status' => 'success',
                    'message' => 'successful overtime request'
                ]);
            }catch (\Exception $e){
                $e->getMessage();
            }
        }else{
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

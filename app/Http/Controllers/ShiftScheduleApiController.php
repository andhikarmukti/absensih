<?php

namespace App\Http\Controllers;

use App\Models\ShiftSchedule;
use Illuminate\Http\Request;

class ShiftScheduleApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $shift_schedule_id = $request->shift_schedule_id;
        if($shift_schedule_id){
            try{
                $shift_and_schedule = ShiftSchedule::join('shifts', 'shift_schedules.shift_id', '=', 'shifts.id')
                                                    ->where('shift_schedules.id', '=', $shift_schedule_id)
                                                    ->select('shift_schedules.*', 'shifts.shift_name', 'shifts.start_shift_hours', 'shifts.end_shift_hours')
                                                    ->first();

                return response()->json($shift_and_schedule);
            } catch(\Exception $e){
                $e->getMessage();
            }
        }else{
            return response()->json([
                'status' => 'failed',
                'message' => 'bad auth'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

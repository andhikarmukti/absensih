<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Shift;
use App\Models\Entity;
use App\Models\Company;
use App\Models\Department;
use Illuminate\Http\Request;
use App\Filters\BranchFilter;
use App\Models\BranchCompany;
use App\Filters\CompanyFilter;
use App\Models\OfficePosition;
use Illuminate\Routing\Pipeline;
use Illuminate\Support\Facades\Hash;

class AdminAbsensihController extends Controller
{
    public function index()
    {
        return view('admin-absensih.index');
    }

    public function create_company()
    {
        $companies  = app(Pipeline::class)->send(Company::query())->through([
            CompanyFilter::class,
        ])
            ->thenReturn()
            // ->orderByDesc('branch_name')
            ->paginate(20);

        return view('admin-absensih.create-company', compact(
            'companies'
        ));
    }

    public function store_company(Request $request, Company $company)
    {
        $company->create([
            'company_name' => $request->company_name
        ]);

        Shift::create([
            'shift_name' => 'Shift Off',
            'company_id' => Company::where('company_name', $request->company_name)->first()->id
        ]);

        return back()->with('berhasil', 'Berhasil menambahkan company baru');
    }

    public function create_branch(Company $company)
    {
        $companies = $company->all();
        $branches  = app(Pipeline::class)->send(BranchCompany::query())->through([
            BranchFilter::class,
        ])
            ->thenReturn()
            // ->orderByDesc('branch_name')
            ->paginate(20);


        return view('admin-absensih.create-branch', compact(
            'companies',
            'branches',
            'company'
        ));
    }

    public function store_branch(Request $request, BranchCompany $branch_company)
    {
        $branch_company->create([
            'company_id'       => $request->company_id,
            'branch_name'      => $request->branch_name,
            'branch_longitude' => $request->branch_longitude,
            'branch_latitude'  => $request->branch_latitude,
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan branch baru');
    }

    public function create_entity(Request $request)
    {
        $companies = Company::all();
        $branches = BranchCompany::all();

        if ($request->company) {
            $branches = BranchCompany::where('company_id', $request->company)->get();
            return response()->json($branches);
        }

        $data_entities = Entity::join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
            ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
            ->get();

        return view('admin-absensih.create-entity', compact(
            'companies',
            'branches',
            'data_entities'
        ));
    }

    public function store_entity(Request $request, Entity $entity)
    {
        $entity->create([
            'company_id' => $request->company_id,
            'branch_id' => $request->branch_id,
            'entity_name' => $request->entity_name,
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan entity');
    }

    public function create_department(Request $request)
    {
        $companies = Company::all();
        $branches  = BranchCompany::all();
        $entities  = Entity::all();

        if ($request->company) {
            $branches = BranchCompany::where('company_id', $request->company)->get();
            return response()->json($branches);
        }
        if ($request->branch) {
            $entities = Entity::where('branch_id', $request->branch)->get();
            return response()->json($entities);
        }

        $data_departments = Department::join('entities', 'departments.entity_id', '=', 'entities.id')
            ->join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
            ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
            ->get();

        return view('admin-absensih.create-department', compact(
            'companies',
            'branches',
            'entities',
            'data_departments'
        ));
    }

    public function store_department(Request $request)
    {
        Department::create([
            'entity_id' => $request->entity_id,
            'department_name' => $request->department_name,
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan department baru');
    }

    public function create_user(Request $request)
    {
        $companies        = Company::all();
        $branches         = BranchCompany::all();
        $entities         = Entity::all();
        $departments      = Department::all();
        $office_positions = OfficePosition::all();

        if ($request->company) {
            $branches = BranchCompany::where('company_id', $request->company)->get();
            $office_positions = OfficePosition::where('company_id', $request->company)->get();
            return response()->json([$branches, $office_positions]);
        }
        if ($request->branch) {
            $entities = Entity::where('branch_id', $request->branch)->get();
            return response()->json($entities);
        }
        if ($request->entity) {
            $departments = Department::where('entity_id', $request->entity)->get();
            return response()->json($departments);
        }

        $data_users = User::join('departments', 'users.department_id', '=', 'departments.id')
            ->join('entities', 'departments.entity_id', '=', 'entities.id')
            ->join('branch_companies', 'entities.branch_id', '=', 'branch_companies.id')
            ->join('companies', 'branch_companies.company_id', '=', 'companies.id')
            ->orderBy('company_id', 'ASC')
            ->get();

        return view('admin-absensih.create-user', compact(
            'companies',
            'branches',
            'entities',
            'departments',
            'data_users',
            'office_positions'
        ));
    }

    public function store_user(Request $request)
    {
        $password = 'absensih';

        User::create([
            'full_name'             => $request->full_name,
            'username'              => $request->username,
            'email'                 => $request->email,
            'department_id'         => $request->department_id,
            'office_position_id'    => $request->office_position_id,
            'role'                  => $request->role,
            'password'              => Hash::make($password),
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan user baru ( ' . $request->full_name . ' )');
    }

    public function create_office_position(Request $request, Company $company)
    {
        $companies        = Company::all();
        $office_positions = OfficePosition::all();

        return view('admin-absensih.create-office-position', compact(
            'companies',
            'office_positions',
            'company'
        ));
    }

    public function store_office_position(Request $request)
    {
        OfficePosition::create([
            'company_id' => $request->company_id,
            'position_name' => $request->position_name,
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan office position');
    }
}

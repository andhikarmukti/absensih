<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user_id = $request->user_id;

        if ($user_id) {
            $user = User::find($user_id);
            try {
                return response()->json($user);
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //username, old_password, new_password

        $username     = $request->username;
        $old_password = $request->old_password;
        $new_password = $request->new_password;

        if ($username && $old_password && $new_password) {
            try {
                if (Auth::attempt(['username' => $username, 'password' => $old_password])) {
                    User::where('username', $username)->update([
                        'password' => Hash::make($new_password)
                    ]);
                    return response()->json([
                        'status' => 'success',
                        'message' => 'successful change password'
                    ], 200);
                } else {
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'bad old password'
                    ], 401);
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

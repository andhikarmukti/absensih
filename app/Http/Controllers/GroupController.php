<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Group;
use App\Models\GroupUser;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\GroupUse;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = auth()->user()->company->group;

        return view('group.create', compact(
            'groups'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Group::create([
            'company_id' => auth()->user()->company->id,
            'group_name' => $request->group_name
        ]); return back()->with('berhasil', 'Berhasil menambahkan group baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Group $group)
    {
        $group = $group->where('id', $id)->first();

        return view('group.edit', compact(
            'group'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Group $group)
    {
        $group->where('id', $id)->update([
            'group_name' => $request->group_name
        ]);

        return redirect('/group')->with('update', 'Berhasil update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Group $group)
    {
        $group = Group::where('id', $id)->first();
        GroupUser::where('group_id', $id)->delete();
        $group->delete();
        return back()->with('delete', 'Berhasil menghapus data');
    }

    public function group_users(Request $request)
    {
        $groups      = auth()->user()->company->group;
        $users_group = auth()->user()->company->user(auth()->user()->company->id);

        if($request->group_id){
        $users = User::join('group_users', 'users.id', '=', 'group_users.user_id')
                    ->join('groups', 'groups.id', '=', 'group_users.group_id')
                    ->where('groups.id', $request->group_id)
                    ->get();

        return response()->json($users);
        }

        return view('group.group-users', compact(
            'groups',
            'users_group'
        ));
    }

    public function group_users_store(Request $request)
    {
        $check_user_in_group = GroupUser::where('user_id', $request->user_id)->where('group_id', $request->group_id)->get()->count();
        if($check_user_in_group > 0){
            return back()->with('sudah ada', 'Maaf user tersebut sudah tergabung di dalam group');
        }

        GroupUser::create([
            'user_id' => $request->user_id,
            'group_id' => $request->group_id,
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan user baru ke group');
    }

    public function group_users_delete(Request $request, $id, GroupUser $groupUser)
    {
        GroupUser::where('user_id', $id)->where('group_id', $request->group_id)->delete();
        return back()->with('delete', 'Berhasil hapus data');
    }
}

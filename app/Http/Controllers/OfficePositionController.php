<?php

namespace App\Http\Controllers;

use App\Models\OfficePosition;
use Illuminate\Http\Request;

class OfficePositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $office_positions = OfficePosition::where('company_id', auth()->user()->company->id)->get();

        return view('office-position.create', compact(
            'office_positions'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        OfficePosition::create([
            'company_id' => auth()->user()->company->id,
            'position_name' => $request->position_name
        ]);
        return back()->with('berhasil', 'Berhasil menambahkan office position baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OfficePosition  $officePosition
     * @return \Illuminate\Http\Response
     */
    public function show(OfficePosition $officePosition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OfficePosition  $officePosition
     * @return \Illuminate\Http\Response
     */
    public function edit(OfficePosition $officePosition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OfficePosition  $officePosition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OfficePosition $officePosition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OfficePosition  $officePosition
     * @return \Illuminate\Http\Response
     */
    public function destroy(OfficePosition $officePosition)
    {
        //
    }
}

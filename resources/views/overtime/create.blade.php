@extends('layouts.main');

@section('content')
<a href="/">kembali</a>
<h3 class="mb-4">Request Overtime</h3>

<div class="col-6">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form method="POST" action="/overtime">
        @csrf
        <div class="mb-3">
            <label for="start_overtime_date" class="form-label">Start Overtime Date</label>
            <input type="date" class="form-control" id="start_overtime_date" name="start_overtime_date">
        </div>
        <div class="mb-3">
            <label for="end_overtime_date" class="form-label">End Overtime Date</label>
            <input type="date" class="form-control" id="end_overtime_date" name="end_overtime_date">
        </div>
        <div class="mb-3">
            <label for="start_overtime_hour" class="form-label">Start Overtime Hour</label>
            <input type="time" class="form-control" id="start_overtime_hour" name="start_overtime_hour">
        </div>
        <div class="mb-3">
            <label for="end_overtime_hour" class="form-label">End Overtime Hour</label>
            <input type="time" class="form-control" id="end_overtime_hour" name="end_overtime_hour">
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <input type="text" class="form-control" id="description" name="description">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
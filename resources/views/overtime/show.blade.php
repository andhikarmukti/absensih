@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3 class="mb-4">List Request Overtime</h3>

<div class="col-6">
    {{-- Alert --}}
    @if(session()->has('accept'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('accept') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('decline'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('decline') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
</div>

<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">User Name</th>
            <th scope="col">Start Overtime Date</th>
            <th scope="col">End Overtime Date</th>
            <th scope="col">Start Overtime Hour</th>
            <th scope="col">End Overtime Hour</th>
            <th scope="col">Total Overtime Hour</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <th scope="row">{{ $user->user_id }}</th>
            <td>{{ $user->start_overtime_date }}</td>
            <td>{{ $user->end_overtime_date }}</td>
            <td>{{ $user->start_overtime_hour }}</td>
            <td>{{ $user->end_overtime_hour }}</td>
            <td>{{ $user->total_overtime_hour }}</td>
            <td>{{ $user->description }}</td>
            <td>
                <form action="/overtime" method="post" class="d-inline">
                    @csrf
                    @method('put')
                    <button type="submit" class="badge bg-success border-0" name="accept" value="{{ $user->id }}"
                        onclick="return alert('anda yakin?')">accept</button>
                </form>
                <form action="/overtime" method="post" class="d-inline">
                    @csrf
                    @method('put')
                    <button type="submit" class="badge bg-danger border-0" name="decline" value="{{ $user->id }}"
                        onclick="return alert('anda yakin?')">decline</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
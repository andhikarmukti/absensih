@extends('layouts.main')

@section('content')
<a href="/paid-leave/add-paid-leave">kembali</a>

<h2 class="mb-5 mt-5">Edit Jenis Cuti</h2>

<div class="col-3">

    {{-- Alert --}}
    @if(session()->has('store'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('store') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('nama cuti sama'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('nama cuti sama') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/paid-leave/edit/{{ $paid_leave->id }}" method="post">
        @csrf
        @method('put')
        <div class="mb-3">
            <label for="paid_leave_name" class="form-label">Nama Cuti</label>
            <input type="text" class="form-control" id="paid_leave_name" name="paid_leave_name"
                value="{{ $paid_leave->paid_leave_name }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection

@extends('layouts.main')

@section('content')
<a href="/">kembali</a>

<h2 class="mb-5 mt-5">Tambah Jenis Cuti</h2>

<div class="col-3">

    {{-- Alert --}}
    @if(session()->has('store'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('store') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('nama cuti sama'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('nama cuti sama') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('update'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('update') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('delete'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('delete') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/paid-leave/add-paid-leave/{{ auth()->user()->company->id }}" method="post">
        @csrf
        <div class="mb-3">
            <label for="paid_leave_name" class="form-label">Nama Cuti</label>
            <input type="text" class="form-control" id="paid_leave_name" name="paid_leave_name">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<div class="col-6 mt-5">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">Jenis Cuti</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($paid_leaves as $paid_leave)
            <tr>
                <td>{{ $paid_leave->paid_leave_name }}</td>
                <td>
                    <a href="/paid-leave/edit/{{ $paid_leave->id }}"
                        class="badge bg-warning text-decoration-none">edit</a>
                    <form action="/paid-leave/delete/{{ $paid_leave->id }}" method="POST" class="d-inline">
                        @csrf
                        @method('delete')
                        <button type="submit" class="badge bg-danger border-0"
                            onclick="return confirm('are you sure?')">delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

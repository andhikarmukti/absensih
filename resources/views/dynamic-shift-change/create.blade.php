@extends('layouts.main')

@section('content')
<a href="/">kembali</a>

<div class="col-6">
    <h3>Add New Dynamic Shift Change</h3>

    {{-- Alert --}}
    @if(session()->has('data kembar'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('data kembar') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('store'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('store') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('user already'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('user already') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('delete'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('delete') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/dynamic-shift-change" method="POST">
        @csrf
        <div class="mb-3">
            <label for="user_id" class="form-label">User</label>
            <select class="form-select @error('user_id') is-invalid @enderror" id="user_id" name="user_id">
                <option selected disabled>--</option>
                @foreach ($users as $user)
                <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                @endforeach
            </select>
            @error('user_id')
            <div class="invalid-feedback">
                Tidak boleh kosong. Wajib diisi!
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="rotation_shift" class="form-label">Rotation Shift</label>
            <select class="form-select @error('rotation_shift') is-invalid @enderror" id="rotation_shift"
                name="rotation_shift">
                <option selected disabled>--</option>
                <option value="Weekly (every Monday)">Weekly (every Monday 01:00 AM)</option>
            </select>
            @error('rotation_shift')
            <div class="invalid-feedback">
                Tidak boleh kosong. Wajib diisi!
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="from_shift_schedule_id" class="form-label">From Shift Schedule</label>
            <select class="form-select @error('from_shift_schedule_id') is-invalid @enderror"
                id="from_shift_schedule_id" name="from_shift_schedule_id">
                <option selected disabled>--</option>
                @foreach ($shift_schedules as $shift_schedule)
                <option value="{{ $shift_schedule->id }}">{{ $shift_schedule->shift_schedule_name }}</option>
                @endforeach
            </select>
            @error('from_shift_schedule_id')
            <div class="invalid-feedback">
                Tidak boleh kosong. Wajib diisi!
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="to_shift_schedule_id" class="form-label">To Shift Schedule</label>
            <select class="form-select @error('to_shift_schedule_id') is-invalid @enderror" id="to_shift_schedule_id"
                name="to_shift_schedule_id">
                <option selected disabled>--</option>
                @foreach ($shift_schedules as $shift_schedule)
                <option value="{{ $shift_schedule->id }}">{{ $shift_schedule->shift_schedule_name }}</option>
                @endforeach
            </select>
            @error('to_shift_schedule_id')
            <div class="invalid-feedback">
                Tidak boleh kosong. Wajib diisi!
            </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<table class="table table-hover mt-5">
    <thead>
        <tr>
            <th scope="col">User</th>
            <th scope="col">Rotation Shift</th>
            <th scope="col">Form Shift</th>
            <th scope="col">To Shift</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($dynamic_shift_changes as $dynamic_shift_change)
        <tr>
            <th scope="row">{{ $user->where('id', $dynamic_shift_change->user_id)->first()->full_name }}</th>
            <td>{{ $dynamic_shift_change->rotation_shift }}</td>
            <td>{{ $shift_schedule->where('id', $dynamic_shift_change->from_shift_schedule_id)->first()->shift_schedule_name ?? 'deleted' }}</td>
            <td>{{ $shift_schedule->where('id', $dynamic_shift_change->to_shift_schedule_id)->first()->shift_schedule_name ?? 'deleted' }}</td>
            <td class="text-center">
                <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                <form action="/dynamic-shift-change/{{ $dynamic_shift_change->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <button class="badge bg-danger border-0" onclick="return confirm('are you sure?')">delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

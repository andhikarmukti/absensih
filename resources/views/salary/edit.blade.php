@extends('layouts.main')

@section('content')
    <a href="/salary">kembali</a>
    <h3>Edit Salary</h3>
    <div class="col-6">

        {{-- Alert --}}
        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/salary/edit/{{ $salary->id }}" method="post">
            @csrf
            @method('put')
            <label for="salary_name">Salary Name</label>
            <input type="text" class="form-control" id="salary_name" name="salary_name" value="{{ $salary->salary_name }}">

            <label for="salary_value">Salary Value</label>
            <input type="text" class="form-control" id="salary_value" name="salary_value" value="{{ number_format($salary->salary_value,0, ",", ".") }}">

            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>






    <script type="text/javascript">

        var salary_value = document.getElementById('salary_value');
        salary_value.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            salary_value.value = formatRupiah(this.value);
        });

        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            salary_value     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                salary_value += separator + ribuan.join('.');
            }

            salary_value = split[1] != undefined ? salary_value + ',' + split[1] : salary_value;
            return prefix == undefined ? salary_value : (salary_value ? '' + salary_value : '');
        }
    </script>

@endsection


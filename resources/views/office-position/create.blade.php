@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3 class="mt5 mb-4">Add Office Position</h3>

<div class="col-6">

    {{-- alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/office-position" method="POST">
        @csrf
        <div class="mb-3">
            <label for="position_name" class="form-label">Position Name</label>
            <input type="text" class="form-control" id="position_name" name="position_name">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<table class="table table-hover mt-5">
    <thead>
        <tr>
            <th scope="col">Position Name</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($office_positions as $office_position)
        <tr>
            <th scope="row">{{ $office_position->position_name }}</th>
            <td class="text-center">
                <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                <a href="#" class="badge bg-danger text-decoration-none">delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3 class="mt-5 mb-4">Add Reimburse Request</h3>
<div class="col-lg-6">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/reimburse" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label for="reimburse_date" class="form-label">Reimburse Date</label>
            <input type="date" class="form-control" id="reimburse_date" name="reimburse_date">
        </div>
        <div class="mb-3">
            <label for="reimburse_value" class="form-label">Reimburse Value</label>
            <input type="text" class="form-control" id="reimburse_value" name="reimburse_value">
        </div>
        <div class="mb-3">
            <label for="photo_proof" class="form-label">Photo Proof</label>
            <input type="file" class="form-control" id="photo_proof" name="photo_proof">
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <input type="text" class="form-control" id="description" name="description">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>






<script type="text/javascript">
    var reimburse_value = document.getElementById('reimburse_value');
    reimburse_value.addEventListener('keyup', function(e){
        // tambahkan 'Rp.' pada saat form di ketik
        // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
        reimburse_value.value = formatRupiah(this.value);
    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		  = number_string.split(','),
        sisa     		  = split[0].length % 3,
        reimburse_value   = split[0].substr(0, sisa),
        ribuan     		  = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator        = sisa ? '.' : '';
            reimburse_value += separator + ribuan.join('.');
        }

        reimburse_value = split[1] != undefined ? reimburse_value + ',' + split[1] : reimburse_value;
        return prefix   == undefined ? reimburse_value : (reimburse_value ? '' + reimburse_value : '');
    }
</script>
@endsection
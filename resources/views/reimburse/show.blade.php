@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3 class="mb-4 mt-6">List Request Reimburse</h3>

<div class="col-6">
    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('gagal'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('gagal') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
</div>

<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">User</th>
            <th scope="col">Reimburse Date</th>
            <th scope="col">Reimburse Value</th>
            <th scope="col">Photo Proof</th>
            <th scope="col">Description</th>
            <th scope="col">Approval</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($list_reimburses as $list_reimburse)
        <tr>
            <th scope="row">{{ $user->find($list_reimburse->user_id)->full_name }}</th>
            <td>{{ $list_reimburse->reimburse_date }}</td>
            <td>{{ number_format($list_reimburse->reimburse_value,0,",",".") }}</td>
            <td><a href=" {{ asset('storage/' . $list_reimburse->photo_proof) }}" target="_blank"
                    class="badge bg-warning text-decoration-none">bukti foto</a></td>
            <td>{{ $list_reimburse->description }}</td>
            <td>{{ $list_reimburse->approval }}</td>
            <td>
                <a href="/reimburse/list-request/accept/{{ $list_reimburse->id }}"
                    class="badge bg-success text-decoration-none" onclick="return confirm('Anda yakin?')">accept</a>
                <a href="/reimburse/list-request/decline/{{ $list_reimburse->id }}"
                    class="badge bg-danger text-decoration-none" onclick="return confirm('Anda yakin?')">decline</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
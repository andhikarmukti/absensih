@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3 class="mt-5 mb-4">List Request Paid Leave (Cuti)</h3>
<div class="col-6">
    {{-- Alert --}}
    @if(session()->has('accept'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('accept') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('decline'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('decline') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
</div>
<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">User</th>
            <th scope="col">Jenis Cuti</th>
            <th scope="col">Start Paid Leave Date</th>
            <th scope="col">End Paid Leave Date</th>
            <th scope="col">Description</th>
            <th scope="col">Approval</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($list_users as $list_user)
        <tr>
            <th scope="row">{{ $user->where('id', $list_user->user_id)->first()->full_name }}</th>
            <td>{{ $paidleave->where('id', $list_user->paid_leave_id)->first()->paid_leave_name }}</td>
            <td>{{ $list_user->start_paid_leave_date }}</td>
            <td>{{ $list_user->end_paid_leave_date }}</td>
            <td>{{ $list_user->description }}</td>
            <td>{{ $list_user->approval }}</td>
            <td class="text-center">
                <a href="/paid-leave/list-request/accept/{{ $list_user->id }}"
                    class="badge bg-success text-decoration-none" onclick="return confirm('anda yakin?')">accept</a>
                <a href="/paid-leave/list-request/decline/{{ $list_user->id }}"
                    class="badge bg-danger text-decoration-none" onclick="return confirm('anda yakin?')">decline</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3 class="mt-5 mb-4">Request Paid leave ( Sisa kuota cuti Anda = {{ $paid_leave_quota }})</h3>
<div class="col-5">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('gagal'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('gagal') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('kuota tidak cukup'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('kuota tidak cukup') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form accept="/paid-leave" method="POST">
        @csrf
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Jenis Cuti</label>
            <select class="form-select" name="paid_leave_id">
                <option selected disabled>--</option>
                @foreach ($paid_leave_types as $paid_leave_type)
                <option value="{{ $paid_leave_type->id }}">{{ $paid_leave_type->paid_leave_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="start_paid_leave_date" class="form-label">Start Paid Leave Date</label>
            <input type="date" class="form-control" id="start_paid_leave_date" name="start_paid_leave_date">
        </div>
        <div class="mb-3">
            <label for="end_paid_leave_date" class="form-label">End Paid Leave Date</label>
            <input type="date" class="form-control" id="end_paid_leave_date" name="end_paid_leave_date">
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <input type="text" class="form-control" id="description" name="description">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

@endsection
@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3 class="mt-5 mb-4">Paid Leave Access by User</h3>

<div class="col-4 mb-3">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/paid-leave/paid-leave-access" method="GET">
        <select class="form-select" id="user_id" name="user_id">
            <option selected disabled>--</option>
            @foreach ($users as $user)
            <option value="{{ $user->id }}" {{ $user->id == request('user_id') ? 'selected' : "" }}>{{ $user->full_name }}</option>
            @endforeach
        </select>
        <div class="btn-group">
            <button id="button_search" class="btn btn-primary mt-2" type="submit">Search</button>
            <a href="/paid-leave/paid-leave-access" class="btn btn-danger mt-2">Reset</a>
        </div>
    </form>
</div>
<div class="col-4">
    @if(request('user_id'))
    <form action="/paid-leave/paid-leave-access" method="POST">
        <input type="hidden" value="{{ request('user_id') }}" name="user_id">
        <table id="table" class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Jenis Cuti</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($paid_leaves as $paid_leave)
                <tr>
                    <th scope="row">{{ $paid_leave->paid_leave_name }}</th>
                        <td class="text-center">
                            @csrf
                            <input id="checkbox" type="checkbox" name="{{ $paid_leave->paid_leave_name }}" value="{{ $paid_leave->id }}"
                            @foreach ($paid_leaves_join as $paid_leave_join)
                            {{ $paid_leave->id == $paid_leave_join->paid_leave_id ? 'checked' : "" }}
                            @endforeach>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <button id="button_submit" class="mt-2 btn btn-primary">Submit</button>
    </form>
    @endif
</div>
@endsection
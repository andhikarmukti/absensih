@extends('layouts.main')

@section('content')
<a href="/user">kembali</a>
<div class="col-6">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('delete'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('delete') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <h4>Tambah Shift Schedule</h4>
    <form action="/user/tambah-shift-schedule" method="post">
        @csrf
        <div class="mb-3 mt-5">
            <label for="user_id" class="form-label">Nama Karyawan</label>
            <select class="form-select" name="user_id" id="user_id">
                <option value="" selected disabled>-</option>
                @foreach ($users as $user)
                <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="shift_schedule_id" class="form-label">Shift Schedule</label>
            <select class="form-select" id="shift_schedule_id" name="shift_schedule_id">
                <option value="">--</option>
                @foreach ($shiftSchedules as $shiftSchedule)
                <option value="{{ $shiftSchedule->id }}">{{ $shiftSchedule->shift_schedule_name }}</option>
                @endforeach
            </select>
            @if($shiftSchedules->isEmpty())
            <div id="shift_schedule_id" class="form-text">Silahkakn buat dulu shift schedulenya <a
                    href="/shift-schedule" target="_blank">disini</a></div>
            @endif
        </div>
        <div class="mb-3">
            <label for="keterangan" class="form-label">Keterangan</label>
            <textarea class="form-control" id="keterangan" name="keterangan" disabled cols="30" rows="11"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<table class="table table-hover mt-5">
    <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Shift Schedule Name</th>
            <th scope="col">Start Shift Date</th>
            <th scope="col">End Shift Date</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($shiftScheduleLists as $shiftScheduleList)
        <tr>
            <th scope="row">{{ $shiftScheduleList->full_name }}</th>
            <td>{{ $shiftScheduleList->shift_schedule_name }}</td>
            <td>{{ $shiftScheduleList->start_shift_pattern_date }}</td>
            <td>{{ $shiftScheduleList->end_shift_pattern_date }}</td>
            <td>
                <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                <form action="/user/tambah-shift-schedule/delete/{{ $shiftScheduleList->email }}" method="POST"
                    class="d-inline">
                    @csrf
                    @method('delete')
                    <button type="submit" class="badge bg-danger border-0"
                        onclick="return confirm('are you sure?')">delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    $('#shift_schedule_id').change(function(){
    var shift_schedule_id = $(this).val();
    $.ajax({
                type : 'GET',
                url : '/user/tambah-shift-schedule?shift_schedule_id='+shift_schedule_id,
                dataType : 'JSON',
                success : function(result){
                    console.log(result['monday'])
                    $('textarea#keterangan').val(`
                      start date : `+ result['shift_schedule']['start_shift_pattern_date'] +`
                      end date : `+ result['shift_schedule']['end_shift_pattern_date'] +`
                      monday : `+ result['monday'] +`
                      tuesday : `+ result['tuesday'] +`
                      wednesday : `+ result['wednesday'] +`
                      thursday : `+ result['thursday'] +`
                      friday : `+ result['friday'] +`
                      saturday : `+ result['saturday'] +`
                      sunday : `+ result['sunday'] +`
                    `);
                }
            })
  });
</script>
@endsection

@extends('layouts.main')

@section('content')
<a href="/user">kembali</a>
    <h3>Tambah Salary ke User</h3>
    @csrf
    <div class="col-6">

        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/user/tambah-salary" method="POST">
            @csrf
            <label for="user_id">User Name</label>
            <select class="form-select" name="user_id" id="user_id">
                <option value="" selected disabled>-</option>
                @foreach ($users as $user)
                <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                @endforeach
            </select>
            <label for="salary_id">Salary Name</label>
            <select class="form-select" name="salary_id" id="salary_id">
                <option value="" selected disabled>-</option>
                @foreach ($salaries as $salary)
                <option value="{{ $salary->id }}">{{ $salary->salary_name }}</option>
                @endforeach
            </select>
            <label for="salary_id">Salary Value</label>
            <input type="text" id="nominal" disabled>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    
    <table class="table table-hover mt-5">
        <thead>
          <tr>
            <th scope="col">User Name</th>
            <th scope="col">Salary Name</th>
            <th scope="col">Salary Value</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($user_salaries as $user_salary)
            <tr>
              <th scope="row">{{ $user_salary->full_name }}</th>
              <td>{{ $user_salary->salary_name }}</td>
              <td>Rp. {{ number_format($user_salary->salary_value,0,',','.') }}</td>
              <td>
                  <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                  <a href="#" class="badge bg-danger text-decoration-none">delete</a>
              </td>
            </tr>
            @endforeach
        </tbody>
      </table>




    

    <script>
        $('#salary_id').change(function(){
          var salary_id = $(this).val();
          $.ajax({
              type : 'GET',
              url : '/user/tambah-salary?salary_id='+salary_id,
              dataType : 'JSON',
              success : function(result){
                  console.log(result['salary_value'])
                  $('#nominal').val(` Rp `+ result['salary_value'].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
              }
          })
        });
      </script>
@endsection
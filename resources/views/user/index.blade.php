@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<div>
    <a href="/user/tambah-user-baru" class="btn btn-success mt-5">Tambah User Baru</a>
</div>
<div>
    <a href="/user/edit-profile" class="btn btn-warning mt-5">Edit User Profile</a>
</div>
<div>
    <a href="/user/tambah-shift-schedule" class="btn btn-primary mt-5">Tambahkan Shift Schedule ke User</a>
</div>
<div>
    <a href="/user/tambah-salary" class="btn btn-warning mt-5">Tambahkan Salary ke User</a>
</div>
@endsection
@extends('layouts.main')

@section('content')
    <a href="/">kembali</a>
    <h3 class="mt-4 mb-5">Add Job Level To User</h3>
<div class="col-4">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/job-level-user" method="POST">
        @csrf
        <div class="mb-3">
          <label for="user_id" class="form-label">User</label>
          <select class="form-select" id="user_id" name="user_id">
            <option selected disabled>--</option>
            @foreach ($users as $user)
            <option value="{{ $user->id }}">{{ $user->full_name }}</option>
            @endforeach
          </select>
        </div>
        <div class="mb-3">
          <label for="job_level_id" class="form-label">Job Level</label>
          <select class="form-select" id="job_level_id" name="job_level_id">
            <option selected disabled>--</option>
            @foreach ($job_levels as $job_level)
            <option value="{{ $job_level->id }}">{{ $job_level->level_name }}</option>
            @endforeach
          </select>
          @if($job_levels->isEmpty())
          <div class="form-text">Silahkan buat dulu job levelnya <a href="/job-level" target="_blank">disini</a></div>
          @endif
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</div>

<table class="table table-hover mt-5">
    <thead>
      <tr>
        <th scope="col">User</th>
        <th scope="col">Job Level</th>
        <th scope="col" class="text-center">Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($user_job_levels as $user_job_level)
        <tr>
          <th scope="row">{{ $user_job_level->full_name }}</th>
          <td>{{ $user_job_level->level_name }}</td>
          <td class="text-center">
              <a href="#" class="badge bg-warning text-decoration-none">edit</a>
          </td>
        </tr>
        @endforeach
    </tbody>
  </table>
@endsection
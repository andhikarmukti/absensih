@extends('layouts.main')

@section('content')
<a href="/user">kembali</a>
<h3>Tambah User Baru (halaman ini hanya bisa diakses oleh admin)</h3>

<div class="col-6 mt-5">

    {{-- alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('deactive'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('deactive') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/user/tambah-user-baru" method="POST">
        @csrf
        <div class="mb-3">
            <label for="full_name" class="form-label">Full Name</label>
            <input type="text" class="form-control" id="full_name" name="full_name">
        </div>
        <div class="mb-3">
            <label for="username" class="form-label">Username</label>
            <input type="text" class="form-control" id="username" name="username">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>
        <div class="mb-3">
            <label for="branch_id" class="form-label">Branch</label>
            <select class="form-select" name="branch_id" id="branch_id">
                @foreach ($branches as $branch)
                <option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                @endforeach
            </select>
            <div id="shift_id" class="form-text">Tambah branch <a href="/branch-company" target="_blank">disini</a>
            </div>
        </div>
        <div class="mb-3">
            <label for="entity_id" class="form-label">Entity</label>
            <select class="form-select" name="entity_id" id="entity_id">
                @foreach ($entities as $entity)
                <option value="{{ $entity->id }}">{{ $entity->entity_name }}</option>
                @endforeach
            </select>
            <div id="shift_id" class="form-text">Tambah Entity <a href="/entity" target="_blank">disini</a></div>
        </div>
        <div class="mb-3">
            <label for="department_id" class="form-label">Deparment</label>
            <select class="form-select" name="department_id" id="department_id">
                @foreach ($departments as $department)
                <option value="{{ $department->id }}">{{ $department->department_name }}</option>
                @endforeach
            </select>
            <div id="shift_id" class="form-text">Tambah department <a href="/department" target="_blank">disini</a>
            </div>
        </div>
        <div class="mb-3">
            <label for="office_position_id" class="form-label">Office Position</label>
            <select class="form-select" name="office_position_id" id="office_position_id" required>
                <option value="" selected disabled>--</option>
                @foreach ($office_positions as $office_position)
                <option value="{{ $office_position->id }}">{{ $office_position->position_name }}</option>
                @endforeach
            </select>
            <div id="shift_id" class="form-text">Tambah office position <a href="/office-position"
                    target="_blank">disini</a></div>
        </div>
        <div class="mb-3">
            <label for="job_level_id" class="form-label">Job Level</label>
            <select class="form-select" name="job_level_id" id="job_level_id" required>
                <option value="" selected disabled>--</option>
                @foreach ($job_levels as $job_level)
                <option value="{{ $job_level->id }}">{{ $job_level->level_name }}</option>
                @endforeach
            </select>
            <div id="shift_id" class="form-text">Tambah job level <a href="/job-level" target="_blank">disini</a></div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<table class="table table-hover mt-5">
    <thead>
        <tr>
            <th scope="col">User Name</th>
            @if(auth()->user()->role == 'superadmin')
            <th scope="col">Company</th>
            @endif
            <th scope="col">Branch Company</th>
            <th scope="col">Entity</th>
            <th scope="col">Department</th>
            <th scope="col">Office Position</th>
            <th scope="col" class="text-center">Verification Status</th>
            @if(auth()->user()->role == 'superadmin')
            <th scope="col" class="text-center">Action</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach ($user_companies as $user_company)
        <tr>
            <th scope="row">{{ $user_company->full_name }}</th>
            @if(auth()->user()->role == 'superadmin')
            <td>{{ $user_company->company->company_name }}</td>
            @endif
            <td>{{ $user_company->branch->branch_name }}</td>
            <td>{{ $user_company->entity->entity_name }}</td>
            <td>{{ $user_company->department->department_name }}</td>
            <td>{{ $user_company->officeposition->position_name }}</td>
            <th scope="row" class="text-center"><svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img"
                    aria-label="Info:">
                    <use xlink:href="{{ $user_company->is_active === 1 ? " #check-circle-fill"
                        : "#exclamation-triangle-fill" }}" class="{{ $user_company->is_active === 1 ? " text-success"
                        : "text-danger" }}" />
                </svg></th>
            @if(auth()->user()->role == 'superadmin')
            <td>
                <a href="/user/tambah-user-baru/active/{{ $user_company->id }}"
                    class="badge bg-success text-decoration-none" onclick="return confirm('anda yakin?')">active</a>
                <a href="/user/tambah-user-baru/deactive/{{ $user_company->id }}"
                    class="badge bg-danger text-decoration-none" onclick="return confirm('anda yakin?')">deactive</a>
            </td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

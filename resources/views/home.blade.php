@extends('layouts.main')

@section('content')
<div class="mb-1">
    <h3>Absen</h3>
    <a href="/absen" class="btn btn-primary mt-1">Absen</a>
</div>
<hr>
<div class="mb-1">
    <h3>Branch</h3>
    <a href="/branch-company" class="btn btn-success mt-1">Branch Company</a>
    <a href="/entity" class="btn btn-success mt-1">Entity</a>
    <a href="/department" class="btn btn-success mt-1">Department</a>
</div>
<hr>
<div class="mb-1">
    <h3>Group</h3>
    <a href="/group" class="btn btn-outline-danger mt-1">Group</a>
    <a href="/group-users" class="btn btn-outline-danger mt-1">Group Users</a>
</div>
<hr>
<div class="mb-1">
    <h3>Salary</h3>
    <a href="/salary" class="btn btn-secondary mt-1">Salary</a>
</div>
<hr>
<div class="mb-1">
    <h3>Shift</h3>
    <a href="/shift" class="btn btn-info mt-1">Shift</a>
    <a href="/shift-schedule" class="btn btn-info mt-1">Shift Schedule</a>
    <a href="/change-shift" class="btn btn-info mt-1">Change Shift</a>
    <a href="/change-shift/list-request" class="btn btn-info mt-1">List Request Change Shift</a>
    <a href="/dynamic-shift-change" class="btn btn-info mt-1">Dynamic Shift Change</a>
</div>
<hr>
<div class="mb-1">
    <h3>Struktur Organisasi</h3>
    <a href="/organizational-structure" class="btn btn-outline-secondary mt-1">Organizational Structure</a>
</div>
<hr>
<div class="mb-1">
    <h3>User</h3>
    <a href="/user" class="btn btn-warning mt-1">User</a>
</div>
<hr>
<div class="mb-1">
    <h3>Attendance Correction</h3>
    <a href="/koreksi-absensi" class="btn btn-danger mt-1">Request Attendance Correction</a>
    <a href="/koreksi-absensi/list-request" class="btn btn-danger mt-1">List Request Attendance Correction</a>
</div>
<hr>
<div class="mb-1">
    <h3>Allowance</h3>
    <a href="/allowance" class="btn btn-outline-primary mt-1">Allowance</a>
</div>
<hr>
<div class="mb-1">
    <h3>Employee Allowance</h3>
    <a href="/employee-allowance-base-on-allowance" class="btn btn-success mt-1">Employee Allowance (base on
        allowance)</a>
    <a href="/employee-allowance-base-on-user" class="btn btn-success mt-1">Employee Allowance (base on user)</a>
</div>
<hr>
<div class="mb-1">
    <h3>Formula</h3>
    <a href="/formula" class="btn btn-primary mt-1">Formula</a>
</div>
<hr>
<div class="mb-1">
    <h3>Overtime</h3>
    <a href="/overtime" class="btn btn-primary mt-1">Request Overtime</a>
    <a href="/overtime/list-request" class="btn btn-primary mt-1">List Request Overtime</a>
</div>
<hr>
<div class="mb-1">
    <h3>Office Position</h3>
    <a href="/office-position" class="btn btn-warning mt-1">Add Office Position</a>
</div>
<hr>
<div class="mb-1">
    <h3>Paid Leave (Cuti)</h3>
    <a href="/paid-leave/add-paid-leave" class="btn btn-outline-danger mt-1">Add paid leave type</a>
    <a href="/paid-leave" class="btn btn-outline-danger mt-1">Request paid leave</a>
    <a href="/paid-leave/list-request" class="btn btn-outline-danger mt-1">List Request paid leave</a>
    <a href="/paid-leave/paid-leave-access" class="btn btn-outline-danger mt-1">Paid Leave Access</a>
</div>
<hr>
<div class="mb-1">
    <h3>Reimburse</h3>
    <a href="/reimburse" class="btn btn-secondary mt-1">Make Reimburse Request</a>
    <a href="/reimburse/list-request" class="btn btn-secondary mt-1">List Request Reimburse</a>
</div>
<hr>
<div class="mb-1">
    <h3>Job Level</h3>
    <a href="/job-level" class="btn btn-warning mt-1">Add New Job Level</a>
    <a href="/job-level-user" class="btn btn-warning mt-1">Add New Job Level To User</a>
</div>
<hr>
@endsection

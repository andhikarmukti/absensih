@extends('layouts.main')

@section('content')

{{-- Alert --}}
@if(session()->has('checkin'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('checkin') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if(session()->has('checkout'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    {{ session('checkout') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if(session()->has('noImage'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('noImage') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if(session()->has('sudah melewati batas jam absen'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('sudah melewati batas jam absen') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if(session()->has('tidak bisa absen'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('tidak bisa absen') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if(session()->has('tidak ada shift schedule'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('tidak ada shift schedule') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if(session()->has('sedang cuti'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('sedang cuti') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
@if(session()->has('jam kerja kurang'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('jam kerja kurang') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<a href="/">kembali</a>
<h1 class="h2">Hallo, {{ auth()->user()->full_name }}!</h1>

<form action="/absen/{{ auth()->user()->id }}" method="post" enctype="multipart/form-data">
    @csrf
    <div id="my_camera" class="d-flex justify-content-center"></div>
    <br />
    <input type="hidden" name="image" class="image-tag"> {{-- base64 --}}
    <input type="button" value="Take Photo" class="btn btn-warning" name="name" onClick="take_snapshot()">
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/absen/history" class="btn btn-info">History Absen</a>
    <div class="col-md-12 text-center">
        preview
        <div class="col-lg" id="results"></div>
</form>

<script language="JavaScript">
    Webcam.set({
        width: 800,
        height: 600,
        image_format: 'jpeg',
        jpeg_quality: 90
    });

    Webcam.attach( '#my_camera' );

    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        } );
    }
</script>
@endsection

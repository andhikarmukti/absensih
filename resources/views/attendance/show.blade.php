@extends('layouts.main')

@section('content')
<a href="/absen">kembali</a>
<div class="col-lg mt-5">
    <div class="col-5">
        @if(auth()->user()->role == 'superadmin')
        <form action="/absen/history" method="get">
            <select class="form-select" name="company_id">
                @foreach ($companies as $company)
                <option value="{{ $company->id }}" @if(request('company_id')==$company->id) selected @endif>{{
                    $company->company_name }}</option>
                @endforeach
            </select>
            <button class="btn btn-primary mb-3 mt-1" type="submit">Search</button>
        </form>
        @endif
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">Full Name</th>
                <th scope="col">Checkin</th>
                <th scope="col">CheckOut</th>
                <th scope="col">Total Working Hours</th>
                <th scope="col">Arrival Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($attendances as $attendance)
            <tr>
                <th scope="row">{{ $attendance->user->full_name }}</th>
                <td>{{ $attendance->checkin_time }}</td>
                <td>{{ $attendance->checkout_time }}</td>
                <td>{{ $attendance->total_working_hours }}</td>
                <td style="{{ $attendance->arrival_status == 'on time' ? 'color:green' : 'color:red'}}">{{
                    $attendance->arrival_status }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

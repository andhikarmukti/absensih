@extends('layouts.main')

@section('content')
    <a href="/">kembali</a>
    <h3 class="mb-5">Create New Formula</h3>
    <div class="col-4">

        {{-- Alert --}}
        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/formula" method="post">
            @csrf
            <div class="mb-3">
                <label for="formula_name" class="form-label">Formula Name</label>
                <input type="text" class="form-control" id="formula_name" name="formula_name">
            </div>
            <div class="mb-3">
                <label for="formula" class="form-label">Formula</label>
                <input type="text" class="form-control" id="formula" name="formula">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <table class="table table-hover mt-5">
        <thead>
            <tr>
              <th scope="col">Formula Name</th>
              <th scope="col">Formula</th>
              <th scope="col" class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($formulas as $formula)
            <tr>
              <th scope="row">{{ $formula->formula_name }}</th>
              <td>{{ $formula->formula }}</td>
              <td class="text-center">
                  <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                  <a href="#" class="badge bg-danger text-decoration-none">delete</a>
              </td>
            </tr>
            @endforeach
        </tbody>
      </table>
@endsection
@extends('layouts.main')

@section('content')
    <a href="/">kembali</a>
    <h3 class="mt-5 mb-4">Create New Allowance</h3>
    <div class="col-4">

        {{-- Alert --}}
        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        
        <form action="/allowance" method="POST">
            @csrf
            <div class="mb-3">
              <label for="allowance_name" class="form-label">Allowance Name</label>
              <input type="text" class="form-control" id="allowance_name" name="allowance_name">
            </div>
            <div class="mb-3">
              <label for="allowance_value" class="form-label">Allowance Value</label>
              <input type="text" class="form-control" id="allowance_value" name="allowance_value">
            </div>
            <div class="mb-3">
              <label for="formula_id" class="form-label">Formula</label>
              <select class="form-select" id="formula_id" name="formula_id">
                  @foreach ($formulas as $formula)
                  <option value="{{ $formula->id }}">{{ $formula->formula_name }}</option>
                  @endforeach
              </select>
              <div id="shift_id" class="form-text">Silahkan buat formula <a href="/formula" target="_blank">disini</a></div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>

    <table class="table table-hover mt-5">
        <thead>
          <tr>
            <th scope="col">Allowance Name</th>
            <th scope="col">Allowance Value</th>
            <th scope="col">Formula</th>
            <th scope="col" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($allowances as $allowance)
            <tr>
              <th scope="row">{{ $allowance->allowance_name }}</th>
              <td>Rp {{ number_format($allowance->allowance_value,0,',','.') }}</td>
              <td>{{ $allowance->formula->formula_name }}</td>
              <td class="text-center">
                  <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                  <a href="#" class="badge bg-danger text-decoration-none">delete</a>
              </td>
            </tr>
            @endforeach
        </tbody>
      </table>

      <script type="text/javascript">
		
        var allowance_value = document.getElementById('allowance_value');
        allowance_value.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            allowance_value.value = formatRupiah(this.value);
        });
    
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            allowance_value     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                allowance_value += separator + ribuan.join('.');
            }
    
            allowance_value = split[1] != undefined ? allowance_value + ',' + split[1] : allowance_value;
            return prefix == undefined ? allowance_value : (allowance_value ? '' + allowance_value : '');
        }
    </script>
@endsection
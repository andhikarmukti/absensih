@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h1 class="mb-5">shift schedule page</h1>

<div class="col-4">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('update'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('update') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('delete'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('delete') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/shift-schedule" method="post">
        @csrf
        <label for="shift_schedule_name">Name</label>
        <input type="text" class="form-control" id="shift_schedule_name" name="shift_schedule_name">

        <label for="start_shift_pattern_date">Start Date</label>
        <input type="date" class="form-control" id="start_shift_pattern_date" name="start_shift_pattern_date">

        <label for="end_shift_pattern_date">End Date</label>
        <input type="date" class="form-control" id="end_shift_pattern_date" name="end_shift_pattern_date">

        <label for="monday">Monday</label>
        <select class="form-select" id="monday" name="monday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}">{{ $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="tuesday">Tuesday</label>
        <select class="form-select" id="tuesday" name="tuesday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}">{{ $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="wednesday">Wednesday</label>
        <select class="form-select" id="wednesday" name="wednesday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}">{{ $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="thursday">Thursday</label>
        <select class="form-select" id="thursday" name="thursday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}">{{ $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="friday">Friday</label>
        <select class="form-select" id="friday" name="friday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}">{{ $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="saturday">Saturday</label>
        <select class="form-select" id="saturday" name="saturday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}">{{ $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="sunday">Sunday</label>
        <select class="form-select" id="sunday" name="sunday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}">{{ $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>
        <div id="shift_id" class="form-text">Silahkan buat shift <a href="/shift" target="_blank">disini</a></div>
        <button type="submit" class="btn btn-primary mt-3">Submit</button>
    </form>
</div>

<h4 class="mt-5">List Shift Schedule</h4>
<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">Nama Shift</th>
            <th scope="col">Periode</th>
            <th scope="col">Monday</th>
            <th scope="col">Tuesday</th>
            <th scope="col">Wednesday</th>
            <th scope="col">Thursday</th>
            <th scope="col">Friday</th>
            <th scope="col">Saturday</th>
            <th scope="col">Sunday</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($shiftSchedules as $shiftSchedule)
        <tr>
            <th scope="row">{{ $shiftSchedule->shift_schedule_name }}</th>
            <td>{{ $shiftSchedule->start_shift_pattern_date }} s/d {{ $shiftSchedule->end_shift_pattern_date }}</td>
            {{-- {{ dd($shift->where('id', $shiftSchedule->Monday)->first()->shift_name) }} --}}
            <td
                class="{{ $shift->where('id', $shiftSchedule->Monday)->first() != null ? ($shift->where('id', $shiftSchedule->Monday)->first()->shift_name == 'Shift Off' ? 'text-danger fw-bold' : '') : '' }}">
                {{ $shift->where('id', $shiftSchedule->Monday)->first() != null ? $shift->where('id', $shiftSchedule->Monday)->first()->shift_name : 'deleted' }}</td>
            <td
                class="{{ $shift->where('id', $shiftSchedule->Tuesday)->first() != null ? ($shift->where('id', $shiftSchedule->Tuesday)->first()->shift_name == 'Shift Off' ? 'text-danger fw-bold' : '') : '' }}">
                {{ $shift->where('id', $shiftSchedule->Tuesday)->first() != null ? $shift->where('id', $shiftSchedule->Tuesday)->first()->shift_name : 'deleted' }}</td>
            <td
                class="{{ $shift->where('id', $shiftSchedule->Wednesday)->first() != null ? ($shift->where('id', $shiftSchedule->Wednesday)->first()->shift_name == 'Shift Off' ? 'text-danger fw-bold' : '') : '' }}">
                {{ $shift->where('id', $shiftSchedule->Wednesday)->first() != null ? $shift->where('id', $shiftSchedule->Wednesday)->first()->shift_name : 'deleted' }}</td>
            <td
                class="{{ $shift->where('id', $shiftSchedule->Thursday)->first() != null ? ($shift->where('id', $shiftSchedule->Thursday)->first()->shift_name == 'Shift Off' ? 'text-danger fw-bold' : '') : '' }}">
                {{ $shift->where('id', $shiftSchedule->Thursday)->first() != null ? $shift->where('id', $shiftSchedule->Thursday)->first()->shift_name : 'deleted' }}</td>
            <td
                class="{{ $shift->where('id', $shiftSchedule->Friday)->first() != null ? ($shift->where('id', $shiftSchedule->Friday)->first()->shift_name == 'Shift Off' ? 'text-danger fw-bold' : '') : '' }}">
                {{ $shift->where('id', $shiftSchedule->Friday)->first() != null ? $shift->where('id', $shiftSchedule->Friday)->first()->shift_name : 'deleted' }}</td>
            <td
                class="{{ $shift->where('id', $shiftSchedule->Saturday)->first() != null ? ($shift->where('id', $shiftSchedule->Saturday)->first()->shift_name == 'Shift Off' ? 'text-danger fw-bold' : '') : '' }}">
                {{ $shift->where('id', $shiftSchedule->Saturday)->first() != null ? $shift->where('id', $shiftSchedule->Saturday)->first()->shift_name : 'deleted' }}</td>
            <td
                class="{{ $shift->where('id', $shiftSchedule->Sunday)->first() != null ? ($shift->where('id', $shiftSchedule->Sunday)->first()->shift_name == 'Shift Off' ? 'text-danger fw-bold' : '') : '' }}">
                {{ $shift->where('id', $shiftSchedule->Sunday)->first() != null ? $shift->where('id', $shiftSchedule->Sunday)->first()->shift_name : 'deleted' }}</td>
            <td>
                <a href="/shift-schedule/edit/{{ $shiftSchedule->id }}"
                    class="badge bg-warning text-decoration-none">edit</a>
                <form action="/shift-schedule/delete/{{ $shiftSchedule->id }}" method="POST" class="d-inline">
                    @csrf
                    @method('delete')
                    <button type="submit" class="badge bg-danger border-0"
                        onclick="return confirm('are you sure?')">delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

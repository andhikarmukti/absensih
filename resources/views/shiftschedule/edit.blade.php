@extends('layouts.main')

@section('content')
<a href="/shift-schedule">kembali</a>
<h1 class="mb-5">Edit shift schedule</h1>

<div class="col-4">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/shift-schedule/edit/{{ $shift_schedule->id }}" method="post">
        @csrf
        @method('put')
        <label for="shift_schedule_name">Name</label>
        <input type="text" class="form-control" id="shift_schedule_name" name="shift_schedule_name"
            value="{{ $shift_schedule->shift_schedule_name }}">

        <label for="start_shift_pattern_date">Start Date</label>
        <input type="date" class="form-control" id="start_shift_pattern_date" name="start_shift_pattern_date"
            value="{{ $shift_schedule->start_shift_pattern_date }}">

        <label for="end_shift_pattern_date">End Date</label>
        <input type="date" class="form-control" id="end_shift_pattern_date" name="end_shift_pattern_date"
            value="{{ $shift_schedule->end_shift_pattern_date }}">

        <label for="monday">Monday</label>
        <select class="form-select" id="monday" name="monday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}" {{ $shift->id == $shift_schedule->Monday ? "selected" : "" }}>{{
                $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="tuesday">Tuesday</label>
        <select class="form-select" id="tuesday" name="tuesday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}" {{ $shift->id == $shift_schedule->Tuesday ? "selected" : "" }}>{{
                $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="wednesday">Wednesday</label>
        <select class="form-select" id="wednesday" name="wednesday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}" {{ $shift->id == $shift_schedule->Wednesday ? "selected" : "" }}>{{
                $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="thursday">Thursday</label>
        <select class="form-select" id="thursday" name="thursday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}" {{ $shift->id == $shift_schedule->Thursday ? "selected" : "" }}>{{
                $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="friday">Friday</label>
        <select class="form-select" id="friday" name="friday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}" {{ $shift->id == $shift_schedule->Friday ? "selected" : "" }}>{{
                $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="saturday">Saturday</label>
        <select class="form-select" id="saturday" name="saturday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}" {{ $shift->id == $shift_schedule->Saturday ? "selected" : "" }}>{{
                $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>

        <label for="sunday">Sunday</label>
        <select class="form-select" id="sunday" name="sunday">
            @if($shifts->isEmpty())
            <option value="" disabled selected>Silahkan buat dulu shiftnya</option>
            @else
            @foreach ($shifts as $shift)
            <option value="{{ $shift->id }}" {{ $shift->id == $shift_schedule->Sunday ? "selected" : "" }}>{{
                $shift->shift_name }}</option>
            @endforeach
            @endif
        </select>
        <button type="submit" class="btn btn-primary mt-3" value="{{ $shift_schedule->company_id }}"
            name="company_id">Submit</button>
    </form>
</div>
@endsection

@extends('layouts.main')

@section('content')
<a href="/admin-absensih">kembali</a>
<h3>Add New Company</h3>
<div class="col-4">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/admin-absensih/create-company" method="POST">
        @csrf
        <div class="mb-3">
            <label for="company_name" class="form-label">Company Name</label>
            <input type="text" class="form-control" id="company_name" name="company_name">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<div class="col-4">
    <form action="/admin-absensih/create-company" method="GET">
        <div class="input-group mb-3 mt-5">
            <input type="text" class="form-control" placeholder="Search.." name="keyword"
                value="{{ request('keyword') }}">
            <button class="btn btn-primary" type="submit" id="button-search">Search</button>
            <a class="btn btn-danger" href="/admin-absensih/create-company" id="button-search">Reset</a>
        </div>
    </form>
</div>

<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">Company Name</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($companies as $company)
        <tr>
            <th scope="row">{{ $company->company_name }}</th>
            <td>
                <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                <a href="#" class="badge bg-danger text-decoration-none">delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
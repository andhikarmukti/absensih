@extends('layouts.main')

@section('content')
<a href="/admin-absensih">kembali</a>
<h3 class="mt-5 mb-4">Add New Branch</h3>
<div class="col-4">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/admin-absensih/create-branch" method="POST">
        @csrf
        <div class="mb-3">
            <label for="company_id" class="form-label">Company Name</label>
            <select class="form-select" id="company_id" name="company_id">
                <option selected disabled>--</option>
                @foreach ($companies as $company)
                <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="branch_name" class="form-label">Branch Name</label>
            <input type="text" class="form-control" id="branch_name" name="branch_name">
        </div>
        <div class="mb-3">
            <label for="branch_longitude" class="form-label">Branch Longitude</label>
            <input type="text" class="form-control" id="branch_longitude" name="branch_longitude">
        </div>
        <div class="mb-3">
            <label for="branch_latitude" class="form-label">Branch Latitude</label>
            <input type="text" class="form-control" id="branch_latitude" name="branch_latitude">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<div class="col-4">
    <form action="/admin-absensih/create-branch" method="GET">
        <select class="form-select mt-5 mb-1" id="company_id" name="company_id">
            <option selected disabled>--</option>
            @foreach ($companies as $company)
            <option value="{{ $company->id }}" {{ request('company_id')==$company->id ? 'selected' : "" }}>{{
                $company->company_name }}</option>
            @endforeach
        </select>
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Search.." name="keyword"
                value="{{ request('keyword') }}">
            <button class="btn btn-primary" type="submit" id="button-search">Search</button>
            <a class="btn btn-danger" href="/admin-absensih/create-branch" id="button-search">Reset</a>
        </div>
    </form>
</div>

<table class="table table-hover mt-1">
    <thead>
        <tr>
            <th scope="col">Company Name</th>
            <th scope="col">Branch Name</th>
            <th scope="col">Branch Longitude</th>
            <th scope="col">Branch Latitude</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($branches as $branch)
        <tr>
            <th scope="row">{{ $company->find($branch->company_id)->company_name }}</th>
            <td>{{ $branch->branch_name }}</td>
            <td>{{ $branch->branch_longitude }}</td>
            <td>{{ $branch->branch_latitude }}</td>
            <td>
                <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                <a href="#" class="badge bg-danger text-decoration-none">delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
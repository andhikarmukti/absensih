<!DOCTYPE html>
<html lang="en">

<head>
    <title>Datatables JS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!--DataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" />
    <!--Daterangepicker -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style type="text/css">
        .header h2 {
            font-weight: lighter;
            text-align: center;
            margin: 0
        }

        .header h3 {
            font-weight: lighter;
            text-align: center;
            margin: 0
        }

        .number {
            text-align: right;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="page-header header">
            <h2>Table History Attendance</h2>
        </div>
        <div class="row">
            <div class="col">
                <table id="history-attendance" class="table table-hover table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Name</th>
                            <th>Shift</th>
                            <th>Checkin</th>
                            <th>Checkout</th>
                            <th>Total Working Hours</th>
                            <th>Checkin Image</th>
                            <th>Checkout Image</th>
                            <th>Status</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody class="">
                        @foreach ($histories as $history)
                        <tr>
                            <td>{{ date_format(date_create($history->checkin_time), 'd/m/Y') }}</td>
                            <td>{{ $history->full_name }}</td>
                            <td>{{ $history->shift_name }}</td>
                            <td>{{ date_format(date_create($history->checkin_time), 'H:i:s') }}</td>
                            <td>{{ date_format(date_create($history->checkout_time), 'H:i:s') }}</td>
                            <td>{{ $history->total_working_hours }}</td>
                            <td><a href="/{{ $history->checkin_image }}" target="_blank">open image</a></td>
                            <td><a href="/{{ $history->checkout_image }}" target="_blank">open image</a></td>
                            <td>{{ $history->arrival_status }}</td>
                            <td>{{ $history->description }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr id="sort">
                            <th></th>
                            <th>Name</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!--Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!--Boostrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!--DataTables -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <!--DateRangePicker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript">
        //fungsi untuk filtering data berdasarkan tanggal
        var start_date;
        var end_date;
        var DateFilterFunction = (function(oSettings, aData, iDataIndex) {
            console.log(dateStart);
            var dateStart = parseDateValue(start_date);
            var dateEnd = parseDateValue(end_date);
            //Kolom tanggal yang akan kita gunakan berada dalam urutan 2, karena dihitung mulai dari 0
            //nama depan = 0
            //nama belakang = 1
            //tanggal terdaftar =2
            var evalDate = parseDateValue(aData[0]);
            if ((isNaN(dateStart) && isNaN(dateEnd)) ||
                (isNaN(dateStart) && evalDate <= dateEnd) ||
                (dateStart <= evalDate && isNaN(dateEnd)) ||
                (dateStart <= evalDate && evalDate <= dateEnd)) {
                return true;
            }
            return false;
        });

        // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
        function parseDateValue(rawDate) {
            var dateArray = rawDate.split("/");
            var parsedDate = new Date(dateArray[2], parseInt(dateArray[1]) - 1, dateArray[
                0]); // -1 because months are from 0 to 11
            return parsedDate;
        }

        $(document).ready(function() {
            //konfigurasi DataTable pada tabel dengan id history-attendance dan menambahkan  div class dateseacrhbox dengan dom untuk meletakkan inputan daterangepicker
            $('#history-attendance #sort th').each(function() {
                var keyword = $(this).text();
                if (keyword != '') {
                    var title = $(this).text();
                    $(this).html('<input type="text" style="width:100%" placeholder="Search"/>');
                }
            });

            var $dTable = $('#history-attendance').DataTable({
                initComplete: function() {
                    // Apply the search
                    this.api().columns().every(function() {
                        var that = this;

                        $('input', this.footer()).on('keyup change clear', function() {
                            console.log(this.value);
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    });
                },

                dom: "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            });

            //menambahkan daterangepicker di dalam datatables
            $("div.datesearchbox").html(
                '<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range.."> </div>'
            );

            document.getElementsByClassName("datesearchbox")[0].style.textAlign = "left";

            //konfigurasi daterangepicker pada input dengan id datesearch
            $('#datesearch').daterangepicker({
                autoUpdateInput: false
            });

            //menangani proses saat apply date range
            $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format(
                    'DD/MM/YYYY'));
                start_date = picker.startDate.format('DD/MM/YYYY');
                end_date = picker.endDate.format('DD/MM/YYYY');
                $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
                $dTable.draw();
            });

            $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                start_date = '';
                end_date = '';
                $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(
                    DateFilterFunction, 1));
                $dTable.draw();
            });
        });
    </script>
</body>

</html>

@extends('layouts.main')

@section('content')
<a href="/admin-absensih">kembali</a>
<h3 class="mt-5 mb-4">Add New Department</h3>
<div class="col-5">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/admin-absensih/create-department" method="POST">
        @csrf
        <div class="mb-3">
            <label for="company_name" class="form-label">Company Name</label>
            <select class="form-select" id="company_id" name="company_id">
                <option selected disabled>--</option>
                @foreach ($companies as $company)
                <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                @endforeach
            </select>
        </div>
        <div hidden class="mb-3" id="branch-input">
            <label for="branch_company" class="form-label">Branch Company</label>
            <select class="form-select" id="branch_id" name="branch_id">
                <option selected disabled>--</option>
                @foreach ($branches as $branch)
                <option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                @endforeach
            </select>
        </div>
        <div hidden class="mb-3" id="entity-input">
            <label for="entity_name" class="form-label">Entity Name</label>
            <select class="form-select" id="entity_id" name="entity_id">
                <option selected disabled>--</option>
                @foreach ($branches as $branch)
                <option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                @endforeach
            </select>
        </div>
        <div hidden class="mb-3" id="department-input">
            <label for="department_id" class="form-label">Department Name</label>
            <input type="text" class="form-control" name="department_name" id="department_name">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<table class="table table-hover mt-5">
    <thead>
        <tr>
            <th scope="col">Company Name</th>
            <th scope="col">Branch Name</th>
            <th scope="col">Entity Name</th>
            <th scope="col">Department Name</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data_departments as $data_department)
        <tr>
            <td>{{ $data_department->company_name }}</td>
            <td>{{ $data_department->branch_name }}</td>
            <td>{{ $data_department->entity_name }}</td>
            <th scope="row">{{ $data_department->department_name }}</th>
            <td>
                <a href="/admin-absensih/create-deapartment/edit/{{ $data_department->id }}"
                    class="badge bg-warning text-decoration-none" onclick="return confirm('are you sure?')">edit</a>
                <a href="/admin-absensih/create-deapartment/delete/{{ $data_department->id }}"
                    class="badge bg-danger text-decoration-none" onclick="return confirm('are you sure?')">delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>




<script>
    $('#company_id').change(function(){
        $('#branch-input').removeAttr('hidden');
        var company = $(this).val();
        $.ajax({
            type : 'GET',
            url : '/admin-absensih/create-department?company='+company,
            dataType : 'JSON',
            success : function(result){
                $('#branch_id').empty();
                $('#branch_id').append(`<option selected disabled>--</option>`);
                $.each(result, function(index, element){
                    console.log(result);
                            $('#branch_id').append(`
                                <option value = '`+ element['id'] +`'>`+ element['branch_name'] +`</option>
                            `)
                        })
            }
        })
    })

    $('#branch_id').change(function(){
        $('#entity-input').removeAttr('hidden');
        var branch = $(this).val();
        console.log(branch);
        $.ajax({
            type : 'GET',
            url : '/admin-absensih/create-department?branch='+branch,
            dataType : 'JSON',
            success : function(result){
                $('#entity_id').empty();
                $('#entity_id').append(`<option selected disabled>--</option>`);
                $.each(result, function(index, element){
                    console.log(result);
                            $('#entity_id').append(`
                                <option value = '`+ element['id'] +`'>`+ element['entity_name'] +`</option>
                            `)
                        })
            }
        })
    })

    $('#entity_id').change(function(){
        $('#department-input').removeAttr('hidden');
    })
</script>
@endsection
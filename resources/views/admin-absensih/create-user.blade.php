@extends('layouts.main')

@section('content')
<a href="/admin-absensih">kembali</a>
<h3 class="mt-3 mb-4">Add New User</h3>
<div class="col-5">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/admin-absensih/create-user" method="POST">
        @csrf
        <div class="mb-3">
            <label for="company_name" class="form-label">Company Name</label>
            <select class="form-select" id="company_id" name="company_id">
                <option selected disabled>--</option>
                @foreach ($companies as $company)
                <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                @endforeach
            </select>
        </div>
        <div hidden class="mb-3" id="branch-input">
            <label for="branch_company" class="form-label">Branch Company</label>
            <select class="form-select" id="branch_id" name="branch_id">
                <option selected disabled>--</option>
                @foreach ($branches as $branch)
                <option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                @endforeach
            </select>
        </div>
        <div hidden class="mb-3" id="entity-input">
            <label for="entity_name" class="form-label">Entity Name</label>
            <select class="form-select" id="entity_id" name="entity_id">
                <option selected disabled>--</option>
                @foreach ($branches as $branch)
                <option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                @endforeach
            </select>
        </div>
        <div hidden class="mb-3" id="department-input">
            <label for="department_id" class="form-label">Department Name</label>
            <select class="form-select" id="department_id" name="department_id">
                <option selected disabled>--</option>
                @foreach ($departments as $department)
                <option value="{{ $department->id }}">{{ $department->department_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3" id="department-input">
            <label for="office_position_id" class="form-label">Office Position</label>
            <select class="form-select" id="office_position_id" name="office_position_id">
                <option selected disabled>--</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="full_name" class="form-label">Full Name</label>
            <input type="text" class="form-control" id="full_name" name="full_name">
        </div>
        <div class="mb-3">
            <label for="username" class="form-label">Username</label>
            <input type="text" class="form-control" id="username" name="username">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>
        <div class="mb-3">
            <label for="role" class="form-label">Role User</label>
            <select class="form-select" id="role" name="role">
                <option value="" selected disabled>--</option>
                <option value="staff">Staff</option>
                <option value="admin">Admin</option>
                <option value="superadmin">Super Admin</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<table class="table table-hover mt-5">
    <thead>
        <tr>
            <th scope="col">Company Name</th>
            <th scope="col">Employee Name</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data_users as $data_user)
        <tr>
            <td>{{ $data_user->company_name }}</td>
            <td>{{ $data_user->full_name }}</td>
            <td>
                <a href="/admin-absensih/create-deapartment/edit/{{ $data_user->id }}"
                    class="badge bg-warning text-decoration-none" onclick="return confirm('are you sure?')">edit</a>
                <a href="/admin-absensih/create-deapartment/delete/{{ $data_user->id }}"
                    class="badge bg-danger text-decoration-none" onclick="return confirm('are you sure?')">delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>




<script>
    $('#company_id').change(function(){
        $('#branch-input').removeAttr('hidden');
        var company = $(this).val();
        console.log(company);
        $.ajax({
            type : 'GET',
            url : '/admin-absensih/create-user?company='+company,
            dataType : 'JSON',
            success : function(result){
                $('#branch_id').empty();
                $('#entity_id').empty();
                $('#department_id').empty();
                $('#branch_id').append(`<option selected disabled>--</option>`);
                $.each(result[0], function(index, element){
                    $('#branch_id').append(`
                    <option value = '`+ element['id'] +`'>`+ element['branch_name'] +`</option>
                    `)
                })
                $('#office_position_id').empty();
                $('#office_position_id').append(`<option selected disabled>--</option>`);
                $.each(result[1], function(index, element){
                    $('#office_position_id').append(`
                            <option value = '`+ element['id'] +`'>`+ element['position_name'] +`</option>
                        `)
                })
            }
        })
    })

    $('#branch_id').change(function(){
        $('#entity-input').removeAttr('hidden');
        var branch = $(this).val();
        console.log(branch);
        $.ajax({
            type : 'GET',
            url : '/admin-absensih/create-user?branch='+branch,
            dataType : 'JSON',
            success : function(result){
                $('#entity_id').empty();
                $('#entity_id').append(`<option selected disabled>--</option>`);
                $.each(result, function(index, element){
                    console.log(result);
                            $('#entity_id').append(`
                                <option value = '`+ element['id'] +`'>`+ element['entity_name'] +`</option>
                            `)
                        })
            }
        })
    })

    $('#entity_id').change(function(){
        $('#branch-input').removeAttr('hidden');
        var entity = $(this).val();
        console.log(entity);
        $.ajax({
            type : 'GET',
            url : '/admin-absensih/create-user?entity='+entity,
            dataType : 'JSON',
            success : function(result){
                $('#department_id').empty();
                $('#department_id').append(`<option selected disabled>--</option>`);
                $.each(result, function(index, element){
                    $('#department_id').append(`
                        <option value = '`+ element['id'] +`'>`+ element['department_name'] +`</option>
                    `)
                })
            }
        })
    })

    $('#entity_id').change(function(){
        $('#department-input').removeAttr('hidden');
    })
</script>
@endsection
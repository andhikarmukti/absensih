@extends('layouts.main')

@section('content')
<a href="/admin-absensih">kembali</a>
<h3 class="mt5 mb-4">Add New Entity</h3>

<div class="col-5">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/admin-absensih/create-entity" method="POST">
        @csrf
        <div class="mb-3">
            <label for="company_name" class="form-label">Company Name</label>
            <select class="form-select" id="company_id" name="company_id">
                <option selected disabled>--</option>
                @foreach ($companies as $company)
                <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                @endforeach
            </select>
        </div>
        <div hidden class="mb-3" id="branch-input">
            <label for="branch_company" class="form-label">Branch Company</label>
            <select class="form-select" id="branch_id" name="branch_id">
                <option selected disabled>--</option>
                @foreach ($branches as $branch)
                <option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                @endforeach
            </select>
        </div>
        <div hidden class="mb-3" id="entity-input">
            <label for="entity_name" class="form-label">Entity Name</label>
            <input type="text" class="form-control" name="entity_name" id="entity_name">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<table class="table table-hover mt-5">
    <thead>
        <tr>
            <th scope="col">Company Name</th>
            <th scope="col">Branch Name</th>
            <th scope="col">Entity Name</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data_entities as $data_entity)
        <tr>
            <td>{{ $data_entity->company_name }}</td>
            <td>{{ $data_entity->branch_name }}</td>
            <th scope="row">{{ $data_entity->entity_name }}</th>
            <td>
                <a href="/admin-absensih/create-entity/edit/{{ $data_entity->id }}"
                    class="badge bg-warning text-decoration-none" onclick="return confirm('are you sure?')">edit</a>
                <a href="/admin-absensih/create-entity/delete/{{ $data_entity->id }}"
                    class="badge bg-danger text-decoration-none" onclick="return confirm('are you sure?')">delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>



<script>
    $('#company_id').change(function(){
        $('#branch-input').removeAttr('hidden');
        var company = $(this).val();
        $.ajax({
            type : 'GET',
            url : '/admin-absensih/create-entity?company='+company,
            dataType : 'JSON',
            success : function(result){
                $('#branch_id').empty();
                $('#branch_id').append(`<option selected disabled>--</option>`);
                $.each(result, function(index, element){
                    console.log(result);
                            $('#branch_id').append(`
                                <option value = '`+ element['id'] +`'>`+ element['branch_name'] +`</option>
                            `)
                        })
            }
        })
    })

    $('#branch_id').change(function(){
        $('#entity-input').removeAttr('hidden');
    })
</script>
@endsection
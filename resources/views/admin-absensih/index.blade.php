@extends('layouts.main')

@section('content')
<div>
    <a href="/admin-absensih/create-company" class="badge bg-primary text-decoration-none">Tambah Company</a>
    <a href="/admin-absensih/create-branch" class="badge bg-primary text-decoration-none">Tambah Branch</a>
    <a href="/admin-absensih/create-entity" class="badge bg-primary text-decoration-none">Tambah Entity</a>
    <a href="/admin-absensih/create-department" class="badge bg-primary text-decoration-none">Tambah Department</a>
</div>
<div>
    <a href="/admin-absensih/create-office-position" class="badge bg-success text-decoration-none">Tambah Office Position</a>
</div>
<div>
    <a href="/admin-absensih/create-user" class="badge bg-warning text-decoration-none">Tambah User</a>
</div>
@endsection
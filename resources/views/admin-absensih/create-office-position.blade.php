@extends('layouts.main')

@section('content')
    <a href="/admin-absensih">kembali</a>
    <h3 class="mt-3 mb-5">Add New Office Position</h3>

    <div class="col-5">

        {{-- Alert --}}
        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
    
        <form action="/admin-absensih/create-office-position" method="POST">
            @csrf
            <div class="mb-3">
                <label for="company_name" class="form-label">Company Name</label>
                <select class="form-select" id="company_id" name="company_id">
                    <option selected disabled>--</option>
                    @foreach ($companies as $company)
                    <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3" id="department-input">
                <label for="office_position_id" class="form-label">Office Position</label>
                <input type="text" class="form-control" name="position_name" id="position_name">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    
    <table class="table table-hover mt-5">
        <thead>
            <tr>
                <th scope="col">Company Name</th>
                <th scope="col">Position Name</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($office_positions as $office_position)
            <tr>
                <td>{{ $company->where('id', $office_position->company_id)->first()->company_name }}</td>
                <td>{{ $office_position->position_name }}</td>
                <td>
                    <a href="/admin-absensih/create-deapartment/edit/{{ $office_position->id }}"
                        class="badge bg-warning text-decoration-none" onclick="return confirm('are you sure?')">edit</a>
                    <a href="/admin-absensih/create-deapartment/delete/{{ $office_position->id }}"
                        class="badge bg-danger text-decoration-none" onclick="return confirm('are you sure?')">delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
@extends('layouts.main')

@section('content')
<a href="/shift">kembali</a>
<div class="col-6">

    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <h4 class="mb-3 mt-5">Edit Shift</h4>
    <form action="/shift/edit/{{ $shift->id }}" method="post">
        @csrf
        @method('put')
        <div class="mb-3">
            <label for="shift_name" class="form-label">Shift Name</label>
            <input type="text" class="form-control" id="shift_name" name="shift_name" value="{{ $shift->shift_name }}">
        </div>
        <div class="mb-3">
            <label for="start_shift_hours" class="form-label">Start Shift Hours</label>
            <input type="time" class="form-control" id="start_shift_hours" name="start_shift_hours"
                value="{{ $shift->start_shift_hours }}">
        </div>
        <div class="mb-3">
            <label for="end_shift_hours" class="form-label">End Shift Hours</label>
            <input type="time" class="form-control" id="end_shift_hours" name="end_shift_hours"
                value="{{ $shift->end_shift_hours }}">
        </div>
        <div class="mb-5">
            <input type="checkbox" class="btn-check" id="location_check" name="location_check" autocomplete="off" {{
                $shift->location_check == 'on' ? 'checked' : '' }}>
            <label class="btn btn-outline-primary" for="location_check">Location Check</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection

@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<div class="col-6">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('update'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('update') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('delete'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('delete') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <h4 class="mb-5 mt-5">Tambah Shift</h4>
    <form action="/shift" method="post">
        @csrf
        <input type="hidden" class="form-control" id="company_id" name="company_id"
            value="{{ auth()->user()->company->id }}">
        <div class="mb-3">
            <label for="shift_name" class="form-label">Shift Name</label>
            <input type="text" class="form-control" id="shift_name" name="shift_name">
        </div>
        <div class="mb-3">
            <label for="start_shift_hours" class="form-label">Start Shift Hours</label>
            <input type="time" class="form-control" id="start_shift_hours" name="start_shift_hours">
        </div>
        <div class="mb-3">
            <label for="end_shift_hours" class="form-label">End Shift Hours</label>
            <input type="time" class="form-control" id="end_shift_hours" name="end_shift_hours">
        </div>
        <div class="row">
            <div class="border rounded-pill text-center p-4 mb-3 col-4">
                <div class="mb-1">
                    <input type="checkbox" class="btn-check" id="location_check" name="location_check"
                        autocomplete="off">
                    <label class="btn btn-outline-primary" for="location_check">Location Check</label>
                </div>
                <div class="mb-1">
                    <input type="checkbox" class="btn-check" id="is_flexible" name="is_flexible" autocomplete="off">
                    <label class="btn btn-outline-primary" for="is_flexible">Flexible</label>
                </div>
            </div>
            <div class="col-4">
                <label for="total_working_hours" class="form-label">Total Working Hours</label>
                <div class="col-8">
                    <input readonly placeholder="tes" type="time" class="form-control" id="total_working_hours"
                        name="total_working_hours" value="00:00">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<h4 class="mt-5">List Shift</h4>
<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">Nama Shift</th>
            <th scope="col">Start Shift Hours</th>
            <th scope="col">End Shift Hours</th>
            <th scope="col" class="text-center">Location Check</th>
            <th scope="col" class="text-center">Flexible</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($shifts as $shift)
        <tr>
            <th scope="row">{{ $shift->shift_name }}</th>
            <td>{{ $shift->start_shift_hours }}</td>
            <td>{{ $shift->end_shift_hours }}</td>
            <td class="text-center">{{ $shift->location_check === 'on' ? "Ya" : "" }}</td>
            <td class="text-center">{{ $shift->is_flexible === 'on' ? "Ya" : "" }}</td>
            <td>
                <a href="/shift/edit/{{ $shift->id }}" class="badge bg-warning text-decoration-none">edit</a>
                {{-- <form action="/shift/delete/{{ $shift->id }}" method="POST" class="d-inline">
                    @csrf
                    @method('delete')
                    <button type="submit" class="badge bg-danger border-0"
                        onclick="return confirm('are you sure?')">delete</button>
                </form> --}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    $('#is_flexible').click(function(){
        let disableTotalWorkingHour = $('#total_working_hours').attr("readonly");
        let disableStartShiftHour = $('#start_shift_hours').attr("disabled");
        let disableEndShiftHour = $('#end_shift_hours').attr("disabled");


        !disableStartShiftHour ? $('#start_shift_hours').prop('disabled', true) : $('#start_shift_hours').removeAttr("disabled");
        $('#start_shift_hours').val('--:--');
        $('#end_shift_hours').val('--:--');
        !disableEndShiftHour ? $('#end_shift_hours').prop('disabled', true) : $('#end_shift_hours').removeAttr("disabled");
        disableTotalWorkingHour ? $('#total_working_hours').removeAttr("readonly") : $('#total_working_hours').prop('readonly', true);
    });
    // $('#start_shift_hours').keyup(() => {
    //     let start_hour = $('#start_shift_hours').val();
    //     $.ajax({
    //             type : 'GET',
    //             url : '/shift?start_hour='+start_hour,
    //             dataType : 'JSON',
    //             success : function(result){
    //                 console.log(result)
    //             }
    //         });
    // });
    $('#end_shift_hours').keyup(() => {
        let start_hour = $('#start_shift_hours').val();
        let end_hour = $('#end_shift_hours').val();
        $.ajax({
                type : 'GET',
                url : '/shift?end_hour='+end_hour+'&start_hour='+start_hour,
                dataType : 'JSON',
                success : function(result){
                    $('#total_working_hours').val(result)
                }
            });
    });
</script>
@endsection

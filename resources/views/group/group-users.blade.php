@extends('layouts.main')

@section('content')
    <a href="/">kembali</a>
    <h3>Tambahkan User ke dalam Group</h3>
    <div class="col-6">

        {{-- Alert --}}
        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        @if(session()->has('sudah ada'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('sudah ada') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        @if(session()->has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('delete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/group-users" method="post">
            @csrf
            <div class="mb-3">
              <label for="group_id" class="form-label">Group Name</label>
              <select class="form-select" id="group_id" name="group_id">
                  <option selected disabled>-</option>
                  @foreach ($groups as $group)
                  <option value="{{ $group->id }}">{{ $group->group_name }}</option>
                  @endforeach
                </select>

                <label class="form-label" for="user_id">User Name</label>
                <select class="form-select" id="user_id" name="user_id">
                    <option selected disabled>-</option>
                    @foreach ($users_group as $user_group)
                    <option value="{{ $user_group->id }}">{{ $user_group->full_name }}</option>
                    @endforeach
                  </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>

          <h4 class="mt-5" id="judul_group"></h4>
          <table class="table table-hover" hidden>
            <thead>
              <tr>
                <th scope="col">User Name</th>
                <th scope="col" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="users_name">
            </tbody>
          </table>
    </div>



    <script>
        $('#group_id').change(function(){
          var group_id   = $(this).val();
          var group_name = $('#group_id').children("option:selected").text();
        console.log(group_name)
          $.ajax({
                type : 'GET',
                url : '/group-users?group_id='+group_id,
                dataType : 'JSON',
                success : function(result){
                    $('#users_name').empty();
                    $('#judul_group').text(group_name);
                    $.each( result, function( key, value ) {
                    console.log(value)
                    $('#users_name').append(`
                    <tr>
                        <td>`+ value['full_name'] +`</td>
                        <td class="text-center">
                            <form action="/group-users/delete/`+ value['user_id'] +`" method="POST" class="text-center">
                                @csrf
                                @method('delete')
                                <button type="submit" class="badge bg-danger border-0" name="group_id" value="`+ group_id +`" onclick="return confirm('are you sure?')">delete</button>
                            </form>
                        </td>
                    </tr>
                    `)
                    });
                }
            })
            $('.table').removeAttr('hidden');
        });
      </script>
@endsection

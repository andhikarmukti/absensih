@extends('layouts.main')

@section('content')
    <a href="/">kembali</a>
    <h3>Tambah Group</h3>
    <div class="col-6">

        {{-- Alert --}}
        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        @if(session()->has('update'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ session('update') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        @if(session()->has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('delete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/group" method="post">
            @csrf
            <div class="mb-3">
              <label for="group_name" class="form-label">Group Name</label>
              <input type="text" class="form-control" id="group_name" name="group_name">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>

    <div class="col-8">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Group Name</th>
                <th scope="col" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($groups as $group)
                <tr>
                  <th scope="row">{{ $group->group_name }}</th>
                  <td class="text-center">
                      <a href="/group/edit/{{ $group->id }}" class="badge bg-warning text-decoration-none">edit</a>
                      <form action="/group/delete/{{ $group->id }}" method="POST" class="d-inline">
                        @csrf
                        @method('delete')
                        <button type="submit" class="badge bg-danger border-0" onclick="return confirm('are you sure?')">delete</button>
                    </form>
                  </td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>
@endsection

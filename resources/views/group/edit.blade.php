@extends('layouts.main')

@section('content')
    <a href="/group">kembali</a>
    <h3>Edit Group</h3>
    <div class="col-6">

        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/group/edit/{{ $group->id }}" method="post">
            @csrf
            @method('PUT')
            <div class="mb-3">
              <label for="group_name" class="form-label">Group Name</label>
              <input type="text" class="form-control" id="group_name" name="group_name" value="{{ $group->group_name }}">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>
@endsection

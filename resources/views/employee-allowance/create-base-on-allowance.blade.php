@extends('layouts.main')

@section('content')
    <a href="/">kembali</a>
    <h3 class="mt-5">Add New Employee Allowance (based on allowance)</h3>
    <div class="col-6">

        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        @if(session()->has('sudah ada'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('sudah ada') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
    
        <form action="/employee-allowance" method="post">
            @csrf
            <div class="mb-3">
              <label for="allowance_id" class="form-label">Allowance Name</label>
              <select class="form-select" id="allowance_id" name="allowance_id">
                  <option selected disabled>-</option>
                  @foreach ($allowances as $allowance)
                  <option value="{{ $allowance->id }}">{{ $allowance->allowance_name }}</option>
                  @endforeach
                </select>
                @if($allowances->isEmpty())
                <div id="shift_id" class="form-text">Silahkan buat allowance <a href="/allowance" target="_blank">disini</a></div>
                @endif
                
                <label class="form-label" for="user_id">User Name</label>
                <select class="form-select" id="user_id" name="user_id">
                    <option selected disabled>-</option>
                    @foreach ($users_company as $user_company)
                    <option value="{{ $user_company->id }}">{{ $user_company->full_name }}</option>
                    @endforeach
                  </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>

          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">User Name</th>
                <th scope="col" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="body_table">
            </tbody>
          </table>
    </div>

    <script>
        $('#allowance_id').change(function(){
        var allowance_id   = $(this).val();
        // console.log(allowance_id)
          $.ajax({
                type : 'GET',
                url : '/employee-allowance-base-on-allowance?allowance_id='+allowance_id,
                dataType : 'JSON',
                success : function(result){
                    console.log(result)
                    $('#body_table').empty();
                    $.each( result, function( key, value ) {
                    $('#body_table').append(`
                    <tr>
                        <td>`+ value['full_name'] +`</td>
                        <td class="text-center">
                            <a href="#" class="badge bg-warning text-decoration-none">edit</a>
                            <a href="#" class="badge bg-danger text-decoration-none">delete</a>
                        </td>
                    </tr>
                    `)
                    });
                }
            })
        });
      </script>
@endsection
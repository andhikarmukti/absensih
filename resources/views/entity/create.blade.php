@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
    <h3>Tambah Entity</h3>
    <div class="col-6">

      {{-- Alert --}}
        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        @if(session()->has('update'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ session('update') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        @if(session()->has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('delete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/entity" method="post">
            @csrf
            <label for="branch_id">Branch Name</label>
            <select class="form-select" name="branch_id" id="branch_id">
                @foreach ($branches as $branch)
                <option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                @endforeach
              </select>

            <label for="entity_name">Entity Name</label>
            <input type="text" class="form-control" id="entity_name" name="entity_name">

            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>

    <table class="table table-hover mt-5">
        <thead>
          <tr>
            <th scope="col">Branch Name</th>
            <th scope="col">Entity name</th>
            <th scope="col" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($entities as $entity)
            <tr>
              <th scope="row">{{ $entity->branch_name }}</th>
              <td>{{ $entity->entity_name }}</td>
              <td class="text-center">
                  <a href="/entity/edit/{{ $entity->id }}" class="badge bg-warning text-decoration-none">edit</a>
                  @if(auth()->user()->role == 'superadmin')
                  <a href="/entity/delete/{{ $entity->id }}" class="badge bg-danger text-decoration-none">delete</a>
                  @endif
              </td>
            </tr>
            @endforeach
        </tbody>
      </table>
@endsection

@extends('layouts.main')

@section('content')
    <a href="/entity">kembali</a>
    <h3>Edit Entity</h3>
    <div class="col-6">

        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/entity/edit/{{ $id }}" method="post">
            @csrf
            @method('put')
            <label for="branch_id">Branch Name</label>
            <select class="form-select" name="branch_id" id="branch_id">
                @foreach ($branches as $branch)
                <option value="{{ $branch->id }}" {{ $branch->id == $entity->branch_id ? 'selected' : '' }}>{{ $branch->branch_name }}</option>
                @endforeach
              </select>

            <label for="entity_name">Entity Name</label>
            <input type="text" class="form-control" id="entity_name" name="entity_name" value="{{ $entity->entity_name }}">

            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>
@endsection

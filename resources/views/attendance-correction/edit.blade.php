@extends('layouts.main')

@section('content')
    <a href="/koreksi-absensi">kembali</a>

    <div class="col-6">

      {{-- Alert --}}
      @if(session()->has('berhasil'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ session('berhasil') }}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif
      @if(session()->has('belum ada supervisor id'))
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
          {{ session('belum ada supervisor id') }}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif

        <form action="/koreksi-absensi/{{ $history_attendance->id }}" method="POST">
          @csrf
            {{-- <input type="hidden" value="{{ $history_attendance->id }}" name="history_attendance_id"> --}}
            <div class="mb-3">
              <label for="old_checkin_time" class="form-label">Old Checkin Time</label>
              <input type="text" class="form-control" id="old_checkin_time" name="old_checkin_time" value="{{ $history_attendance->checkin_time }}" readonly>
            </div>
            <div class="mb-3">
              <label for="checkin_time" class="form-label">New Checkin Time</label>
              <input type="time" class="form-control" id="checkin_time" name="checkin_time">
            </div>
            <div class="mb-3">
              <label for="description" class="form-label">Keterangan</label>
              <input type="text" class="form-control" id="description" name="description">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>
@endsection

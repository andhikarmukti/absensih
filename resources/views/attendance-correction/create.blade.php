@extends('layouts.main')

@section('content')
    <a href="/">kembali</a>
    <h3>Tambah Data Attendance Correction</h3>

    <div class="col-10">
        <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">Full Name</th>
                <th scope="col">Checkin</th>
                <th scope="col">CheckOut</th>
                <th scope="col">Total Working Hours</th>
                <th scope="col">Arrival Status</th>
                <th scope="col" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($attendances as $attendance)
                <tr>
                  <th scope="row">{{ $attendance->user->full_name }}</th>
                  <td>{{ $attendance->checkin_time }}</td>
                  <td>{{ $attendance->checkout_time }}</td>
                  <td>{{ $attendance->total_working_hours }}</td>
                  <td>{{ $attendance->arrival_status }}</td>
                  <td class="text-center">
                      <a href="/koreksi-absensi/{{ $attendance->id }}" class="badge bg-success text-decoration-none">koreksi kehadiran</a>
                  </td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>
@endsection
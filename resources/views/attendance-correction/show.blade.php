@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3 class="mt-5 mb-4 text-center">List Request Attendance Correction</h3>

{{-- Alert --}}
<div class="col-6">
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('decline'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('decline') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
</div>

<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Chekin Time Before</th>
            <th scope="col">Chekin Time Correction</th>
            <th scope="col">Description</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($list_request as $lr)
        <tr>
            <th scope="row">{{ $lr->full_name }}</th>
            <td>{{ $lr->checkin_time }}</td>
            <td>{{ $lr->checkin_time_correction }}</td>
            <td>{{ $lr->description }}</td>
            <td>
                <form action="/koreksi-absensi/accept/{{ $lr->id }}" class="d-inline" method="post">
                    @csrf
                    <input type="hidden" name="history_attendance_id" id="history_attendance_id"
                        value="{{ $lr->history_attendance_id }}">
                    <input type="hidden" name="checkin_time_correction" id="checkin_time_correction"
                        value="{{ $lr->checkin_time_correction }}">
                    <input type="hidden" name="user_id" id="user_id" value="{{ $lr->user_id }}">
                    <button type="submit" class="badge bg-success border-0"
                        onclick="return confirm('are you sure?')">accept</button>
                </form>
                <form action="/koreksi-absensi/decline/{{ $lr->id }}" class="d-inline" method="post">
                    @csrf
                    <button type="submit" class="badge bg-danger border-0"
                        onclick="return confirm('are you sure?')">decline</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

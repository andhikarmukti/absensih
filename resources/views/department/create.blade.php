@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
    <h3>Tambah Department</h3>
    <div class="col-4">

      {{-- Alert --}}
        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        @if(session()->has('udpate'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ session('udpate') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif
        @if(session()->has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('delete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/department" method="post">
        @csrf
            <label for="department_name">Entity Name</label>
            <select class="form-select" name="entity_id" id="entity_id">
                @foreach ($entities as $entity)
                <option value="{{ $entity->id }}">{{ $entity->entity_name }}</option>
                @endforeach
            </select>
            <label for="department_name">Deparment Name</label>
            <input type="text" class="form-control" id="department_name" name="department_name">

            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>

    <table class="table table-hover mt-5">
        <thead>
          <tr>
            <th scope="col">Entity Name</th>
            <th scope="col">Department Name</th>
            <th scope="col" class="text-center">Handle</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($departments as $department)
            <tr>
              <th scope="row">{{ $department->entity_name }}</th>
              <td>{{ $department->department_name }}</td>
              <td class="text-center">
                  <a href="/department/edit/{{ $department->id }}" class="badge bg-warning text-decoration-none">edit</a>
                  <a href="/department/delete/{{ $department->id }}" class="badge bg-danger text-decoration-none">delete</a>
              </td>
            </tr>
            @endforeach
        </tbody>
      </table>
@endsection

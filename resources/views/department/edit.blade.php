@extends('layouts.main')

@section('content')
<a href="/department">kembali</a>
    <h3>Edit Department</h3>
    <div class="col-4">

        @if(session()->has('berhasil'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        @endif

        <form action="/department/edit/{{ $id }}" method="post">
        @csrf
        @method('PUT')
            <label for="department_name">Entity Name</label>
            <select class="form-select" name="entity_id" id="entity_id">
                @foreach ($entities as $entity)
                <option value="{{ $entity->id }}" {{ $entity->id == $department->entity_id ? 'selected' : '' }}>{{ $entity->entity_name }}</option>
                @endforeach
            </select>
            <label for="department_name">Deparment Name</label>
            <input type="text" class="form-control" id="department_name" name="department_name" value="{{ $department->department_name }}">

            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>
@endsection

@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3>Tambah data struktur organisasi (menentukan supervisor)</h3>

<div class="col-4 mt-5">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('sudah ada'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('sudah ada') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('level tidak sesuai'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('level tidak sesuai') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('kuning'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('kuning') }} <a href="/job-level-user" class="alert-link" target="_blank">di sini</a>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('update'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('update') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('delete'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('delete') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('supervisor level error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('supervisor level error') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/organizational-structure" method="POST">
        @csrf
        <label for="user_id">User Name</label>
        <select class="form-select" id="user_id" name="user_id">
            <option value="" selected disabled>-</option>
            @foreach ($users_company as $user_company)
            <option value="{{ $user_company->id }}">{{ $user_company->full_name }}</option>
            @endforeach
        </select>

        <label for="supervisor_id">Supervisor Name</label>
        <select class="form-select" id="supervisor_id" name="supervisor_id">
            <option value="" selected disabled>-</option>
            @foreach ($users_company as $user_company)
            <option value="{{ $user_company->id }}">{{ $user_company->full_name }}</option>
            @endforeach
        </select>
        <button type="submit" class="btn btn-primary mt-3">Submit</button>
    </form>
</div>

<h3 class="mt-5">Tabel Struktur Organisasi</h3>
<table class="table table-hover mt-4">
    <thead>
        <tr>
            <th scope="col">User Name</th>
            <th scope="col">Supervisor Name</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($organizationalStructures as $organizationalStructure)
        <tr>
            <th scope="row">{{ $organizationalStructure->full_name }}</th>
            <td>{{ $supervisor->find($organizationalStructure->supervisor_id)->full_name }}</td>
            <td class="text-center">
                <a href="/organizational-structure/edit/{{ $organizationalStructure->id }}"
                    class="badge bg-warning text-decoration-none">edit</a>
                <form action="/organizational-structure/delete/{{ $organizationalStructure->id }}" method="POST"
                    class="d-inline">
                    @csrf
                    @method('delete')
                    <button type="submit" class="badge bg-danger border-0"
                        onclick="return confirm('are you sure?')">delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

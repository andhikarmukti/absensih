@extends('layouts.main')

@section('content')
<a href="/organizational-structure">kembali</a>
<h3>Tambah data struktur organisasi (menentukan supervisor)</h3>

<div class="col-4 mt-5">
    <form action="/organizational-structure/edit/{{ $id }}" method="POST">
        @csrf
        @method('put')
        <label for="user_id">User Name</label>
        <select class="form-select" id="user_id" name="user_id">
            <option value="{{ $user->id }}" selected>{{ $user->full_name }}</option>
        </select>

        <label for="supervisor_id">Supervisor Name</label>
        <select class="form-select" id="supervisor_id" name="supervisor_id">
            <option value="" selected disabled>-</option>
            @foreach ($users_company as $user_company)
            <option value="{{ $user_company->id }}" {{ $organizationalStructure->supervisor_id == $user_company->id ? 'selected' : "" }}> {{ $user_company->full_name }}</option>
            @endforeach
        </select>
        <button type="submit" class="btn btn-primary mt-3">Submit</button>
    </form>
</div>
@endsection

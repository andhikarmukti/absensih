@extends('layouts.main')

@section('content')
    <a href="/">kembali</a>
    <h3 class="mt-4 mb-4">Add New Job Level</h3>
<div class="col-4">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('berhasil') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/job-level" method="POST">
        @csrf
        <div class="mb-3">
          <label for="level_name" class="form-label">Level Name</label>
          <input type="text" class="form-control" id="level_name" name="level_name">
        </div>
        <div class="mb-3">
            <label for="level_value" class="form-label">Job Level Value</label>
            <input type="number" class="form-control" id="level_value" name="level_value">
            <div id="emailHelp" class="form-text">Semakin kecil angkanya, maka semakin tinggi level jabatannya.</div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</div>

<table class="table table-hover mt-5">
  <thead>
    <tr>
      <th scope="col">Job Level Name</th>
      <th scope="col">Job Level Value</th>
      <th scope="col" class="text-center">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($job_levels as $job_level)
    <tr>
      <th scope="row">{{ $job_level->level_name }}</th>
      <td>{{ $job_level->level_value }}</td>
      <td class="text-center">
        <a href="/job-level/edit/{{ $job_level->id }}" class="badge bg-warning text-decoration-none">edit</a>
        <a href="/job-level/accept{{ $job_level->id }}" class="badge bg-danger text-decoration-none">delete</a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection
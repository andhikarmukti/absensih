@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3>Tambah Branch</h3>
<div class="col-6">

    {{-- Alert --}}
    @if(session()->has('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('update'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        {{ session('update') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if(session()->has('delete'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ session('delete') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    <form action="/branch-company" method="post">
        @csrf
        <input type="hidden" class="form-control" id="company_id" name="company_id" value="{{ $company_id }}">

        <label for="branch_name">Branch Name</label>
        <input type="text" class="form-control" id="branch_name" name="branch_name">

        <label for="branch_longitude">Longitude</label>
        <input type="text" class="form-control" id="branch_longitude" name="branch_longitude">

        <label for="branch_latitude">Latitude</label>
        <input type="text" class="form-control" id="branch_latitude" name="branch_latitude">

        <button type="submit" class="btn btn-primary mt-3">Submit</button>
    </form>
</div>
<h3 class="mt-5">Company : {{ auth()->user()->company->company_name }}</h3>
<table class="table table-hover mt-4">
    <thead>
        <tr>
            <th scope="col">Branch Name</th>
            <th scope="col">Longitude</th>
            <th scope="col">Latitude</th>
            <th scope="col" class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($branch_companies as $branch_company)
        <tr>
            <th scope="row">{{ $branch_company->branch_name }}</th>
            <td>{{ $branch_company->branch_longitude }}</td>
            <td>{{ $branch_company->branch_latitude }}</td>
            <td class="text-center">
                <a href="/branch-company/edit/{{ $branch_company->id }}" class="badge bg-warning text-decoration-none">edit</a>
                @if(auth()->user()->role == 'superadmin')
                <a href="/branch-company/delete/{{ $branch_company->id }}" class="badge bg-danger text-decoration-none" onclick="return confirm('ini akan menghapus semua data entity, department, dan user yang bersangkutan. Apakah Anda yakin?')">delete</a>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<script>
    function edit() {
        let answer = confirm('please type : DELETE');
    }
</script>
@endsection

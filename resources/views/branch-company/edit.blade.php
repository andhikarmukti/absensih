@extends('layouts.main')

@section('content')
<a href="/branch-company">kembali</a>
    <h3>Edit Branch</h3>
    <div class="col-6">
        <form action="/branch-company/edit/{{ $id }}" method="post">
            @csrf
            @method('put')
            <label for="branch_name">Branch Name</label>
            <input type="text" class="form-control" id="branch_name" name="branch_name" value="{{ $branch_company->branch_name }}">

            <label for="branch_longitude">Longitude</label>
            <input type="text" class="form-control" id="branch_longitude" name="branch_longitude" value="{{ $branch_company->branch_longitude }}">

            <label for="branch_latitude">Latitude</label>
            <input type="text" class="form-control" id="branch_latitude" name="branch_latitude" value="{{ $branch_company->branch_latitude }}">

            <button type="submit" class="btn btn-primary mt-3">Submit</button>
        </form>
    </div>
@endsection

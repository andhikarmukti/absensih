@extends('layouts.main')

@section('content')
    <a href="/">kembali</a>
    <h3>Pengajuan change shift</h3>
    <div class="col-6 mt-5">

      @if(session()->has('berhasil'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ session('berhasil') }}
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      @endif
    
        <form action="/change-shift" method="post">
          @csrf
            <div class="mb-3">
              <label for="start_shift_date" class="form-label">Start Shift Date</label>
              <input type="date" class="form-control" id="start_shift_date" name="start_shift_date">
            </div>
            <div class="mb-3">
              <label for="end_shift_date" class="form-label">End Shift Date</label>
              <input type="date" class="form-control" id="end_shift_date" name="end_shift_date">
            </div>
            <label for="shift_id" class="form-label">Shift</label>
            <select class="form-select" id="shift_id" name="shift_id">
              <option value="" selected disabled>-</option>
              @foreach ($shifts_company as $shift_company)
              <option value="{{ $shift_company->id }}">{{ $shift_company->shift_name }}</option>
              @endforeach
              </select>
            <button type="submit" class="btn btn-primary mt-4">Submit</button>
          </form>
    </div>
@endsection
@extends('layouts.main')

@section('content')
<a href="/">kembali</a>
<h3>List Request Change Shift</h3>

<div class="col-8">

  {{-- Alert --}}
  @if(session()->has('accept'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('accept') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
  @endif
  @if(session()->has('decline'))
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('decline') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
  @endif

  <table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">User Name</th>
        <th scope="col">Start Shift Date</th>
        <th scope="col">End Shift Date</th>
        <th scope="col">Shift</th>
        <th scope="col">Approval</th>
        <th scope="col" class="text-center">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($list_request_change_shift as $list_request)
      <tr>
        <th scope="row">{{ auth()->user()->where('id', $list_request->user_id)->first()->full_name }}</th>
        <td>{{ $list_request->start_shift_date }}</td>
        <td>{{ $list_request->end_shift_date }}</td>
        <td>{{ $shift->where('id', $list_request->shift_id)->first()->shift_name }}</td>
        <td>{{ $list_request->approval }}</td>
        <td class="text-center">
          <form action="/change-shift/list-request/accept/{{ $list_request->id }}" method="POST" class="d-inline">
            @csrf
            @method('put')
            <button type="submit" class="badge bg-success border-0" value="{{ $list_request->id }}" name="accept" onclick="return confirm('are you sure?')">accept</button>
          </form>
          <form action="/change-shift/list-request/decline/{{ $list_request->id }}" method="POST" class="d-inline">
            @csrf
            @method('put')
            <button type="submit" class="badge bg-danger border-0" value="{{ $list_request->id }}" name="decline" onclick="return confirm('are you sure?')">decline</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection



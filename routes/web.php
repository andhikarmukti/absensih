<?php

use App\Models\User;
use App\Models\Reimburse;
use GuzzleHttp\Middleware;
use App\Models\ChangeShift;
use App\Models\ShiftSchedule;
use App\Models\PaidLeaveHistory;
use App\Models\HistoryAttendance;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\ShiftController;
use App\Http\Controllers\EntityController;
use App\Http\Controllers\SalaryController;
use App\Http\Controllers\FormulaController;
use App\Http\Controllers\OvertimeController;
use App\Http\Controllers\AllowanceController;
use App\Http\Controllers\ReimburseController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\ChangeShiftController;
use App\Http\Controllers\AdminAbsensihController;
use App\Http\Controllers\BranchCompanyController;
use App\Http\Controllers\ShiftScheduleController;
use App\Http\Controllers\OfficePositionController;
use App\Http\Controllers\PaidLeaveAccessController;
use App\Http\Controllers\PaidLeaveHistoryController;
use App\Http\Controllers\EmployeeAllowanceController;
use App\Http\Controllers\HistoryAttendanceController;
use App\Http\Controllers\AttendanceCorrectionController;
use App\Http\Controllers\DynamicShiftChangeController;
use App\Http\Controllers\HolidayController;
use App\Http\Controllers\JobLevelController;
use App\Http\Controllers\OrganizationalStructureController;
use App\Http\Controllers\PaidLeaveController;
use App\Models\DynamicShiftChange;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';

Route::get('/', function () {
    return view('home');
})->middleware('auth');

// Absen
Route::get('/absen', [HistoryAttendanceController::class, 'create'])->middleware('auth');
Route::get('/absen/history', [HistoryAttendanceController::class, 'show'])->middleware('auth');
Route::post('/absen/{id}', [HistoryAttendanceController::class, 'store'])->middleware('auth');

// Shift
Route::get('/shift', [ShiftController::class, 'create'])->middleware('auth');
Route::get('/shift/edit/{id}', [ShiftController::class, 'edit'])->middleware('auth', 'admin');
Route::put('/shift/edit/{id}', [ShiftController::class, 'update'])->middleware('auth', 'admin');
Route::delete('/shift/delete/{id}', [ShiftController::class, 'destroy'])->middleware('auth', 'admin');
Route::post('/shift', [ShiftController::class, 'store'])->middleware('auth');

// Dynamic Shift Change
Route::get('/dynamic-shift-change', [DynamicShiftChangeController::class, 'create'])->middleware('auth');
Route::post('/dynamic-shift-change', [DynamicShiftChangeController::class, 'store'])->middleware('auth');
Route::delete('/dynamic-shift-change/{id}', [DynamicShiftChangeController::class, 'destroy'])->middleware('auth');


// Shift Schedule
Route::get('/shift-schedule', [ShiftScheduleController::class, 'create'])->middleware('auth');
Route::get('/shift-schedule/edit/{id}', [ShiftScheduleController::class, 'edit'])->middleware('auth', 'admin');
Route::put('/shift-schedule/edit/{id}', [ShiftScheduleController::class, 'update'])->middleware('auth', 'admin');
Route::delete('/shift-schedule/delete/{id}', [ShiftScheduleController::class, 'destroy'])->middleware('auth', 'admin');
Route::post('/shift-schedule', [ShiftScheduleController::class, 'store'])->middleware('auth');

// Change Shift
Route::get('/change-shift', [ChangeShiftController::class, 'create'])->middleware('auth');
Route::get('/change-shift/list-request', [ChangeShiftController::class, 'list_request_change_shift'])->middleware('auth');
Route::post('/change-shift', [ChangeShiftController::class, 'store'])->middleware('auth');
Route::put('/change-shift/list-request/accept/{id}', function () {
    ChangeShift::find(request('accept'))->update([
        'approval' => 'accept'
    ]);
    return back()->with('accept', 'pengajuan change shift diterima');
})->middleware('auth');
Route::put('/change-shift/list-request/decline/{id}', function () {
    ChangeShift::find(request('decline'))->update([
        'approval' => 'decline'
    ]);
    return back()->with('decline', 'pengajuan change shift ditolak');
})->middleware('auth');

// User
Route::get('/user', [UserController::class, 'index'])->middleware('auth');
Route::get('/user/tambah-user-baru', [UserController::class, 'create'])->middleware('auth', 'admin');
Route::get('/user/tambah-user-baru/active/{id}', function ($id) {
    User::find($id)->update([
        'is_active' => 1
    ]);
    return back()->with('berhasil', 'user berhasil diaktifkan');
})->middleware('auth', 'admin');
Route::get('/user/tambah-user-baru/deactive/{id}', function ($id) {
    User::find($id)->update([
        'is_active' => 0
    ]);
    return back()->with('deactive', 'user berhasil dinonaktifkan');
})->middleware('auth', 'admin');
Route::post('/user/tambah-user-baru', [UserController::class, 'store'])->middleware('auth');
// User Shift Schedule
Route::get('/user/tambah-shift-schedule', [UserController::class, 'add_shift_schedule_create'])->middleware('auth');
Route::delete('/user/tambah-shift-schedule/delete/{email}', [UserController::class, 'delete_shift_schedule_create'])->middleware('auth');
Route::post('/user/tambah-shift-schedule', [UserController::class, 'add_shift_schedule_store'])->middleware('auth');
// User Salary
Route::get('/user/tambah-salary', [UserController::class, 'add_salary_create'])->middleware('auth');
Route::post('/user/tambah-salary', [UserController::class, 'add_salary_store'])->middleware('auth');
// User Edit Profile
Route::get('/user/edit-profile', [UserController::class, 'edit_profile'])->middleware('auth');
Route::post('/user/edit-profile', [UserController::class, 'edit_profile_update'])->middleware('auth');

// Branch
Route::get('/branch-company', [BranchCompanyController::class, 'create'])->middleware('auth');
Route::get('/branch-company/edit/{id}', [BranchCompanyController::class, 'edit'])->middleware('auth', 'admin');
Route::get('/branch-company/delete/{id}', [BranchCompanyController::class, 'destroy'])->middleware('auth', 'superadmin');
Route::put('/branch-company/edit/{id}', [BranchCompanyController::class, 'update'])->middleware('auth', 'admin');
Route::post('/branch-company', [BranchCompanyController::class, 'store'])->middleware('auth');

// Entity
Route::get('/entity', [EntityController::class, 'create'])->middleware('auth');
Route::get('/entity/edit/{id}', [EntityController::class, 'edit'])->middleware('auth', 'admin');
Route::get('/entity/delete/{id}', [EntityController::class, 'destroy'])->middleware('auth', 'superadmin');
Route::put('/entity/edit/{$id}', [EntityController::class, 'update'])->middleware('auth');
Route::post('/entity', [EntityController::class, 'store'])->middleware('auth');

// Department
Route::get('/department', [DepartmentController::class, 'create'])->middleware('auth');
Route::get('/department/edit/{id}', [DepartmentController::class, 'edit'])->middleware('auth');
Route::get('/department/delete/{id}', [DepartmentController::class, 'destroy'])->middleware('auth', 'superadmin');
Route::put('/department/edit/{id}', [DepartmentController::class, 'update'])->middleware('auth');
Route::post('/department', [DepartmentController::class, 'store'])->middleware('auth');

// Salary
Route::get('/salary', [SalaryController::class, 'create'])->middleware('auth');
Route::get('/salary/edit/{id}', [SalaryController::class, 'edit'])->middleware('auth');
Route::post('/salary', [SalaryController::class, 'store'])->middleware('auth');
Route::put('/salary/edit/{id}', [SalaryController::class, 'update'])->middleware('auth');
Route::delete('/salary/delete/{id}', [SalaryController::class, 'destroy'])->middleware('auth');

// Organizational Structure
Route::get('/organizational-structure', [OrganizationalStructureController::class, 'create'])->middleware('auth');
Route::get('/organizational-structure/edit/{id}', [OrganizationalStructureController::class, 'edit'])->middleware('auth');
Route::put('/organizational-structure/edit/{id}', [OrganizationalStructureController::class, 'update'])->middleware('auth');
Route::delete('/organizational-structure/delete/{id}', [OrganizationalStructureController::class, 'destroy'])->middleware('auth');
Route::post('/organizational-structure', [OrganizationalStructureController::class, 'store'])->middleware('auth');

// Group
Route::get('/group', [GroupController::class, 'create'])->middleware('auth');
Route::get('/group/edit/{id}', [GroupController::class, 'edit'])->middleware('auth');
Route::put('/group/edit/{id}', [GroupController::class, 'update'])->middleware('auth');
Route::delete('/group/delete/{id}', [GroupController::class, 'destroy'])->middleware('auth');
Route::post('/group', [GroupController::class, 'store'])->middleware('auth');
// Group Users
Route::get('/group-users', [GroupController::class, 'group_users'])->middleware('auth');
Route::delete('/group-users/delete/{id}', [GroupController::class, 'group_users_delete'])->middleware('auth');
Route::post('/group-users', [GroupController::class, 'group_users_store'])->middleware('auth');

// Attendance Correction
Route::get('/koreksi-absensi', [AttendanceCorrectionController::class, 'create'])->middleware('auth');
Route::get('/koreksi-absensi/list-request', [AttendanceCorrectionController::class, 'show'])->middleware('auth');
Route::get('/koreksi-absensi/{id}', [AttendanceCorrectionController::class, 'edit'])->middleware('auth');
Route::post('/koreksi-absensi/{id}', [AttendanceCorrectionController::class, 'store'])->middleware('auth');
Route::post('/koreksi-absensi/accept/{id}', [HistoryAttendanceController::class, 'update'])->middleware('auth');
Route::post('/koreksi-absensi/decline/{id}', [AttendanceCorrectionController::class, 'update'])->middleware('auth');

// Allowance
Route::get('/allowance', [AllowanceController::class, 'create'])->middleware('auth');
Route::post('/allowance', [AllowanceController::class, 'store'])->middleware('auth');

// Formula
Route::get('/formula', [FormulaController::class, 'create'])->middleware('auth');
Route::post('/formula', [FormulaController::class, 'store'])->middleware('auth');

// Employee Allowance
Route::get('/employee-allowance-base-on-allowance', [EmployeeAllowanceController::class, 'create_base_on_allowance'])->middleware('auth');
Route::get('/employee-allowance-base-on-user', [EmployeeAllowanceController::class, 'create_base_on_user'])->middleware('auth');
Route::post('/employee-allowance', [EmployeeAllowanceController::class, 'store'])->middleware('auth');

// Overtime
Route::get('/overtime', [OvertimeController::class, 'create'])->middleware('auth');
Route::post('/overtime', [OvertimeController::class, 'store'])->middleware('auth');
Route::put('/overtime', [OvertimeController::class, 'update'])->middleware('auth');
Route::get('/overtime/list-request', [OvertimeController::class, 'show'])->middleware('auth');

// Office Position
Route::get('/office-position', [OfficePositionController::class, 'create'])->middleware('auth');
Route::post('/office-position', [OfficePositionController::class, 'store'])->middleware('auth');

// Paid Leave
Route::get('/paid-leave', [PaidLeaveHistoryController::class, 'create'])->middleware('auth');
Route::get('/paid-leave/add-paid-leave', [PaidLeaveController::class, 'create'])->middleware('auth');
Route::post('/paid-leave/add-paid-leave/{company_id}', [PaidLeaveController::class, 'store'])->middleware('auth');
Route::get('/paid-leave/edit/{id}', [PaidLeaveController::class, 'edit'])->middleware('auth');
Route::put('/paid-leave/edit/{id}', [PaidLeaveController::class, 'update'])->middleware('auth');
Route::delete('/paid-leave/delete/{id}', [PaidLeaveController::class, 'destroy'])->middleware('auth');
Route::get('/paid-leave/paid-leave-access', [PaidLeaveAccessController::class, 'create'])->middleware('auth');
Route::post('/paid-leave/paid-leave-access', [PaidLeaveAccessController::class, 'store'])->middleware('auth');
Route::post('/paid-leave', [PaidLeaveHistoryController::class, 'store'])->middleware('auth');
Route::get('/paid-leave/list-request', [PaidLeaveHistoryController::class, 'show'])->middleware('auth');
Route::get('/paid-leave/list-request/accept/{id}', [PaidLeaveHistoryController::class, 'update'])->middleware('auth', 'admin');
Route::get('/paid-leave/list-request/decline/{id}', function ($id) {
    PaidLeaveHistory::find($id)->update([
        'approval' => 'decline'
    ]);
    return back()->with('decline', 'request cuti ditolak');
})->middleware('auth', 'admin');

// Reimburse
Route::get('/reimburse', [ReimburseController::class, 'create'])->middleware('auth');
Route::post('/reimburse', [ReimburseController::class, 'store'])->middleware('auth');
Route::get('/reimburse/list-request', [ReimburseController::class, 'show'])->middleware('auth');
Route::get('/reimburse/list-request/accept/{id}', function ($id) {
    Reimburse::find($id)->update([
        'approval' => 'accept'
    ]);
    return back()->with('berhasil', 'Request reimburse diterima');
})->middleware('auth', 'admin');
Route::get('/reimburse/list-request/decline/{id}', function ($id) {
    Reimburse::find($id)->update([
        'approval' => 'decline'
    ]);
    return back()->with('gagal', 'Request reimburse ditolak');
})->middleware('auth', 'admin');

// Job Level
Route::get('/job-level', [JobLevelController::class, 'create'])->middleware('auth', 'admin');
Route::post('/job-level', [JobLevelController::class, 'store'])->middleware('auth', 'admin');
// job level user
Route::get('/job-level-user', [UserController::class, 'create_job_level'])->middleware('auth', 'admin');
Route::post('/job-level-user', [UserController::class, 'store_job_level'])->middleware('auth', 'admin');






// Admin Absensih
Route::get('/admin-absensih', [AdminAbsensihController::class, 'index'])->middleware('superadmin');
//company
Route::get('/admin-absensih/create-company', [AdminAbsensihController::class, 'create_company'])->middleware('superadmin');
Route::post('/admin-absensih/create-company', [AdminAbsensihController::class, 'store_company'])->middleware('superadmin');
//branch
Route::get('/admin-absensih/create-branch', [AdminAbsensihController::class, 'create_branch'])->middleware('superadmin');
Route::post('/admin-absensih/create-branch', [AdminAbsensihController::class, 'store_branch'])->middleware('superadmin');
//entity
Route::get('/admin-absensih/create-entity', [AdminAbsensihController::class, 'create_entity'])->middleware('superadmin');
Route::post('/admin-absensih/create-entity', [AdminAbsensihController::class, 'store_entity'])->middleware('superadmin');
//department
Route::get('/admin-absensih/create-department', [AdminAbsensihController::class, 'create_department'])->middleware('superadmin');
Route::post('/admin-absensih/create-department', [AdminAbsensihController::class, 'store_department'])->middleware('superadmin');
//user
Route::get('admin-absensih/create-user', [AdminAbsensihController::class, 'create_user'])->middleware('superadmin');
Route::post('admin-absensih/create-user', [AdminAbsensihController::class, 'store_user'])->middleware('superadmin');
// office position
Route::get('/admin-absensih/create-office-position', [AdminAbsensihController::class, 'create_office_position'])->middleware('superadmin');
Route::post('/admin-absensih/create-office-position', [AdminAbsensihController::class, 'store_office_position'])->middleware('superadmin');

Route::get('history-attendance-v2', [HistoryAttendanceController::class, 'historyAttendanceV2'])->middleware('superadmin')->name('historyV2');
Route::get('history-attendance-v2-edit/{id}', function ($id) {
    dd('edit ' . $id);
})->name('edit');






// Route::get('/test', function() {
//     dd(HistoryAttendance::limit(1)->get() );
//     });

// Route::get('/test2', function() {
//     $check_photo_checkout = auth()->user()->historyattendance()->whereDate('checkin_time', today())->get();
//     var_dump(json_encode($check_photo_checkout));
//     });

Route::get('holiday-update', [HolidayController::class, 'index']);

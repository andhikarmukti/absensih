<?php

use Illuminate\Http\Request;
use App\Models\HistoryAttendance;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserApiController;
use App\Http\Controllers\LoginApiController;
use App\Http\Controllers\ShiftApiController;
use App\Http\Controllers\OvertimeApiController;
use App\Http\Controllers\PaidLeaveApiController;
use App\Http\Controllers\ChangeShiftApiController;
use App\Http\Controllers\ShiftScheduleApiController;
use App\Http\Controllers\PaidLeaveHistoryApiController;
use App\Http\Controllers\HistoryAttendanceApiController;
use App\Http\Controllers\AttendanceCorrectionApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Attendance
Route::post('/attendance-submit', [HistoryAttendanceApiController::class, 'store'])->middleware('api-auth', 'location-check');
Route::get('/attendance-history', [HistoryAttendanceApiController::class, 'show'])->middleware('api-auth');
Route::post('/attendance-correction', [AttendanceCorrectionApiController::class, 'store'])->middleware('api-auth');

// Login
Route::post('/login', [LoginApiController::class, 'index'])->middleware('api-auth');

// User
Route::get('/user-by-id', [UserApiController::class, 'show'])->middleware('api-auth');
Route::post('/change-user-password', [UserApiController::class, 'update'])->middleware('api-auth');

// Overtime
Route::post('/overtime', [OvertimeApiController::class, 'store'])->middleware('api-auth');

// Change Shift
Route::post('/change-shift', [ChangeShiftApiController::class, 'store'])->middleware('api-auth');

// Shift
Route::get('/shift', [ShiftApiController::class, 'show'])->middleware('api-auth');

// Shift Schedule
Route::get('/shift-schedule', [ShiftScheduleApiController::class, 'show'])->middleware('api-auth');

// Paid Leave
Route::get('/paid-leave', [PaidLeaveHistoryApiController::class, 'show'])->middleware('api-auth');
Route::post('/paid-leave', [PaidLeaveHistoryApiController::class, 'store'])->middleware('api-auth');

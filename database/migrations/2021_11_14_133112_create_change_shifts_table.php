<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChangeShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_shifts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->date('start_shift_date');
            $table->date('end_shift_date');
            $table->foreignId('shift_id');
            $table->string('approval')->default('pending');
            $table->integer('supervisor_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_shifts');
    }
}

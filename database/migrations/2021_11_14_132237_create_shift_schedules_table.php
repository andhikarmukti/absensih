<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShiftSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_schedules', function (Blueprint $table) {
            $table->id();
            $table->foreignId('company_id');
            $table->foreignId('shift_id')->nullable();
            $table->string('shift_schedule_name');
            $table->foreignId('Monday')->nullable();
            $table->foreignId('Tuesday')->nullable();
            $table->foreignId('Wednesday')->nullable();
            $table->foreignId('Thursday')->nullable();
            $table->foreignId('Friday')->nullable();
            $table->foreignId('Saturday')->nullable();
            $table->foreignId('Sunday')->nullable();
            $table->date('start_shift_pattern_date');
            $table->date('end_shift_pattern_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shift_schedules');
    }
}

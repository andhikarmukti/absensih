<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOvertimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overtimes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->date('start_overtime_date');
            $table->date('end_overtime_date');
            $table->time('start_overtime_hour');
            $table->time('end_overtime_hour');
            $table->string('total_overtime_hour');
            $table->string('approval')->default('pending');
            $table->string('description')->nullable();
            $table->integer('supervisor_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overtimes');
    }
}

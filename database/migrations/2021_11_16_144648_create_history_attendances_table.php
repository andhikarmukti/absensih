<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_attendances', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('shift_id')->nullable();
            $table->dateTime('checkin_time')->nullable();
            $table->dateTime('checkout_time')->nullable();
            $table->time('total_working_hours')->nullable();
            $table->string('checkin_image')->default('storage/img/default.jpg');
            $table->string('checkout_image')->default('storage/img/default.jpg');
            $table->string('latitude_checkin')->nullable();
            $table->string('longitude_checkin')->nullable();
            $table->string('latitude_checkout')->nullable();
            $table->string('longitude_checkout')->nullable();
            $table->string('arrival_status')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_attendances');
    }
}

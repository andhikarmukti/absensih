<?php

namespace Database\Seeders;

use App\Models\Holiday;
use App\Models\PaidLeaveUser;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            AllowanceSeeder::class,
            AttendanceCorrectionSeeder::class,
            BranchCompanySeeder::class,
            ChangeShiftSeeder::class,
            CompanySeeder::class,
            CompanySettingSeeder::class,
            DepartmentSeeder::class,
            EmployeeAllowanceSeeder::class,
            EntitySeeder::class,
            FormulaSeeder::class,
            GroupSeeder::class,
            GroupUserSeeder::class,
            HistoryAttendanceSeeder::class,
            HolidaySeeder::class,
            JobLevelSeeder::class,
            OfficePositionSeeder::class,
            OrganizationalStructureSeeder::class,
            OvertimeSeeder::class,
            PaidLeaveHistorySeeder::class,
            PaidLeaveSeeder::class,
            PaidLeaveUserSeeder::class,
            PayrollSeeder::class,
            ReimburseSeeder::class,
            SalarySeeder::class,
            ShiftPatternSeeder::class,
            ShiftScheduleSeeder::class,
            ShiftSeeder::class,
            UserSeeder::class
        ]);
    }
}

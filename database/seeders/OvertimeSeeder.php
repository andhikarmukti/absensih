<?php

namespace Database\Seeders;

use App\Models\Overtime;
use Illuminate\Database\Seeder;

class OvertimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Overtime::create([
            'user_id' => 1,
            'start_overtime_date' => '2021-12-10',
            'end_overtime_date' => '2021-12-18',
            'start_overtime_hour' => '16:00:00',
            'end_overtime_hour' => '18:00:00',
            'total_overtime_hour' => 2,
            'description' => 'lembur meeting',
            'supervisor_id' => 1
        ]);
    }
}

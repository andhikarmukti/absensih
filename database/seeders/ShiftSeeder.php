<?php

namespace Database\Seeders;

use App\Models\Shift;
use Illuminate\Database\Seeder;

class ShiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shift::create([
            'company_id' => 1,
            'shift_name' => 'Shift Off'
        ]);
        Shift::create([
            'company_id' => 1,
            'shift_name' => 'Shift Pagi',
            'start_shift_hours' => '08:00',
            'end_shift_hours' => '16:00'
        ]);
        Shift::create([
            'company_id' => 1,
            'shift_name' => 'Shift Siang',
            'start_shift_hours' => '14:00',
            'end_shift_hours' => '22:00'
        ]);
        Shift::create([
            'company_id' => 1,
            'shift_name' => 'Shift Flexible 6 Jam'
        ]);
        Shift::create([
            'company_id' => 1,
            'shift_name' => 'Shift Flexible 8 Jam'
        ]);
    }
}

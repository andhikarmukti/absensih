<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
            'entity_id' => 1,
            'department_name' => 'IT'
        ]);
        Department::create([
            'entity_id' => 3,
            'department_name' => 'Marketing'
        ]);
        Department::create([
            'entity_id' => 1,
            'department_name' => 'Accounting'
        ]);
        Department::create([
            'entity_id' => 4,
            'department_name' => 'Maintenance'
        ]);
        Department::create([
            'entity_id' => 5,
            'department_name' => 'Production'
        ]);
    }
}

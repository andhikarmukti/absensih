<?php

namespace Database\Seeders;

use App\Models\PaidLeaveUser;
use Illuminate\Database\Seeder;

class PaidLeaveUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaidLeaveUser::create([
            'user_id' => 2,
            'paid_leave_id' => 1,
            'paid_leave_quota' => 12,
            'validity_period' => '2021'
        ]);
        
        PaidLeaveUser::create([
            'user_id' => 1,
            'paid_leave_id' => 1,
            'paid_leave_quota' => 12,
            'validity_period' => '2021'
        ]);

        PaidLeaveUser::create([
            'user_id' => 3,
            'paid_leave_id' => 1,
            'paid_leave_quota' => 12,
            'validity_period' => '2021'
        ]);

        PaidLeaveUser::create([
            'user_id' => 3,
            'paid_leave_id' => 3,
            'paid_leave_quota' => 3,
            'validity_period' => '2021'
        ]);

        PaidLeaveUser::create([
            'user_id' => 3,
            'paid_leave_id' => 2,
            'paid_leave_quota' => 3,
            'validity_period' => '2021'
        ]);
    }
}

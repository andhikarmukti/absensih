<?php

namespace Database\Seeders;

use App\Models\PaidLeave;
use Illuminate\Database\Seeder;

class PaidLeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaidLeave::create([
            'company_id' => 1,
            'paid_leave_name' => 'Cuti Karyawan'
        ]);
        PaidLeave::create([
            'company_id' => 1,
            'paid_leave_name' => 'Cuti Nikah'
        ]);
        PaidLeave::create([
            'company_id' => 1,
            'paid_leave_name' => 'Cuti Melahirkan'
        ]);
    }
}

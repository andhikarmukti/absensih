<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
            'company_id' => 1,
            'group_name' => 'group A'
        ]);
        Group::create([
            'company_id' => 1,
            'group_name' => 'group B'
        ]);
        Group::create([
            'company_id' => 1,
            'group_name' => 'group C'
        ]);
    }
}

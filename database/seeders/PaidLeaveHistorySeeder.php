<?php

namespace Database\Seeders;

use App\Models\PaidLeaveHistory;
use Illuminate\Database\Seeder;

class PaidLeaveHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaidLeaveHistory::create([
            'user_id' => 2,
            'paid_leave_id' => 1,
            'start_paid_leave_date' => '2021-11-18',
            'end_paid_leave_date' => '2021-11-20',
            'description' => 'cuti untuk acara keluarga',
            'supervisor_id' => 1
        ]);

        PaidLeaveHistory::create([
            'user_id' => 3,
            'paid_leave_id' => 2,
            'start_paid_leave_date' => '2021-12-12',
            'end_paid_leave_date' => '2021-12-15',
            'description' => 'menikah dengan pasangan',
            'supervisor_id' => 1
        ]);

        PaidLeaveHistory::create([
            'user_id' => 1,
            'paid_leave_id' => 1,
            'start_paid_leave_date' => '2021-12-25',
            'end_paid_leave_date' => '2021-12-30',
            'description' => 'liburan keluarga',
            'supervisor_id' => 1
        ]);
    }
}

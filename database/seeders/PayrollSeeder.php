<?php

namespace Database\Seeders;

use App\Models\Payroll;
use Illuminate\Database\Seeder;

class PayrollSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Payroll::create([
            'user_id' => 1,
            'month' => '11',
            'year' => '2021',
            'income_type' => 'Gaji Pokok',
            'income_value' => 8000000
        ]);
        Payroll::create([
            'user_id' => 1,
            'month' => '11',
            'year' => '2021',
            'income_type' => 'Tunjangan Makan',
            'income_value' => 500000
        ]);
        Payroll::create([
            'user_id' => 1,
            'month' => '11',
            'year' => '2021',
            'income_type' => 'Tunjangan Kesehatan',
            'income_value' => 100000
        ]);
    }
}

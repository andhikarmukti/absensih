<?php

namespace Database\Seeders;

use App\Models\ChangeShift;
use Illuminate\Database\Seeder;

class ChangeShiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChangeShift::create([
            'user_id' => 1,
            'start_shift_date' => '2021-11-17',
            'end_shift_date' => '2021-11-17',
            'shift_id' => 3,
            'supervisor_id' => 1
        ]);
        ChangeShift::create([
            'user_id' => 1,
            'start_shift_date' => '2021-11-20',
            'end_shift_date' => '2021-11-20',
            'shift_id' => 3,
            'supervisor_id' => 1,
            'approval' => 'accept'
        ]);
    }
}

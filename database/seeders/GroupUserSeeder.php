<?php

namespace Database\Seeders;

use App\Models\GroupUser;
use Illuminate\Database\Seeder;

class GroupUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GroupUser::create([
            'group_id' => 1,
            'user_id' => 1
        ]);
        GroupUser::create([
            'group_id' => 1,
            'user_id' => 2
        ]);
        GroupUser::create([
            'group_id' => 2,
            'user_id' => 1
        ]);
        GroupUser::create([
            'group_id' => 1,
            'user_id' => 3
        ]);
        GroupUser::create([
            'group_id' => 3,
            'user_id' => 1
        ]);
    }
}

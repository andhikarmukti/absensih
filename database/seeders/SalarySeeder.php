<?php

namespace Database\Seeders;

use App\Models\Salary;
use Illuminate\Database\Seeder;

class SalarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Salary::create([
            'company_id' => 1,
            'salary_name' => 'magang',
            'salary_value' => 2000000
        ]);
        Salary::create([
            'company_id' => 1,
            'salary_name' => 'staff',
            'salary_value' => 4000000
        ]);
        Salary::create([
            'company_id' => 1,
            'salary_name' => 'senior staff',
            'salary_value' => 5000000
        ]);
    }
}

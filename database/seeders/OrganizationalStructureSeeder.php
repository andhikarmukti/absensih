<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OrganizationalStructure;

class OrganizationalStructureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrganizationalStructure::create([
            'user_id' => 2,
            'supervisor_id' => 1,
            // 'difference_level' => 3
        ]);
        OrganizationalStructure::create([
            'user_id' => 3,
            'supervisor_id' => 1,
            // 'difference_level' => 3
        ]);

        OrganizationalStructure::create([
            'user_id' => 5,
            'supervisor_id' => 4,
            // 'difference_level' => 3
        ]);
        OrganizationalStructure::create([
            'user_id' => 6,
            'supervisor_id' => 4,
            // 'difference_level' => 3
        ]);
    }
}

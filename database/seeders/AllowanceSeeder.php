<?php

namespace Database\Seeders;

use App\Models\Allowance;
use Illuminate\Database\Seeder;

class AllowanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Allowance::create([
            'company_id' => 1,
            'allowance_name' => 'Tunjangan Kesehatan',
            'allowance_value' => 100000,
            'formula_id' => 1
        ]);
        Allowance::create([
            'company_id' => 1,
            'allowance_name' => 'Tunjangan Jabatan',
            'allowance_value' => 300000,
            'formula_id' => 1
        ]);
        Allowance::create([
            'company_id' => 1,
            'allowance_name' => 'Tunjangan Makan',
            'allowance_value' => 20000,
            'formula_id' => 2
        ]);
        Allowance::create([
            'company_id' => 5,
            'allowance_name' => 'Tunjangan Transport',
            'allowance_value' => 15000,
            'formula_id' => 2
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\BranchCompany;
use Illuminate\Database\Seeder;

class BranchCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BranchCompany::create([
            'company_id' => '1',
            'branch_name' => 'Cabang Cilegon',
            'branch_longitude' => null,
            'branch_latitude' => null
        ]);
        BranchCompany::create([
            'company_id' => '2',
            'branch_name' => 'Cabang Serang',
            'branch_longitude' => null,
            'branch_latitude' => null
        ]);
        BranchCompany::create([
            'company_id' => '5',
            'branch_name' => 'Cabang Tangerang',
            'branch_longitude' => null,
            'branch_latitude' => null
        ]);
        BranchCompany::create([
            'company_id' => '3',
            'branch_name' => 'Cabang Serang',
            'branch_longitude' => null,
            'branch_latitude' => null
        ]);
        BranchCompany::create([
            'company_id' => '4',
            'branch_name' => 'Cabang Depok',
            'branch_longitude' => null,
            'branch_latitude' => null
        ]);
    }
}

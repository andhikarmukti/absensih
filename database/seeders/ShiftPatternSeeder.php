<?php

namespace Database\Seeders;

use App\Models\ShiftPattern;
use Illuminate\Database\Seeder;

class ShiftPatternSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShiftPattern::create([
            'shift_id' => 2,
            'shift_day' => 'monday'
        ]);
        ShiftPattern::create([
            'shift_id' => 2,
            'shift_day' => 'tuesday'
        ]);
        ShiftPattern::create([
            'shift_id' => 2,
            'shift_day' => 'wednesday'
        ]);
        ShiftPattern::create([
            'shift_id' => 2,
            'shift_day' => 'thursday'
        ]);
        ShiftPattern::create([
            'shift_id' => 2,
            'shift_day' => 'friday'
        ]);
        ShiftPattern::create([
            'shift_id' => 2,
            'shift_day' => 'staruday'
        ]);
        ShiftPattern::create([
            'shift_id' => 3,
            'shift_day' => 'monday'
        ]);
        ShiftPattern::create([
            'shift_id' => 3,
            'shift_day' => 'tuesday'
        ]);
        ShiftPattern::create([
            'shift_id' => 3,
            'shift_day' => 'wednesday'
        ]);
        ShiftPattern::create([
            'shift_id' => 3,
            'shift_day' => 'thursday'
        ]);
        ShiftPattern::create([
            'shift_id' => 3,
            'shift_day' => 'friday'
        ]);
        ShiftPattern::create([
            'shift_id' => 3,
            'shift_day' => 'staruday'
        ]);
    }
}

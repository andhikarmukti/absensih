<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'full_name' => 'Andhika Raharja Mukti',
            'username' => 'andhikarm',
            'email' => 'andhikarmukti@gmail.com',
            'role' => 'superadmin',
            'department_id' => 1,
            'shift_schedule_id' => 1,
            'salary_id' => 3,
            'job_level_id' => 1,
            'office_position_id' => 1,
            'password' => Hash::make('asdasdasd'),
            'is_active' => 1
        ]);
        User::create([
            'full_name' => 'Muhajir',
            'username' => 'muhajir',
            'email' => 'muhajir@gmail.com',
            'role' => 'staff',
            'department_id' => 1,
            'shift_schedule_id' => 1,
            'salary_id' => 2,
            'job_level_id' => 2,
            'office_position_id' => 1,
            'password' => Hash::make('asdasdasd'),
            'is_active' => 1
        ]);
        User::create([
            'full_name' => 'Prisilia',
            'username' => 'prisil',
            'email' => 'prisilia@gmail.com',
            'role' => 'staff',
            'department_id' => 3,
            'shift_schedule_id' => 2,
            'salary_id' => 1,
            'job_level_id' => 2,
            'office_position_id' => 2,
            'password' => Hash::make('asdasdasd'),
            'is_active' => 1
        ]);

        User::create([
            'full_name' => 'Yusril Kholili',
            'username' => 'yusril',
            'email' => 'yusrilkholili@gmail.com',
            'role' => 'admin',
            'department_id' => 2,
            'shift_schedule_id' => 3,
            'salary_id' => 5,
            'job_level_id' => 1,
            'office_position_id' => 7,
            'password' => Hash::make('asdasdasd'),
            'is_active' => 1
        ]);
        User::create([
            'full_name' => 'Eriska Eriyani',
            'username' => 'eriskaeriyani',
            'email' => 'eriska@gmail.com',
            'role' => 'staff',
            'department_id' => 2,
            'shift_schedule_id' => 3,
            'salary_id' => 5,
            'job_level_id' => 2,
            'office_position_id' => 5,
            'password' => Hash::make('asdasdasd'),
            'is_active' => 1
        ]);
        User::create([
            'full_name' => 'Dita Novianti',
            'username' => 'ditanovianti',
            'email' => 'dita@gmail.com',
            'role' => 'staff',
            'department_id' => 2,
            'shift_schedule_id' => 3,
            'salary_id' => 5,
            'job_level_id' => 2,
            'office_position_id' => 6,
            'password' => Hash::make('asdasdasd'),
            'is_active' => 1
        ]);
    }
}

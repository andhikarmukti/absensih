<?php

namespace Database\Seeders;

use App\Models\JobLevel;
use Illuminate\Database\Seeder;

class JobLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobLevel::create([
            'level_name' => 'Leader',
            'level_value' => 3,
            'company_id' => 1
        ]);
        JobLevel::create([
            'level_name' => 'Senior Staff',
            'level_value' => 4,
            'company_id' => 1
        ]);
        JobLevel::create([
            'level_name' => 'Staff',
            'level_value' => 5,
            'company_id' => 1
        ]);
        JobLevel::create([
            'level_name' => 'Magang',
            'level_value' => 6,
            'company_id' => 1
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\OfficePosition;
use Illuminate\Database\Seeder;

class OfficePositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OfficePosition::create([
            'company_id' => 1,
            'position_name' => 'Web Developer'
        ]);

        OfficePosition::create([
            'company_id' => 1,
            'position_name' => 'Accounting'
        ]);

        OfficePosition::create([
            'company_id' => 1,
            'position_name' => 'Customer Service'
        ]);

        OfficePosition::create([
            'company_id' => 1,
            'position_name' => 'IT Support'
        ]);

        OfficePosition::create([
            'company_id' => 5,
            'position_name' => 'Accounting'
        ]);

        OfficePosition::create([
            'company_id' => 5,
            'position_name' => 'Customer Service'
        ]);

        OfficePosition::create([
            'company_id' => 5,
            'position_name' => 'IT Support'
        ]);
    }
}

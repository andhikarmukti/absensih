<?php

namespace Database\Seeders;

use App\Models\Reimburse;
use Illuminate\Database\Seeder;

class ReimburseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Reimburse::create([
            'user_id' => 3,
            'reimburse_date' => '2021-11-17',
            'reimburse_value' => 75000,
            'photo_proof' => 'buktifotoprisil.jpg',
            'description' => 'beli stempel',
            'supervisor_id' => 1
        ]);
    }
}

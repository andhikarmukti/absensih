<?php

namespace Database\Seeders;

use App\Models\EmployeeAllowance;
use Illuminate\Database\Seeder;

class EmployeeAllowanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeAllowance::create([
            'user_id' => 1,
            'allowance_id' => 1
        ]);

        EmployeeAllowance::create([
            'user_id' => 1,
            'allowance_id' => 2
        ]);

        EmployeeAllowance::create([
            'user_id' => 2,
            'allowance_id' => 1
        ]);
    }
}

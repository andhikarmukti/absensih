<?php

namespace Database\Seeders;

use App\Models\Formula;
use Illuminate\Database\Seeder;

class FormulaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Formula::create([
            'company_id' => 1,
            'formula_name' => 'Tetap',
            'formula' => '*,1'
        ]);
        Formula::create([
            'company_id' => 1,
            'formula_name' => 'Dikali berdasarkan jumlah kehadiran tidak telat',
            'formula' => '*,kehadiran'
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Entity;
use Illuminate\Database\Seeder;

class EntitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Entity::create([
            'branch_id' => 1,
            'entity_name' => 'Artapuri'
        ]);
        Entity::create([
            'branch_id' => 1,
            'entity_name' => 'Bisnison'
        ]);
        Entity::create([
            'branch_id' => 5,
            'entity_name' => 'Marketplace'
        ]);
        Entity::create([
            'branch_id' => 2,
            'entity_name' => 'Rokok Kretek'
        ]);
        Entity::create([
            'branch_id' => 4,
            'entity_name' => 'Onkarir'
        ]);
    }
}

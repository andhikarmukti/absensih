<?php

namespace Database\Seeders;

use App\Models\AttendanceCorrection;
use Illuminate\Database\Seeder;

class AttendanceCorrectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AttendanceCorrection::create([
            'user_id' => 2,
            'history_attendance_id' => 2,
            'checkin_time_correction' => '07:50',
            'supervisor_id' => 1
        ]);
    }
}

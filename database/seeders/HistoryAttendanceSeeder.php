<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\HistoryAttendance;

class HistoryAttendanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HistoryAttendance::create([
            'user_id' => 3,
            'shift_id' => 2,
            'checkin_time' => '2021-11-16 07:52:00',
            'checkout_time' => '2021-11-16 16:02:00',
            'total_working_hours' => '08:10:00',
            'checkin_image' => 'dikagantengcheckin.jpg',
            'checkout_image' => 'dikagantengcheckout.jpg',
            'arrival_status' => 'late'
        ]);
        HistoryAttendance::create([
            'user_id' => 1,
            'shift_id' => 2,
            'checkin_time' => '2021-11-16 08:05:00',
            'checkout_time' => '2021-11-16 16:02:00',
            'total_working_hours' => '08:10:00',
            'checkin_image' => 'muhajircheckin.jpg',
            'checkout_image' => 'muhajircheckout.jpg',
            'arrival_status' => 'on time'
        ]);
        HistoryAttendance::create([
            'user_id' => 2,
            'shift_id' => 3,
            'checkin_time' => '2021-11-16 14:00:00',
            'checkout_time' => '2021-11-16 22:00:00',
            'total_working_hours' => '08:00:00',
            'checkin_image' => 'prisiliacheckin.jpg',
            'checkout_image' => 'prisiliacheckout.jpg',
            'arrival_status' => 'on time'
        ]);

        HistoryAttendance::create([
            'user_id' => 4,
            'shift_id' => 2,
            'checkin_time' => '2021-11-16 07:52:00',
            'checkout_time' => '2021-11-16 16:02:00',
            'total_working_hours' => '08:10:00',
            'checkin_image' => 'yusrilcheckin.jpg',
            'checkout_image' => 'yusrilcheckout.jpg',
            'arrival_status' => 'late'
        ]);
        HistoryAttendance::create([
            'user_id' => 5,
            'shift_id' => 2,
            'checkin_time' => '2021-11-16 08:05:00',
            'checkout_time' => '2021-11-16 16:02:00',
            'total_working_hours' => '08:10:00',
            'checkin_image' => 'eriskacheckin.jpg',
            'checkout_image' => 'eriskacheckout.jpg',
            'arrival_status' => 'on time'
        ]);
        HistoryAttendance::create([
            'user_id' => 6,
            'shift_id' => 3,
            'checkin_time' => '2021-11-16 14:00:00',
            'checkout_time' => '2021-11-16 22:00:00',
            'total_working_hours' => '08:00:00',
            'checkin_image' => 'ditacheckin.jpg',
            'checkout_image' => 'ditacheckout.jpg',
            'arrival_status' => 'on time'
        ]);
    }
}
